package de.iquartett.Generator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.PixmapPackerIO;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.ScreenUtils;

import java.io.IOException;
import java.util.ArrayList;

import de.iquartett.Parser.CardDeck.Card;
import de.iquartett.Parser.CardDeck.Properties;
import de.iquartett.QuartettGame;

import static com.badlogic.gdx.graphics.GL20.GL_COLOR_BUFFER_BIT;

public class AtlasGenerator {

    private final QuartettGame game;
    private Sprite sprite;
    private BitmapFont font;
    private Texture texture;

    private float newh;
    private float neww;
    private Table table;
    //private Label cat_text;
    //private Label cat_val;
    private ArrayList<Label> labels_text = new ArrayList<Label>();
    private ArrayList<Label> labels_val = new ArrayList<Label>();
    private float fontX;
    private float fontY;

    private float w = (200 * 1.5f);
    private float h = (277 * 1.5f);
    private float scale = 0.8f;

    private int space_bot = 5;
    private int space_side = 30;

    private int cards_per_page = 6;
    private int padding = 2;
    private int packer_width = (int) (w * cards_per_page + (cards_per_page + 1) * padding * 2);
    private int packer_height = (int) (h * cards_per_page + (cards_per_page + 1) * padding * 2);

    private String imgs_path;

    private SpriteBatch spriteBatch;
    private GlyphLayout layout;


    public AtlasGenerator(QuartettGame game) {
        this.game = game;

        OrthographicCamera cam = new OrthographicCamera(w, h);
        cam.position.set(new Vector2(w / 2, h / 2), 1);
        cam.update();

        game.loadBatchConfig();
        imgs_path = "iQuartett/" + game.getBatchConfig().getName();

        font = game.font;
        layout = new GlyphLayout(font, "");
        spriteBatch = new SpriteBatch();
        cam.update();
        spriteBatch.setProjectionMatrix(cam.combined);

        table = new Table(game.getSkin());
    }

    public ArrayList<Label> getLabels_text() {
        return labels_text;
    }

    public ArrayList<Label> getLabels_val() {
        return labels_val;
    }

    public Table getTable() {
        return table;
    }

    public void create_atlas() {
        PixmapPacker packer = new PixmapPacker(packer_width, packer_height, Pixmap.Format.RGB565, padding, true);
        packer.setTransparentColor(Color.WHITE);
        FrameBuffer m_fbo = new FrameBuffer(Pixmap.Format.RGB565, (int) w, (int) h, false);
        TextureRegion m_fboRegion;

        int cardsCounter = game.getBatchConfig().getCards().size();
        for (int cardID = 0; cardID < cardsCounter; cardID++) {
            System.out.println("Generating card " + (cardID + 1) + " of " + cardsCounter);


            m_fboRegion = new TextureRegion(m_fbo.getColorBufferTexture());
            m_fboRegion.flip(false, true);

            m_fbo.begin();
            Gdx.gl.glClearColor(1f, 1f, 1f, 1);
            Gdx.gl.glClear(GL_COLOR_BUFFER_BIT);

            /* Create card */
            Card c = game.getBatchConfig().getCards().get(cardID);
            create_card(c);

            /* Rendering */
            spriteBatch.begin();
            Gdx.gl.glClear(GL_COLOR_BUFFER_BIT);
            Gdx.gl.glClearColor(1f, 1, 1, 1f);
            spriteBatch.draw(sprite, 0, h - newh, neww, newh);
            font.draw(spriteBatch, layout, fontX, fontY);
            table.draw(spriteBatch, 1f);
            spriteBatch.end();

            /* Pack pixmap */
            Pixmap pixmap = ScreenUtils.getFrameBufferPixmap(0, 0, (int) w, (int) h);
            pixmap = flipPixmap(pixmap);
            packer.pack(String.valueOf(cardID), pixmap);

            m_fbo.end();
            //PixmapIO.writePNG(Gdx.files.external(imgs_path + "/" + cardID + ".png"), pixmap);
        }

        System.out.println("Finished generating cards");
        System.out.println("Start generating card pack...");

        PixmapPackerIO.SaveParameters saveParameters = new PixmapPackerIO.SaveParameters();
        saveParameters.magFilter = Texture.TextureFilter.Nearest;
        saveParameters.minFilter = Texture.TextureFilter.Nearest;

        //atlas = packer.generateTextureAtlas(saveParameters.minFilter, saveParameters.magFilter, false);

        packer.pack("back", new Pixmap(Gdx.files.internal("back.png")));
        System.out.println(Gdx.files.internal("back.png"));

        PixmapPackerIO pixmapPackerIO = new PixmapPackerIO();
        try {
            // todo add loading animation for this and move the following line to a new Thread to make it non-blocking
            FileHandle savehandle = Gdx.files.external(imgs_path + "/" + game.getBatchConfig().getName() + ".atlas");
            pixmapPackerIO.save(savehandle, packer, saveParameters);
            System.out.println("Finished generating card pack to: " + savehandle.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        packer.dispose();
        m_fbo.dispose();
        dispose();
    }

    // TODO Create different class for card creation!
    private void create_card(Card c) {
        // All three methods need to be called after another, should be fixed in future
        create_headerImg(c);
        create_cardTitle(c);
        create_categories(c, (int) w, (int) h, newh, scale, space_side, space_bot);
    }


    // todo w needs to be added dynamically
    public Sprite create_headerImg(Card c) {
        texture = new Texture(Gdx.files.external("iQuartett/raw/" + game.getSettings().getDeckName() + "/img/" + c.getImages().get(0).getFilename()));
        sprite = new Sprite(texture);

        // todo Needs to be removed from here
        /* Scale image to whole width */
        float faktor = w / sprite.getWidth();
        neww = sprite.getWidth() * faktor;
        newh = sprite.getHeight() * faktor;
        return sprite;
    }

    public GlyphLayout create_cardTitle(Card c) {
        font.setColor(Color.BLACK);

        if (QuartettGame.DEBUG) {
            font.getData().setScale(5f);
            layout.setText(font, String.valueOf(c.getId()));
        } else {
            // todo needs to be scaled with longest name on cards
            font.getData().setScale(0.5f);
//            layout.setText(font, game.getBatchConfig().getCards().get(c.getId()).getName());
            layout.setText(font, c.getName());
        }

        // todo needs to be removed from here
        fontX = 0 + (w - layout.width) / 2;
        fontY = h - newh - space_side / 2 - layout.height / 2;
        return layout;
    }

    public Table create_categories(Card c, int screen_width, int screen_height, float img_height, float text_scale, float space_side, float space_bot) {
        table.clear();
        labels_text.clear();
        labels_val.clear();
        table.pad(space_side);
        table.setSize(screen_width, screen_height - img_height);
        int catSize = c.getCategories().size();
        //catSize = 1;
        for (int i = 0; i < catSize; i++) {
            Properties cardProperties = game.getBatchConfig().getProperties().get(c.getCategories().get(i).getPropertyId());
            String catText = cardProperties.getText();

            labels_text.add(new Label(catText, game.getSkin()));
            labels_text.get(i).setFontScale(text_scale);
            labels_text.get(i).setColor(Color.BLACK);

            labels_val.add(new Label(String.valueOf(c.getCategories().get(i).getValue()) + " " + cardProperties.getUnit(), game.getSkin()));
            labels_val.get(i).setFontScale(text_scale);
            labels_val.get(i).setColor(Color.BLACK);

            Actor text_actor = labels_text.get(i);
            text_actor.setName("text_actor" + i);
            table.add(text_actor).spaceBottom(space_bot).left().expandX();
            table.add(labels_val.get(i)).spaceBottom(space_bot).left().expandX();

            if (game.getBatchConfig().getProperties().get(c.getCategories().get(i).getPropertyId()).higherWins()) {
                Label l = new Label(">", game.getSkin());
                l.setFontScale(text_scale);
                l.setColor(Color.BLACK);
                table.add(l).spaceBottom(space_bot).center();
            } else {
                Label l = new Label("<", game.getSkin());
                l.setFontScale(text_scale);
                l.setColor(Color.BLACK);
                table.add(l).spaceBottom(space_bot).center();
            }
            table.row();
        }
        return table;
    }

    private Pixmap flipPixmap(Pixmap src) {
        final int width = src.getWidth();
        final int height = src.getHeight();
        Pixmap flipped = new Pixmap(width, height, src.getFormat());
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                flipped.drawPixel((width - x - 1), (height - y - 1), src.getPixel(width - x - 1, y));
            }
        }
        return flipped;
    }

    private void dispose() {
        spriteBatch.dispose();
        texture.dispose();
        //font.dispose();
    }
}
