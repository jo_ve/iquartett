package de.iquartett.Generator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import de.iquartett.Parser.CardDeck.BatchConfig;
import de.iquartett.Parser.Config;
import de.iquartett.QuartettGame;

public class DeckGenerator {

    private final BatchConfig batchConfig;
    private final QuartettGame game;

    public DeckGenerator(QuartettGame game, BatchConfig batchConfig) {
        this.game = game;
        this.batchConfig = batchConfig;
    }

    public void saveDeck() {
        System.out.println("Start generating deck");
        FileHandle root = Gdx.files.external(game.getDecksPath() + "/" + batchConfig.getName().toLowerCase());
        root.file().mkdirs();
        FileHandle to = Gdx.files.external(root + "/" + batchConfig.getName().toLowerCase() + ".json");
        Config.encodeConfData(batchConfig, to);
        System.out.println("Finished generating deck " + batchConfig.getName().toLowerCase());
    }


}
