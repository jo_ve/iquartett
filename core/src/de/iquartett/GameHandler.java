package de.iquartett;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Timer;

import de.iquartett.Card.CardDeck;
import de.iquartett.Database.GameStateAndTurn;
import de.iquartett.Logic.GameData.GameState;
import de.iquartett.Logic.GameLogic;
import de.iquartett.Logic.PlayerData.AI;
import de.iquartett.Logic.PlayerData.Human;
import de.iquartett.Logic.PlayerData.PlayerEnum;
import de.iquartett.Logic.PlayerData.PlayerHandler;
import de.iquartett.Parser.CardDeck.BatchConfig;
import de.iquartett.Screens.GameScreen;
import de.iquartett.Screens.Screens;
import de.iquartett.Screens.WinningScreen;

public class GameHandler {
    private final QuartettGame game;
    private GameLogic gameLogic;
    private int currentRound;
    private PlayerEnum currentTurn;
    private PlayerHandler playerHandler;
    private CardHandler cardHandler;
    private GameState gameState;
    private boolean dealt;
    private int secondsToGo;

    public GameHandler(QuartettGame quartettGame) {
        CardDeck.resetCardDeck();
        this.game = quartettGame;
        game.setGameHandler(this);
        game.loadBatchConfig();
        // todo dispose textureAtlas anywhere
        TextureAtlas textureAtlas = new TextureAtlas(Gdx.files.external("iQuartett/" + this.getBatchConfig().getName() + "/" + this.getBatchConfig().getName() + ".atlas"));
        Human human;
        if (game.getDatabase().loadObject(2, "human.db") == null) {
            human = new Human(game);
        } else {
            human = (Human) game.getDatabase().loadObject(2, "human.db");
            human.game = game;
        }
        AI ai;
        if (game.getDatabase().loadObject(3, "ai.db") == null) {
            ai = new AI(game);
        } else {
            ai = (AI) game.getDatabase().loadObject(3, "ai.db");
            ai.game = game;
        }
        if (game.getDatabase().loadCardDeckHuman() != null) {
            CardDeck.setHumanDeckFromDatabase(game.getDatabase().loadCardDeckHuman());
//            System.out.println(game.getDatabase().loadCardDeckHuman().size() + " ################");
        }

        if (game.getDatabase().loadCardDeckComputer() != null) {
            CardDeck.setComputerDeckFromDatabase(game.getDatabase().loadCardDeckComputer());
            //System.out.println(game.getDatabase().loadCardDeckComputer().size() + " ################");
        }

        if (game.getDatabase().loadCardDeckNeutral() != null) {
            CardDeck.setNeutralDeckFromDatabase(game.getDatabase().loadCardDeckNeutral());
            //System.out.println(game.getDatabase().loadCardDeckNeutral().size() + " ################");
        }

        this.playerHandler = new PlayerHandler(human, ai);
        this.cardHandler = new CardHandler(this, textureAtlas);
        this.currentTurn = PlayerEnum.HUMAN;
        this.currentRound = 1;
        this.gameState = GameState.HIDDEN_CARD;
        gameLogic = new GameLogic(quartettGame);
        if (game.getDatabase().loadGameStateAndTurn() != null) {
            GameStateAndTurn gameStateAndTurn = game.getDatabase().loadGameStateAndTurn();
            currentTurn = gameStateAndTurn.getPlayer();
            gameState = gameStateAndTurn.getGameState();
            dealt = true;
            //System.out.println("GAMESTATEANDTURN: " + gameStateAndTurn.getGameState() + ", " + gameStateAndTurn.getPlayer());
        }
        if (!game.getSettings().isWinThroughRounds()) {
            secondsToGo = game.getSettings().getAmountOfTimeInMin() * 60;
            Timer.schedule(new Timer.Task() {
                               @Override
                               public void run() {
                                   secondsToGo--;
                               }
                           }
                    , 0        //    (delay)
                    , 1     //    (seconds)
            );
        }

    }

    public boolean isDealt() {
        return dealt;
    }

    void setDealt() {
        this.dealt = true;
    }

    public CardHandler getCardHandler() {
        return cardHandler;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void nextState() {
        gameState = gameState.nextState();
        //System.out.println("going to state: " + gameState);

    }

    public void previousState() {
        gameState = gameState.previousState();
    }

    public BatchConfig getBatchConfig() {
        return game.getBatchConfig();
    }

    public GameLogic getGameLogic() {
        return gameLogic;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public PlayerEnum getCurrentTurn() {
        return currentTurn;
    }

    public void setCurrentTurn(PlayerEnum currentTurn) {
        this.currentTurn = currentTurn;
    }

    public PlayerHandler getPlayerHandler() {
        return playerHandler;
    }

    public QuartettGame getGame() {
        return game;
    }

    public int getSecondsToGo() {
        return secondsToGo;
    }

    public void start() {
        game.loadBatchConfig();
//        gameState = GameState.HIDDEN_CARD;

        GameScreen gameScreen = new GameScreen(game);
        Screens.setGameScreen(gameScreen);
        game.setScreen(gameScreen);
    }

    /**
     * @return -1: comp win, 1: human win, 0: no winner
     */
    // todo letzte karte gleichstand: netral deck size noch 0..
    private int checkWin() {
        if (CardDeck.getHumanDeck().size() == 0 && CardDeck.getComputerDeck().size() > 0) {
            return -1;
        }
        if (CardDeck.getComputerDeck().size() == 0 && CardDeck.getHumanDeck().size() > 0) {
            return 1;
        }

        if (game.getSettings().isWinThroughRounds()) {
            if (currentRound > game.getSettings().getAmountOfRounds()) {
                if (CardDeck.getComputerDeck().size() > CardDeck.getHumanDeck().size()) {
                    return -1;
                } else if (CardDeck.getComputerDeck().size() < CardDeck.getHumanDeck().size()) {
                    return 1;
                }
            }
        } else {
            if (secondsToGo < 0) {
                if (CardDeck.getComputerDeck().size() > CardDeck.getHumanDeck().size()) {
                    return -1;
                } else if (CardDeck.getComputerDeck().size() < CardDeck.getHumanDeck().size()) {
                    return 1;
                }
            }
        }
        return 0;
    }

    public void testForGameOver() {
        switch (checkWin()) {
            case 1:
                WinningScreen humanWinScreen = new WinningScreen(game, 1);
                game.setScreen(humanWinScreen);
                break;
            case -1:
                WinningScreen computerWinScreen = new WinningScreen(game, -1);
                game.setScreen(computerWinScreen);
                break;
            case 0:
                game.getGameHandler().nextState();
                break;
        }
    }
}
