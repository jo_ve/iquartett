package de.iquartett;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.iquartett.Database.Database;
import de.iquartett.Logic.GameData.Settings;
import de.iquartett.Parser.CardDeck.BatchConfig;
import de.iquartett.Parser.Config;
import de.iquartett.Parser.StoreService;
import de.iquartett.Parser.StoreServiceGenerator;
import de.iquartett.Screens.MainMenuScreen;
import de.iquartett.Screens.Screens;


public class QuartettGame extends Game {

    public final static boolean DEBUG = false;

    public SpriteBatch batch;
    public BitmapFont font;
    private Skin skin;
    private GameHandler gameHandler;
    private BatchConfig batchConfig;
    private StoreService storeService;
    private Database database;
    private Settings settings;

    public QuartettGame() {
    }

    public Skin getSkin() {
        return skin;
    }

    public void create() {
        Gdx.input.setCatchBackKey(true);
        database = new Database(this);
//        if (database.loadObject(Settings.class, "settings.db") == null) {
        if (database.loadObject(1, "settings.db") == null) {
            settings = new Settings();
        } else {
//            settings = (Settings) database.loadObject(Settings.class, "settings.db");
            settings = (Settings) database.loadObject(1, "settings.db");
        }

        storeService = StoreServiceGenerator.createService(StoreService.class, "Basic c3R1ZGVudDphZm1iYQ==");

        final FileHandle[] f = Gdx.files.external("iQuartett/raw/").list();

        boolean deckExists = false;
        for (FileHandle aF : f) {
            if (settings.getDeckName().toLowerCase().equals(aF.name())) {
                deckExists = true;
                break;
            }
        }
        if (!deckExists) {
            FileHandle from = Gdx.files.internal("decks/tuning");
            from.copyTo(Gdx.files.external("iQuartett/raw"));
        }

        // load default batchConfig
        loadBatchConfig();
        batch = new SpriteBatch();
        font = new BitmapFont(Gdx.files.internal("game.fnt"));
        //skin = new Skin(Gdx.files.internal("neon/skin/neon-ui.json"), new TextureAtlas("neon/skin/neon-ui.atlas"));
        skin = new Skin(Gdx.files.internal("comic/skin/comic-ui.json"), new TextureAtlas("comic/skin/comic-ui.atlas"));
        Screens.setMainMenuScreen(new MainMenuScreen(this));
        this.setScreen(Screens.getMainMenuScreen());
    }

    public void render() {
        super.render(); //important!
    }

    public void dispose() {
        batch.dispose();
        font.dispose();
        skin.dispose();
        this.getScreen().dispose();
    }

    public FileHandle getDecksPath() {
        return Gdx.files.external("iQuartett/raw/");
    }

    public FileHandle getCurrentDeckPath() {
        return Gdx.files.external(getDecksPath() + "/" + getBatchConfig().getName() + "/");
    }

    public Database getDatabase() {
        return database;
    }

    public Settings getSettings() {
        return settings;
    }

    public GameHandler getGameHandler() {
        return gameHandler;
    }

    public void setGameHandler(GameHandler gameHandler) {
        this.gameHandler = gameHandler;
    }

    public BatchConfig getBatchConfig() {
        return batchConfig;
    }

    public void loadBatchConfig() {
        // todo default values for null settings
        FileHandle handle = Gdx.files.external("iQuartett/raw/" + settings.getDeckName() + "/" + settings.getDeckName() + ".json");
        if (!handle.exists()) {
            System.out.println(settings.getDeckName() + " does not exist, falling back to tuning");
            settings.setDeckName("tuning");
            settings.setDeckID(0);
        }
        FileHandle batchhandle = Gdx.files.external("iQuartett/raw/" + settings.getDeckName() + "/" + settings.getDeckName() + ".json");
        //System.out.println("SETTINGS GOT " + settings.getDeckName());
        batchConfig = Config.decodeConfData(batchhandle.read(), BatchConfig.class);
    }

    public StoreService getStore() {
        return storeService;
    }
}