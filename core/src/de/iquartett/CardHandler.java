package de.iquartett;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;

import de.iquartett.Card.CardDeck;
import de.iquartett.Card.CardSpriteDeck;
import de.iquartett.Card.CardViewManager;
import de.iquartett.Parser.CardDeck.Card;

public class CardHandler extends CardViewManager {

    private GameHandler gameHandler;
    private ArrayList<Card> spawned_deck;
    private ArrayList<Card> shuffled_deck;
    private Iterator<Card> iterator;
    private boolean toggle = false;
    private boolean shuffle_prepare = false;
    private int shuffle_ct = 0;

    public CardHandler(GameHandler gameHandler, TextureAtlas textureAtlas) {
        super(gameHandler.getBatchConfig().getCards().size(), textureAtlas, gameHandler.getGame());
        this.gameHandler = gameHandler;
    }

    // todo
    // - CardViewManager umbenennen
    // - CardSpriteDeck in CardViewManager rein mergen

    public void spawnDeck() {
        spawned_deck = new ArrayList<Card>(gameHandler.getBatchConfig().getCards());
        super.spawnDeck(spawned_deck);
    }

    public void shuffleReset() {
        shuffle_prepare = false;
        shuffle_ct = 0;
    }

    public boolean shuffle(boolean finished) {
        if (!shuffle_prepare) {
            shuffled_deck = new ArrayList<Card>(spawned_deck);
            Collections.shuffle(shuffled_deck);
            super.prepare_shuffle(spawned_deck);
            shuffle_prepare = true;
        }

        if (super.getActions().isFinished("prepare")) {
            //shuffling = true;
            if (shuffle_ct < shuffled_deck.size()) {
                if (super.getActions().isFinished("shuffle" + (shuffle_ct - 1)) || (shuffle_ct - 1) == -1) {
                    Card card = shuffled_deck.get(shuffle_ct);
                    super.shuffle(shuffle_ct, card);
                    shuffle_ct++;
                }
            } else {
                if (super.getActions().isFinished("shuffle" + (shuffle_ct - 1))) {
                    if (finished) {
                        Collections.reverse(shuffled_deck);
                        iterator = shuffled_deck.iterator();
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public void deal() {
        if (iterator.hasNext()) {
            Card card = iterator.next();
            if (this.toggle ^= true) {
                CardDeck.getHumanDeck().add(card);
            } else {
                CardDeck.getComputerDeck().add(card);
            }
            super.deal(card, this.toggle);
        } else {
            // todo bug: human and computer decks are in wrong order due to backwards iteration (?), fix reversion in other way before so no reverse here is necessary
            ArrayList<Card> list = new ArrayList<Card>(CardDeck.getHumanDeck());
            Collections.reverse(list);
            CardDeck.setHumanDeck(new LinkedHashSet<Card>(list));

            ArrayList<Card> list2 = new ArrayList<Card>(CardDeck.getComputerDeck());
            Collections.reverse(list2);
            CardDeck.setComputerDeck(new LinkedHashSet<Card>(list2));

            gameHandler.setDealt();
        }
    }

    @Override
    public void turnUpperCard() {
        super.turnUpperCard();
        gameHandler.nextState();
    }

    public ArrayList<Card> handleRoundResult() {
        ArrayList<Card> played_cards = new ArrayList<Card>();

        played_cards.add(CardDeck.getHumanDeck().next());
        played_cards.add(CardDeck.getComputerDeck().next());

        super.handleRoundResult(played_cards);
        gameHandler.nextState();

        return played_cards;
    }

    public void cardsToStack(int winnerResult, ArrayList<Card> played_cards) {
        ArrayList<Card> gui_cards = new ArrayList<Card>(CardDeck.getNeutralDeck());
        gui_cards.addAll(played_cards);
        int ct_neutral = CardDeck.getNeutralDeck().size();

        //System.out.println("Neutral cards: " + ct_neutral);

        super.cardsToStack(winnerResult, gui_cards, ct_neutral);

        ArrayList<Card> neutral_temp = new ArrayList<Card>(CardDeck.getNeutralDeck());
        ArrayList<Card> adding = new ArrayList<Card>(played_cards);

        if (winnerResult != 0 /*&& CardDeck.getNeutralDeck().size() > 0*/) {
            CardDeck.getNeutralDeck().clear();
        }
        switch (winnerResult) {
            case 1:
                //Collections.reverse(neutral_temp);
                neutral_temp.addAll(adding);
                CardDeck.getHumanDeck().addAll(neutral_temp);
                System.out.println("Human won");
                break;
            case 0:
                //Collections.reverse(adding);
                CardDeck.getNeutralDeck().addAll(adding);
                System.out.println("Neutral won");
                break;
            case -1:
                //Collections.reverse(neutral_temp);
                neutral_temp.addAll(adding);
                CardDeck.getComputerDeck().addAll(neutral_temp);
                System.out.println("Computer won");
                break;
        }

        System.out.println(CardDeck.getHumanDeck().size() + " : " + CardDeck.getComputerDeck().size() + " : " + CardDeck.getNeutralDeck().size());

        //if (QuartettGame.DEBUG) {
        System.out.print("HUMAN DECK: ");
        ArrayList<Card> human_clone = new ArrayList<Card>(CardDeck.getHumanDeck());
        for (Card c : human_clone) {
            System.out.print(c.getId() + ", ");
        }

        System.out.print("COMPUTER DECK: ");
        ArrayList<Card> computer_clone = new ArrayList<Card>(CardDeck.getComputerDeck());
        for (Card c : computer_clone) {
            System.out.print(c.getId() + ", ");
        }

        System.out.print("NEUTRAL DECK: ");
        ArrayList<Card> neutral_clone = new ArrayList<Card>(CardDeck.getNeutralDeck());
        for (Card c : neutral_clone) {
            System.out.print(c.getId() + ", ");
        }

        System.out.println();
        //}

        if (CardDeck.getHumanDeck().size() + CardDeck.getNeutralDeck().size() + CardDeck.getComputerDeck().size() < gameHandler.getBatchConfig().getCards().size()) {
            throw new IllegalStateException("Some cards are missing :(");
        }
        if (CardDeck.getHumanDeck().size() + CardDeck.getNeutralDeck().size() + CardDeck.getComputerDeck().size() > gameHandler.getBatchConfig().getCards().size()) {
            throw new IllegalStateException("Some cards are duplicate :(");
        }

        gameHandler.getGameLogic().removeActivePowerUps();
        gameHandler.setCurrentRound(gameHandler.getCurrentRound() + 1);
    }


    public void repositionCards(CardSpriteDeck cardSpriteDeck) {
        super.repositionCards(cardSpriteDeck);
    }

    public CardSpriteDeck getCardSpriteDeck() {
        return super.deck;
    }
}
