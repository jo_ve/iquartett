package de.iquartett.Downloader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import de.iquartett.Parser.DeckStore.Attribute;
import de.iquartett.Parser.DeckStore.Card;
import de.iquartett.Parser.DeckStore.Deck;
import de.iquartett.Parser.DeckStore.FullCardDeck;
import de.iquartett.Parser.DeckStore.Image;
import de.iquartett.Parser.ParserBridge;
import de.iquartett.QuartettGame;
import de.iquartett.Screens.Screens;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeckDownloader {

    private QuartettGame game;
    private Deck deck;
    private List<Card> cards;
    private float progress = 0;
    private boolean finished;
    private BlockingQueue<Integer> imgblock;
    private BlockingQueue<Integer> attrblock;

    public DeckDownloader(QuartettGame game, Deck deck) {
        this.game = game;
        this.deck = deck;
        cards = null;
    }

    public void download() {
        new Thread(new Runnable() {
            private BlockingQueue<List<Card>> downloadCards() {
                Call<List<Card>> cardscall = game.getStore().getCards(deck.getId());
                final BlockingQueue<List<Card>> blockingQueue = new ArrayBlockingQueue<List<Card>>(1);

                cardscall.enqueue(new Callback<List<Card>>() {
                    @Override
                    public void onResponse(Call<List<Card>> call, Response<List<Card>> response) {
                        List<Card> cards = response.body();
                        assert cards != null;
                        blockingQueue.add(cards);
                    }

                    @Override
                    public void onFailure(Call<List<Card>> call, Throwable throwable) {
                        System.err.println(throwable);
                        Screens.getDownloadScreen().killDownload(deck);
                    }
                });
                return blockingQueue;
            }

            private void download_image(final List<Image> images, final int finalI) {
                ImageDownloader imageDownloader = new ImageDownloader();

                // If no images are present for this card (can this even happen at this point?)
                if (0 >= images.size()) {
                    imgblock.remove(finalI);
                    imgblock.notify();
                }

                for (int i = 0; i < images.size(); i++) {
                    final Image image_obj = images.get(i);
                    ResultReturner rr = new ResultReturner() {
                        @Override
                        public void handleResult(int id, Pixmap pixmap) {
                            // todo this should be synchronized for **ALL** downloading threads with a lock, especially PixmapIO.writePNG should be synchronized!
                            synchronized (this) { // this only synchronizes for method (handleResult here), also synchronizing for download_image is not enough, as it is called inside a thread
                                // Save image
                                String image_url = image_obj.getImage().substring(image_obj.getImage().lastIndexOf('/') + 1);
                                FileHandle to = Gdx.files.external(game.getDecksPath() + "/" + deck.getName().toLowerCase() + "/img/");
                                to.file().mkdirs();
                                PixmapIO.writePNG(to.child(image_url), pixmap);
                                pixmap.dispose();
                                if (id == images.size() - 1) {
                                    synchronized (imgblock) {
                                        imgblock.remove(finalI);
                                        imgblock.notify();
                                    }
                                }
                            }
                        }
                    };
                    String url_backup = image_obj.getImage();
                    String filename = url_backup.substring(image_obj.getImage().lastIndexOf('/') + 1);
                    image_obj.setImage(filename);
                    imageDownloader.download_image(i, url_backup, rr);
                }
            }

            /*public void downloadImage() {
                ImageDownloader downloader = new ImageDownloader(decks, items);
                downloader.download_deck_images();
            }*/

            @Override
            public void run() {
                BlockingQueue<List<Card>> blockingQueue = downloadCards();
                try {
                    // This is blocking!
                    cards = blockingQueue.take();
                    imgblock = new ArrayBlockingQueue<>(cards.size());
                    attrblock = new ArrayBlockingQueue<>(cards.size());
                } catch (
                        InterruptedException e) {
                    e.printStackTrace();
                }

                for (int i = 0; i < cards.size(); i++) {
                    final Card card = cards.get(i);
                    final int finalI = i;

                    Call<List<Attribute>> attributescall = game.getStore().getAttributes(deck.getId(), card.getId());
                    Call<List<Image>> imagescall = game.getStore().getImages(deck.getId(), card.getId());

                    synchronized (imgblock) {
                        imgblock.add(finalI);
                    }

                    synchronized (attrblock) {
                        attrblock.add(finalI);
                    }

                    attributescall.enqueue(new Callback<List<Attribute>>() {
                        @Override
                        public void onResponse(Call<List<Attribute>> call, Response<List<Attribute>> response) {
                            List<Attribute> attributes = response.body();
                            assert attributes != null;
                            card.setAttributes(attributes);
                            synchronized (attrblock) {
                                attrblock.remove(finalI);
                                attrblock.notify();
                            }
                        }

                        @Override
                        public void onFailure(Call<List<Attribute>> call, Throwable throwable) {
                            System.err.println(throwable);
                            Screens.getDownloadScreen().killDownload(deck);
                        }
                    });

                    imagescall.enqueue(new Callback<List<Image>>() {
                        @Override
                        public void onResponse(Call<List<Image>> call, Response<List<Image>> response) {
                            List<Image> images = response.body();
                            assert images != null;
                            card.setImages(images);

                            // Download deck images
                            download_image(images, finalI);
                        }

                        @Override
                        public void onFailure(Call<List<Image>> call, Throwable throwable) {
                            System.out.println(throwable);
                            Screens.getDownloadScreen().killDownload(deck);
                        }
                    });

                    /*try {
                        // This is blocking!
                        System.out.println("Waiting for card: " + i);
                        attributes = attrblock.take();
                        images = imgblock.take();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (attributes == null || images == null) {
                        throw new IllegalStateException();
                    }*/
                }

                synchronized (attrblock) {
                    while (!attrblock.isEmpty()) {
                        try {
                            attrblock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.out.println("attrblock: " + attrblock);
                        Screens.getDownloadScreen().setDownloading(true);
//                        float prog = cards.size() - attrblock.size();
//                        progress = prog / cards.size();
//                        System.out.println(prog + " / " + cards.size() + " = " + progress);

                        progress = progress + 1f / cards.size();
//                        System.out.println(prog + " / " + cards.size() + " = " + progress);
                        System.out.println(cards.size() + " , " + progress);
                    }
                }

                synchronized (imgblock) {
                    while (!imgblock.isEmpty()) {
                        try {
                            imgblock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.out.println("imgblock: " + imgblock);

//                        float prog = cards.size() - imgblock.size();
//                        progress = prog / cards.size();
                        Screens.getDownloadScreen().setDownloading(true);
                        progress = progress + 1f / cards.size();
//                        System.out.println(prog + " / " + cards.size() + " = " + progress);
                        System.out.println(cards.size() + " , " + progress);

                    }
                }

                System.out.println("Finished downloading deck " + deck.getName());
                finished = true;
                Screens.getDownloadScreen().setDownloading(false);

                FullCardDeck fullCardDeck = new FullCardDeck(deck, cards);

                ParserBridge parserBridge = new ParserBridge(game, fullCardDeck);
                parserBridge.parseFullCardDeck();

            }
        }).start();
    }

    public float getProgress() {
        return progress;
    }

    public boolean isFinished() {
        return finished;
    }

    public int getDeckSize() {
        if (cards != null) {
            return cards.size();
        } else return 0;
    }
}
