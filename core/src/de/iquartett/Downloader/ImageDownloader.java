package de.iquartett.Downloader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.SnapshotArray;
import com.badlogic.gdx.utils.StreamUtils;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import de.iquartett.Parser.DeckStore.Deck;

interface ResultReturnHandler {
    void handleResult(int id, Pixmap pixmap);
}

public class ImageDownloader {


    public ImageDownloader() {
    }

    public static Image pixToImage(Pixmap pixmap) {
        return new Image(new TextureRegion(new Texture(pixmap)));
    }

    public void download_deck_images(List<Deck> decks, final Table items) {
        for (int i = 0; i < decks.size(); i++) {
            final ResultReturner imageCallback = new ResultReturner() {
                @Override
                public void handleResult(int id, Pixmap pixmap) {
                    final SnapshotArray<Actor> table_childrens = items.getChildren();
                    Image image = pixToImage(pixmap);
                    for (int k = 0; k < table_childrens.size; k++) {
                        Actor table_actor = table_childrens.get(k);
                        if (table_actor.getName() != null && table_actor.getName().equals("image" + id)) {
                            Image img = (Image) table_actor;
                            img.setDrawable(image.getDrawable());
                            table_childrens.set(k, img);
                            break;
                        }
                    }
                }
            };
            download_image(i, decks.get(i).getImage(), imageCallback);
        }
    }

    public void download_image(final int id, final String url, final ResultReturner imageCallback) {
        new Thread(new Runnable() {

            /** Downloads the content of the specified url to the array. The array has to be big enough. */
            private int download(byte[] out, String url) {
                InputStream in = null;
                try {
                    HttpURLConnection conn;
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(false);
                    conn.setUseCaches(true);
                    conn.connect();
                    in = conn.getInputStream();
                    int readBytes = 0;
                    while (true) {
                        int length = in.read(out, readBytes, out.length - readBytes);
                        if (length == -1) break;
                        readBytes += length;
                    }
                    return readBytes;
                } catch (Exception ex) {
                    System.out.println("Image downloader failed: " + ex);
                    return 0;
                } finally {
                    StreamUtils.closeQuietly(in);
                }
            }

            @Override
            public void run() {
                byte[] bytes = new byte[1000 * 1024]; // assuming the content is not bigger than 1000kb.
                int numBytes = download(bytes, url);
                if (numBytes != 0) {
                    // load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
                    final Pixmap pixmap = new Pixmap(bytes, 0, numBytes);
                    final int originalWidth = pixmap.getWidth();
                    final int originalHeight = pixmap.getHeight();
                    int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
                    int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
                    final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
                    potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                    pixmap.dispose();
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Pixmap pixmap = new Pixmap(originalWidth, originalHeight, potPixmap.getFormat());
                            pixmap.drawPixmap(potPixmap, 0, 0, 0, 0, originalWidth, originalHeight);
                            potPixmap.dispose();
                            imageCallback.handleResult(id, pixmap);
                            /*
                            // why not working?
                            for (int i = 0; i < items.size(); i++) {
                                ListItem item = items.get(i);
                                if (item.getId() == decks.get(imagecount).getId()) {
                                    itemRow.get(i).setPreview_image(image);
                                    System.out.println("image was set " + imagecount);
                                    break;
                                }
                            }*/

                        }
                    });
                } else {
                    System.err.println("Image downloader returned 0 bytes for: " + url);
                    // Image has 0 bytes don't replace default
                }
            }
        }).start();
    }
}

class ResultReturner implements ResultReturnHandler {
    public void handleResult(int id, Pixmap pixmap) {
    }
}