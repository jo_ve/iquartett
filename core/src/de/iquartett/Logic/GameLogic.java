package de.iquartett.Logic;

import de.iquartett.Card.CardDeck;
import de.iquartett.Logic.GameData.PowerUps;
import de.iquartett.Logic.PlayerData.Player;
import de.iquartett.Logic.PlayerData.PlayerEnum;
import de.iquartett.QuartettGame;

/**
 * Class for calculating the game
 */
public class GameLogic {

    private QuartettGame game;

    private boolean skillDrainActive = false;

    public GameLogic(QuartettGame quartettGame) {
        this.game = quartettGame;
    }

    public int compareCategories(int categoryId) {
        float human_value = CardDeck.getHumanDeck().peek().getCategories().get(categoryId).getValue();
        float computer_value = CardDeck.getComputerDeck().peek().getCategories().get(categoryId).getValue();
        boolean higherWins = game.getGameHandler().getBatchConfig().getProperties().get(categoryId).higherWins();
        int humanBigger;
        if (!skillDrainActive) {
            humanBigger = Float.compare(human_value, computer_value);
        } else {
            if (higherWins) {
                humanBigger = Float.compare(human_value, computer_value / 2);
            } else {
                humanBigger = Float.compare(human_value, computer_value * 2);
            }
        }
        skillDrainActive = false;

//        if (game.getGameHandler().getPlayerHandler().getHuman().getActivePowerUps().contains(PowerUps.UNSTOPPABLE)) {
//            game.getGameHandler().setCurrentTurn(PlayerEnum.HUMAN);
//        } else if (game.getGameHandler().getPlayerHandler().getAi().getActivePowerUps().contains(PowerUps.UNSTOPPABLE)) {
//            game.getGameHandler().setCurrentTurn(PlayerEnum.AI);
//        } else {
//            game.getGameHandler().setCurrentTurn(game.getGameHandler().getCurrentTurn());
//        }


        switch (humanBigger) {
            case 1:
                if (higherWins) {
                    // human has won
                    game.getGameHandler().setCurrentTurn(PlayerEnum.HUMAN);
                    return 1;
                } else {
                    // computer has won
                    game.getGameHandler().setCurrentTurn(PlayerEnum.AI);
                    return -1;
                }
            case -1:
                if (higherWins) {
                    // ai has won
                    game.getGameHandler().setCurrentTurn(PlayerEnum.AI);
                    return -1;
                } else {
                    // human has won
                    game.getGameHandler().setCurrentTurn(PlayerEnum.HUMAN);
                    return 1;
                }
            default:
                // draw
                return humanBigger;
        }
    }

    public void testForPowerUps() {
        evaluatePowerUps(game.getGameHandler().getPlayerHandler().getHuman());
        evaluatePowerUps(game.getGameHandler().getPlayerHandler().getAi());
    }

    private void evaluatePowerUps(Player player) {
        for (PowerUps powerUp : player.getActivePowerUps()) {
            powerUp.runLogic(game, player);
        }
    }

    public void removeActivePowerUps() {
        for (PowerUps powerUps : game.getGameHandler().getPlayerHandler().getHuman().getActivePowerUps()) {
            game.getGameHandler().getPlayerHandler().getHuman().removeActivePowerUp(powerUps);
        }
        for (PowerUps powerUps : game.getGameHandler().getPlayerHandler().getAi().getActivePowerUps()) {
            game.getGameHandler().getPlayerHandler().getAi().removeActivePowerUp(powerUps);
        }
    }

    public boolean isSkillDrainActive() {
        return skillDrainActive;
    }

    public void setSkillDrainActive(boolean skillDrainActive) {
        this.skillDrainActive = skillDrainActive;
    }
}
