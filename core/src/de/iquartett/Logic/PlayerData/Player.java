package de.iquartett.Logic.PlayerData;

import java.util.EnumSet;

import de.iquartett.Logic.GameData.PowerUps;
import de.iquartett.QuartettGame;

public abstract class Player implements java.io.Serializable {
    private static final long serialVersionUID = 10L;
    private EnumSet<PowerUps> powerUps = EnumSet.allOf(PowerUps.class);
    private String name;
    public transient QuartettGame game;
    private EnumSet<PowerUps> active_powerups = EnumSet.noneOf(PowerUps.class);
    private boolean isHuman;

    Player(String name, QuartettGame game, boolean isHuman) {
        this.name = name;
        this.game = game;
        EnumSet<PowerUps> powerUps = EnumSet.allOf(PowerUps.class);
        EnumSet<PowerUps> active_powerups = EnumSet.noneOf(PowerUps.class);
        this.isHuman = isHuman;
    }

    public EnumSet<PowerUps> getRemainingPowerUps() {
        return powerUps;
    }

    public EnumSet<PowerUps> getActivePowerUps() {
        return active_powerups;
    }

    public String getName() {
        return name;
    }

    public boolean usePowerUp(PowerUps powerUp) {
        if (!powerUps.contains(powerUp) || (active_powerups != null && active_powerups.contains(powerUp))) {
            return false;
        }
        active_powerups.add(powerUp);
        removePowerUp(powerUp);
        return true;
    }

    public boolean isHuman() {
        return this.isHuman;
    }

    private void removePowerUp(PowerUps powerUp) {
        powerUps.remove(powerUp);
    }

    public void removeActivePowerUp(PowerUps powerUp) {
        active_powerups.remove(powerUp);
    }

    public boolean isPowerupActive(PowerUps powerUp) {
        return active_powerups.contains(powerUp);
    }
}
