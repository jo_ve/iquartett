package de.iquartett.Logic.PlayerData;

public enum PlayerEnum implements java.io.Serializable {
    HUMAN,
    AI;

    private int value;

    PlayerEnum() {
        this.value = ordinal();
    }

    /* should not be called directly only via GameScreen method */
    public PlayerEnum nextPlayer() {
        return PlayerEnum.values()[(value + 1) % PlayerEnum.values().length];
    }
}
