package de.iquartett.Logic.PlayerData;

import de.iquartett.QuartettGame;

public class Human extends Player implements java.io.Serializable {
    private static final long serialVersionUID = 12L;

    public Human(QuartettGame game) {
        super("Player 1", game, true);
    }
}
