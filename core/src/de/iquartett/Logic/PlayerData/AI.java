package de.iquartett.Logic.PlayerData;


import java.util.ArrayList;
import java.util.Collections;

import de.iquartett.Card.CardDeck;
import de.iquartett.Logic.GameData.PowerUps;
import de.iquartett.Parser.CardDeck.Card;
import de.iquartett.Parser.CardDeck.Category;
import de.iquartett.QuartettGame;

public class AI extends Player implements java.io.Serializable {
    private static final long serialVersionUID = 11L;
    private ArrayList<ArrayList<Float>> values = new ArrayList<ArrayList<Float>>();
    private ArrayList<Card> allCards = new ArrayList<Card>();
    float percentageFromMax = 0.0f;

    public AI(QuartettGame game) {
        super("iQuartett AI", game, false);

    }

    public int calculateCategory(Card cardAI) {
        int currentCat = 0;
        initialCalculation();
//        usePowerUp(calculatePowerUp());

        // no more cards available
        if (cardAI == null) {
            currentCat = (int) (Math.random() * 6);
            // normal calculation
        } else {
            for (Category category : cardAI.getCategories()) {
                if (super.game.getBatchConfig().getProperties().get(category.getPropertyId()).higherWins()) {
                    if (category.getValue() / values.get(category.getPropertyId()).get(0) > percentageFromMax) {
                        percentageFromMax = category.getValue() / values.get(category.getPropertyId()).get(0);
                        currentCat = category.getPropertyId();
                    }
                } else {
                    if (values.get(category.getPropertyId()).get(0) / category.getValue() > percentageFromMax) {
                        percentageFromMax = category.getValue() / values.get(category.getPropertyId()).get(0);
                        currentCat = category.getPropertyId();
                    }
                }

            }
        }
        PowerUps powerUp = calculatePowerUp();
        if (powerUp != null) {
            usePowerUp(powerUp);
        }
        return currentCat;
    }

    private void initialCalculation() {
        allCards.addAll(CardDeck.getComputerDeck());
        allCards.addAll(CardDeck.getHumanDeck());

        for (Category ignored : CardDeck.getHumanDeck().peek().getCategories()) {
            values.add(new ArrayList<Float>());
        }
        for (Card card : allCards) {
            for (int i = 0; i < card.getCategories().size(); i++) {
                values.get(i).add(card.getCategories().get(i).getValue());
            }
        }
        for (ArrayList arrayList : values) {
            Collections.sort(arrayList, Collections.reverseOrder());
        }
    }

    private PowerUps calculatePowerUp() {
        int powerUpNmbr = -1;
        if (game.getGameHandler().getPlayerHandler().getAi().getRemainingPowerUps().size() > 0) {
            if (percentageFromMax < 0.67) {
                powerUpNmbr = (int) (Math.random() * 2);
            }
            switch (powerUpNmbr) {
                case -1:
                    break;
                case 0:
//                    System.out.println("KI USED POWERUP SHIELD");
                    return PowerUps.SHIELD;
                case 1:
//                    System.out.println("KI USED POWERUP UNSTOPPABLE");
                    return PowerUps.UNSTOPPABLE;
//            case 2:
//                System.out.println("KI USED POWERUP SKILL_DRAIN");
//                return PowerUps.SKILL_DRAIN;
            }
        }
        return null;
    }
}
