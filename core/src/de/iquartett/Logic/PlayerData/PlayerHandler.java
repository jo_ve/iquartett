package de.iquartett.Logic.PlayerData;

public class PlayerHandler {

    private Human human;
    private AI ai;

    public PlayerHandler(Human human, AI ai) {
        this.human = human;
        this.ai = ai;
    }


    public Human getHuman() {
        return human;
    }

    public AI getAi() {
        return ai;
    }
}
