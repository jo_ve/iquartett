package de.iquartett.Logic.GameData;

/**
 * Enum containing all possible Phases
 */
public enum GameState implements java.io.Serializable {
    HIDDEN_CARD,
    TURNED_CARD,
    SELECT_SCREEN,
    COMPARE_SCREEN;

    private int value;

    GameState() {
        this.value = ordinal();
    }

    /* should not be called directly only via GameScreen method */
    public GameState nextState() {
        return GameState.values()[(value + 1) % GameState.values().length];
    }

    /* should not be called directly only via GameScreen method */
    public GameState previousState() {
        return GameState.values()[(value - 1) % GameState.values().length];
    }
}