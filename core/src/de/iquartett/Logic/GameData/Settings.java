package de.iquartett.Logic.GameData;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public class Settings implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    private float placeholderVolume = 50;
    private Boolean placeholderAnimations = true;
    private String placeholderDeckName = "tuning";
    private boolean placeholderWinThroughRounds = true;
    private int placeholderAmountOfTimeInMin = 1;
    private int placeholderAmountOfRounds = 15;

    private float volume = 50;
    private Boolean animations = true;
    private String deckName = "tuning";
    private int deckID;
    private boolean winThroughRounds = true;
    private int amountOfTimeInMin = 1;
    private int amountOfRounds = 15;

    private static float volumePlaceholder = 50;

    private static Sound roundWon = Gdx.audio.newSound(Gdx.files.internal("sounds/round_won.mp3"));
    private static Sound roundLost = Gdx.audio.newSound(Gdx.files.internal("sounds/round_lost.mp3"));
    private static Sound gameWon = Gdx.audio.newSound(Gdx.files.internal("sounds/game_won.mp3"));
    private static Sound gameLost = Gdx.audio.newSound(Gdx.files.internal("sounds/game_lost.mp3"));
    private static Sound cardFlip = Gdx.audio.newSound(Gdx.files.internal("sounds/card_flip.mp3"));

    public Settings() {
    }

    public static Sound getRoundWon() {
        return roundWon;
    }

    public static Sound getRoundLost() {
        return roundLost;
    }

    public static Sound getGameWon() {
        return gameWon;
    }

    public static Sound getGameLost() {
        return gameLost;
    }

    public static Sound getCardFlip() {
        return cardFlip;
    }

    public static float getVolumePlaceholder() {
        return volumePlaceholder;
    }

    public static void setVolumePlaceholder(float volumePlaceholder) {
        Settings.volumePlaceholder = volumePlaceholder;
    }


    public void setVolume(float volume) {
        this.volume = volume;
    }

    public void setAnimations(Boolean animations) {
        this.animations = animations;
    }

    public void setDeckName(String deckName) {
        this.deckName = deckName;
    }

    public void setDeckID(int deckID) {
        this.deckID = deckID;
    }

    public float getVolume() {
        return volume;
    }

    public Boolean getAnimations() {
        return animations;
    }

    public String getDeckName() {
        return deckName;
    }

    public int getDeckID() {
        return deckID;
    }

    public boolean isWinThroughRounds() {
        return winThroughRounds;
    }

    public void setWinThroughRounds(boolean winThroughRounds) {
        this.winThroughRounds = winThroughRounds;
    }

    public int getAmountOfTimeInMin() {
        return amountOfTimeInMin;
    }

    public void setAmountOfTimeInMin(int amiuntOfTime) {
        this.amountOfTimeInMin = amiuntOfTime;
    }

    public int getAmountOfRounds() {
        return amountOfRounds;
    }

    public void setAmountOfRounds(int amountOfRounds) {
        this.amountOfRounds = amountOfRounds;
    }

    public float getPlaceholderVolume() {
        return placeholderVolume;
    }

    public void setPlaceholderVolume(float placeholderVolume) {
        this.placeholderVolume = placeholderVolume;
    }

    public Boolean getPlaceholderAnimations() {
        return placeholderAnimations;
    }

    public void setPlaceholderAnimations(Boolean placeholderAnimations) {
        this.placeholderAnimations = placeholderAnimations;
    }

    public String getPlaceholderDeckName() {
        return placeholderDeckName;
    }

    public void setPlaceholderDeckName(String placeholderDeckName) {
        this.placeholderDeckName = placeholderDeckName;
    }

    public boolean isPlaceholderWinThroughRounds() {
        return placeholderWinThroughRounds;
    }

    public void setPlaceholderWinThroughRounds(boolean placeholderWinThroughRounds) {
        this.placeholderWinThroughRounds = placeholderWinThroughRounds;
    }

    public int getPlaceholderAmountOfTimeInMin() {
        return placeholderAmountOfTimeInMin;
    }

    public void setPlaceholderAmountOfTimeInMin(int placeholderAmountOfTimeInMin) {
        this.placeholderAmountOfTimeInMin = placeholderAmountOfTimeInMin;
    }

    public int getPlaceholderAmountOfRounds() {
        return placeholderAmountOfRounds;
    }

    public void setPlaceholderAmountOfRounds(int placeholderAmountOfRounds) {
        this.placeholderAmountOfRounds = placeholderAmountOfRounds;
    }
}
