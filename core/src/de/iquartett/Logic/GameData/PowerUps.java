package de.iquartett.Logic.GameData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.iquartett.Card.CardDeck;
import de.iquartett.Logic.PlayerData.Player;
import de.iquartett.Logic.PlayerData.PlayerEnum;
import de.iquartett.QuartettGame;
import de.iquartett.Screens.Screens;

public enum PowerUps {


    //    DOUBLE_PICK { // Player chooses two categories if one wins he wins
//
//        @Override
//        public void runLogic(QuartettGame game, Player player) {
//
//        }
//    },
    SHIELD { // If the Player looses, the game handles it like a draw FINISHED

        @Override
        public void runLogic(QuartettGame game, Player player) {

            if (Screens.getGameScreen().getCompareResult() != 1 && player.isHuman()) {
                Screens.getGameScreen().setCompareResult(0);
                if (Screens.getGameScreen().getLastTurn() == PlayerEnum.HUMAN) {
                    game.getGameHandler().setCurrentTurn(PlayerEnum.HUMAN);
                }
            } else if (Screens.getGameScreen().getCompareResult() != -1 && !player.isHuman()) {
                Screens.getGameScreen().setCompareResult(0);
                if (Screens.getGameScreen().getLastTurn() == PlayerEnum.AI) {
                    game.getGameHandler().setCurrentTurn(PlayerEnum.AI);
                }
            }
        }
    },
    PEEK_A_BOO { // The Player can see the Name of his opponents card FINISHED

        @Override
        public void runLogic(QuartettGame game, Player player) {
        }
    },
    SKILL_DRAIN { // 50% of the categories from the opponents card get doubled or halved (whatever makes it worse)

        @Override
        public void runLogic(QuartettGame game, Player player) {
            List<Integer> list = new ArrayList<Integer>();
            for (int i = 0; i < CardDeck.getComputerDeck().peek().getCategories().size(); i++) {
                list.add(i);
            }

            Collections.shuffle(list);
            Integer[] randomArray = list.subList(0, (CardDeck.getComputerDeck().peek().getCategories().size()) / 2).toArray(new Integer[CardDeck.getComputerDeck().peek().getCategories().size() / 2]);

            //System.out.println(Screens.getGameScreen().getCardScreen().getSelectedCategory().getId());

            for (Integer num : randomArray) {
                //System.out.println(num);
                if (Screens.getGameScreen().getCardScreen().getSelectedCategory().getId() == num) {
                    game.getGameHandler().getGameLogic().setSkillDrainActive(true);
                }
            }
            //Screens.getGameScreen().setCompareResult(game.getGameHandler().getGameLogic().compareCategories(Screens.getGameScreen().getCardScreen().getSelectedCategory().getId()));
        }
    },
    //    DOUBLEKILL { // If the Player wins this card he gets two instead of one
//
//        @Override
//        public void runLogic(QuartettGame game, Player player) {
//        }
//
//        public void runLogic2() {
//            if (Screens.getGameScreen().getCompareResult() == 1) {
//
//            }
//        }
//    },
    UNSTOPPABLE { // If the Player looses this round he still chooses the next category FINISHED

        @Override
        public void runLogic(QuartettGame game, Player player) {
            if (player.isHuman()) {
                game.getGameHandler().setCurrentTurn(PlayerEnum.HUMAN);
            } else {
                game.getGameHandler().setCurrentTurn(PlayerEnum.AI);
            }
        }
    },
    //    SWITCH_A_ROO { // The Players card switches places with one random card from his deck
//
//        @Override
//        public void runLogic(QuartettGame game, Player player) {
//
//        }
//    },
//    INSTAGIB { // The opponents card is immediately sent to the draw cards
//
//        @Override
//        public void runLogic(QuartettGame game, Player player) {
//
//        }
//    },
    CLUELESS { // The best possible category is chosen for the player

        @Override
        public void runLogic(QuartettGame game, Player player) {
            // Everything happens visually so the logic is in CardScreen
        }
    };

    public abstract void runLogic(QuartettGame game, Player player);
}