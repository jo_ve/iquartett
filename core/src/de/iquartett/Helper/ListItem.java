package de.iquartett.Helper;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;

public class ListItem {

    private Table tableNameAndDescr;
    private Table allItems;
    private Label deckName;
    private Label description;
    private Image preview_image;
    private ImageButton downloadBtn;
    private ProgressBar progressBar;

    private int id;

    public ListItem(String name, Image preview_image, Drawable download_btn_img, ProgressBar progressBar, String description, int id, Skin skin) {
        this.id = id;
        this.preview_image = preview_image;
        this.progressBar = progressBar;
        tableNameAndDescr = new Table(skin);
        allItems = new Table(skin);
        this.deckName = new Label(name, skin, "white-big");
        this.description = new Label(description, skin, "white-big");

        downloadBtn = new ImageButton(download_btn_img);

        deckName.setFontScale(1.0f);
        this.description.setFontScale(0.7f);

        if (description.length() == 0) {
            tableNameAndDescr.add(deckName);
        } else {
            tableNameAndDescr.add(deckName);
            tableNameAndDescr.row();
            tableNameAndDescr.add(this.description);
        }

        allItems.add(tableNameAndDescr).expandX().center();
        allItems.add(downloadBtn).size(deckName.getHeight() * 2).align(Align.right);

    }

    public int getId() {
        return id;
    }

    public Table getListItem() {
        return this.allItems;
    }

    public Table getTableNameAndDescr() {
        return this.tableNameAndDescr;
    }

    public ImageButton getDownloadBtn() {
        return this.downloadBtn;
    }

    public Label getDeckName() {
        return deckName;
    }

    public Label getDescription() {
        return description;
    }

    public Image getPreview_image() {
        return preview_image;
    }

    public void setPreview_image(Image preview_image) {
        this.preview_image = preview_image;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }
}