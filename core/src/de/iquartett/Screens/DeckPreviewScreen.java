package de.iquartett.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.iquartett.Generator.AtlasGenerator;
import de.iquartett.Helper.SimpleDirectionGestureDetector;
import de.iquartett.Parser.CardDeck.Card;
import de.iquartett.QuartettGame;

public class DeckPreviewScreen implements Screen {

    private final QuartettGame game;
    private OrthographicCamera cam;
    float fontX;
    float fontY;

    private Sprite headerImg;
    float neww;
    float newh;
    private Stage stage;
    private Viewport viewport;
    //GlyphLayout glyphLayout;
    private Table categoryTable;
    //private FreeTypeFontGenerator generator;
    private BitmapFont font;
    private Card card;

    public DeckPreviewScreen(final QuartettGame game, Card card) {
        this.game = game;

        cam = new OrthographicCamera();
        cam.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        game.batch.setProjectionMatrix(cam.combined);
        cam.update();

        this.card = card;
        font = game.font;

        // todo implement font generator
        // generator = new FreeTypeFontGenerator(Gdx.files.internal("OpenSans-Regular.ttf"));

        viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), cam);
        viewport.apply();

        stage = new Stage(viewport, game.batch);

        AtlasGenerator atlasGenerator = new AtlasGenerator(game);

        headerImg = atlasGenerator.create_headerImg(card);
        //font = atlasGenerator.create_cardTitle(c);

        /* Scale image to whole width */
        float faktor = viewport.getWorldWidth() / headerImg.getWidth();
        neww = headerImg.getWidth() * faktor;
        newh = headerImg.getHeight() * faktor;
        float headerImg_y = viewport.getWorldHeight() - newh;

        Texture t = headerImg.getTexture();
        Image img = new Image(t);
        img.setBounds(0, headerImg_y, neww, newh);

        categoryTable = atlasGenerator.create_categories(card,
                Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight(),
                newh,
                3,
                Gdx.graphics.getHeight() / 25,
                75);

        font.getData().setScale(2);

        //glyphLayout = new GlyphLayout();
        String item = card.getName();
        if (QuartettGame.DEBUG) {
            item = String.valueOf(card.getId() + " " + item);
        }
        //glyphLayout.setText(font, item);

        // todo dont make this height static
        fontY = headerImg_y - 175;

        categoryTable.setSize(Gdx.graphics.getWidth(), fontY);

        Label cardH = new Label(item, new Label.LabelStyle(font, Color.BLACK));

        // todo use font for this

        cardH.setScale(3);

        fontX = 0 + (viewport.getWorldWidth() - cardH.getPrefWidth()) / 2;
        if (fontX < 0) fontX = 0;

        cardH.setPosition(fontX, fontY);

        //categoryTable.setTouchable(Touchable.enabled);
        //atlasGenerator.getTable().setTouchable(Touchable.enabled);
        //stage.addActor(atlasGenerator.getCat_text());
        //stage.addActor(atlasGenerator.getCat_val());
        //stage.addActor(atlasGenerator.getTable());

        //categoryTable.debug();

        stage.addActor(img);
        stage.addActor(cardH);
        stage.addActor(categoryTable);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(new SimpleDirectionGestureDetector(new SimpleDirectionGestureDetector.DirectionListener() {

            @Override
            public void onUp() {
                dispose();
                OptionsMenuScreen.cardCounter = 0;
                game.setScreen(new OptionsMenuScreen(game));
            }

            @Override
            public void onLeft() {
                dispose();
                if (OptionsMenuScreen.cardCounter < game.getBatchConfig().getCards().size() - 1) {
                    OptionsMenuScreen.cardCounter++;
                } else {
                    OptionsMenuScreen.cardCounter = 0;
                }
                game.setScreen(new DeckPreviewScreen(game, game.getBatchConfig().getCards().get(OptionsMenuScreen.cardCounter)));
            }

            @Override
            public void onRight() {
                dispose();
                if (OptionsMenuScreen.cardCounter > 0) {
                    OptionsMenuScreen.cardCounter--;
                } else {
                    OptionsMenuScreen.cardCounter = game.getBatchConfig().getCards().size() - 1;
                }
                game.setScreen(new DeckPreviewScreen(game, game.getBatchConfig().getCards().get(OptionsMenuScreen.cardCounter)));
            }

            @Override
            public void onDown() {
                dispose();
                OptionsMenuScreen.cardCounter = 0;
                game.setScreen(new OptionsMenuScreen(game));
            }
        }));
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1f, 1f, 1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        cam.update();

        //FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        //parameter.size = 10;
        //BitmapFont cardTitle2 = generator.generateFont(parameter);

        //cardTitle2.setUseIntegerPositions(false);

        /*game.batch.begin();
        Gdx.gl.glClear(GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(1f, 1, 1, 1f);

        //game.batch.draw(headerImg, 0, headerImg_y, neww, newh);
        game.font.draw(game.batch, glyphLayout, fontX, fontY);
        //categoryTable.draw(game.batch, 1f);
        game.batch.end();*/
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        cam.update();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        stage.dispose();
        //generator.dispose(); // FIXME don't forget to dispose to avoid memory leaks!
    }
}
