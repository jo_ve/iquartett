package de.iquartett.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.iquartett.QuartettGame;

public class RuleScreen implements Screen {

    private QuartettGame game;
    private OrthographicCamera cam;
    private Stage stage;
    private Table container;
    private Viewport viewport;
    private Screen prevScreen;
    Table table;
    // private ExtendViewport viewport;

    RuleScreen(QuartettGame game, Screen prevScreen) {
        this.game = game;
        this.prevScreen = prevScreen;

        cam = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.update();

        // fixme scrollbar is scratched
        viewport = new FitViewport(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 2f, cam);
        viewport.apply();

        stage = new Stage(viewport, game.batch);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);

        container = new Table();
        container.setFillParent(true);

        table = new Table();

        int padding = 100;

        // todo padding with scrolling
        table.padLeft(padding);
        table.padRight(padding);

        Image bg = new Image(new Texture(Gdx.files.internal("menu_bg.png")));
        bg.setSize(viewport.getWorldWidth(), viewport.getWorldHeight());

        TextButton goBack = new TextButton("Go back", game.getSkin());
        table.add(goBack).spaceBottom(25).colspan(2);
        table.row();

        goBack.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(prevScreen);
            }
        });

        final ScrollPane scroll = new ScrollPane(table, game.getSkin());
        scroll.setScrollingDisabled(true, false);
        //table.pad(0).defaults().expandX().space(0);
//        for (int i = 0; i < 100; i++) {
//            table.row();
//
//            Label label = new Label(i + ". Publish your games on Windows, Mac, Linux, Android, iOS, BlackBerry and HTML5, all with the same code base.", game.getSkin());
//            label.setAlignment(Align.center);
//            label.setWrap(true);
//            table.add(label).width(viewport.getWorldWidth());
//        }


        fillTable(padding);

        //container.pad(padding);

        container.add(scroll).size(viewport.getWorldWidth() - 100, viewport.getWorldHeight() - 100).expand().fill();
        //  container.debugAll();

        stage.addActor(bg);
        stage.addActor(container);
        //stage.addActor(goBack);


      /*  Table scrollableTable = new Table();
        scrollableTable.setFillParent(true);
        stage.addActor(scrollableTable);

        Table table = new Table();

        Label label = new Label("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   " +

                "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.   \n" +

                "Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.   \n" +

                "Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   \n" +

                "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.   \n" +

                "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur", game.getSkin());

        label.setAlignment(Align.center);
        //label.setWrap(true);
        //label.setWidth(table.getWidth());
       // Container container = new Container(label);
        //container.width(200);
        //Table.addActor(container);
        table.add(label).row();
        table.pack();
        table.setTransform(true);  //clipping enabled

        table.setOrigin(table.getWidth()/2,table.getHeight()/2);
        table.setScale(1.0f);

        final ScrollPane scroll = new ScrollPane(table, game.getSkin());
        scrollableTable.add(scroll).expand().fill();

        stage.setDebugAll(true);*/
    }

    @Override
    public void render(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)) {
            game.setScreen(Screens.getMainMenuScreen());
        }

        Gdx.gl.glClearColor(255, 255, 255, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // cam.update();
        //game.batch.setProjectionMatrix(cam.combined);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
//        viewport.update(width, height);
//        cam.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    private void fillTable(int padding) {
        float imageScale = viewport.getWorldWidth() / 6;

        Image shieldPic = new Image(new Texture(Gdx.files.internal("shield_rules.png")));
        shieldPic.setSize(imageScale, imageScale);

        Image peekboo = new Image(new Texture(Gdx.files.internal("peek_a_boo_rules.png")));
        peekboo.setSize(imageScale, imageScale);

        Image unstoppable = new Image(new Texture(Gdx.files.internal("unstoppable_rules.png")));
        unstoppable.setSize(imageScale, imageScale);

        Image clueless = new Image(new Texture(Gdx.files.internal("clueless_rules.png")));
        clueless.setSize(imageScale, imageScale);

        Label generalExpl = new Label("The goal is to have as many cards as possible, if not all, after the time or the rounds are over. You get the opponent's card, if the value of your category marked with \">\" is higher. Analogously for \"<\" smaller.\n\nFor confirming your selection swipe the card up. For cancel swipe down. Left / Right swipe chenges the card image, if available. In options click on the image to get a deck preview.", game.getSkin(), "title");
        generalExpl.setAlignment(Align.center);
        generalExpl.setWrap(true);
        generalExpl.setFontScale(0.7f);

        Label shieldExpl = new Label("The shield effects a draw if you'd lose. Otherwise as usual.", game.getSkin(), "title");
        shieldExpl.setAlignment(Align.center);
        shieldExpl.setWrap(true);
        shieldExpl.setFontScale(0.7f);

        Label peek_a_booExpl = new Label("Peek-A-Boo shows you the opponent's card name.", game.getSkin(), "title");
        peek_a_booExpl.setAlignment(Align.center);
        peek_a_booExpl.setWrap(true);
        peek_a_booExpl.setFontScale(0.7f);

        Label unstoppableExpl = new Label("Unstoppable makes you choose the next category definitely.", game.getSkin(), "title");
        unstoppableExpl.setAlignment(Align.center);
        unstoppableExpl.setWrap(true);
        unstoppableExpl.setFontScale(0.7f);

        Label cluelessExpl = new Label("Clueless chooses the best category for you, but doesn't mean you win.", game.getSkin(), "title");
        cluelessExpl.setAlignment(Align.center);
        cluelessExpl.setWrap(true);
        cluelessExpl.setFontScale(0.7f);

//        table.row().width(viewport.getWorldWidth());
//        table.add(shieldPic);
        table.padLeft(padding);
        table.padRight(padding);
        table.add(generalExpl).width(viewport.getWorldWidth() - padding).spaceBottom(padding / 2f).colspan(2);
        table.row();
        table.add(shieldPic).size(imageScale, imageScale);
        table.add(shieldExpl).width(viewport.getWorldWidth() - imageScale - padding * 2).spaceBottom(padding / 4);
        table.row();
        table.add(peekboo).size(imageScale, imageScale);
        table.add(peek_a_booExpl).width(viewport.getWorldWidth() - imageScale - padding * 2).spaceBottom(padding / 4);
        table.row();
        table.add(unstoppable).size(imageScale, imageScale);
        table.add(unstoppableExpl).width(viewport.getWorldWidth() - imageScale - padding * 2).spaceBottom(padding / 4);
        table.row();
        table.add(clueless).size(imageScale, imageScale);
        table.add(cluelessExpl).width(viewport.getWorldWidth() - imageScale - padding * 2).spaceBottom(padding / 4);

//        table.row().width(viewport.getWorldWidth());

//        table.add(shieldExpl).width(viewport.getWorldWidth());
        //table.debugAll();
        //container.debugAll();


    }
}
