package de.iquartett.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.iquartett.Logic.GameData.Settings;
import de.iquartett.Parser.CardDeck.Card;
import de.iquartett.QuartettGame;

public class WinningScreen implements Screen {
    private final QuartettGame game;
    private OrthographicCamera cam;
    float fontX;
    float fontY;
    private int card_id;

    float neww;
    float newh;
    private Stage stage;
    private Viewport viewport;
    //GlyphLayout glyphLayout;
    private Table categoryTable;
    //private FreeTypeFontGenerator generator;
    private BitmapFont font;
    private Card card;
    Card humanCard;
    Card computerCard;
    int categoryId;
    int compareResult;
    float compPicHeight;
    float humanPicHeight;
    int winner;

    private Sound gameWon = Settings.getGameWon();
    private Sound gameLost = Settings.getGameLost();

    public WinningScreen(final QuartettGame game, int winner) {
        this.game = game;
        this.winner = winner;

        cam = new OrthographicCamera();
        cam.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        game.batch.setProjectionMatrix(cam.combined);
        cam.update();

        this.card_id = card_id;
        font = game.font;
        // todo implement font generator
        // generator = new FreeTypeFontGenerator(Gdx.files.internal("OpenSans-Regular.ttf"));

        viewport = new FitViewport(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, cam);
        viewport.apply();

        stage = new Stage(viewport, game.batch);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);

//        AtlasGenerator atlasGenerator = new AtlasGenerator(game);


//        Sprite compSprite = atlasGenerator.create_headerImg(computerCard);
//        Sprite humanSprite = atlasGenerator.create_headerImg(humanCard);
//
//        Image compPic = scaleSprite(compSprite);
//        Image humanPic = scaleSprite(humanSprite);
//        compPic.setPosition(0, viewport.getWorldHeight() - compPic.getHeight());
////        System.out.println(viewport.getWorldHeight()+" "+compPic.getImageHeight());
//        humanPic.setPosition(0, 0);
//
//        String currentCategory = game.getGameHandler().getBatchConfig().getProperties().get(categoryId).getText();
//        String currentUnit = game.getGameHandler().getBatchConfig().getProperties().get(categoryId).getUnit();
//
//        String compValueText = computerCard.getCategories().get(categoryId).getValue() + "";
//        String humanValueText = humanCard.getCategories().get(categoryId).getValue() + "";
//        String compNameText = computerCard.getName();
//        String humanNameText = humanCard.getName();
//
//        Label categoryValue = new Label(currentCategory + ": ", new Label.LabelStyle(font, Color.BLACK));
////        Label computerValue = new Label(currentCategory + ": " + compValueText + " " + currentUnit, new Label.LabelStyle(font, Color.BLACK));
//        Label computerValue = new Label(compValueText + " " + currentUnit, new Label.LabelStyle(font, Color.BLACK));
//        Label humanValue = new Label(humanValueText + " " + currentUnit, new Label.LabelStyle(font, Color.BLACK));
////        Label humanValue = new Label(currentCategory + ": " + humanValueText + " " + currentUnit, new Label.LabelStyle(font, Color.BLACK));
//        Label compNameValue = new Label(compNameText, new Label.LabelStyle(font, Color.WHITE));
//        Label humanNameValue = new Label(humanNameText, new Label.LabelStyle(font, Color.WHITE));
//
//        categoryValue.setPosition(viewport.getWorldWidth() / 2 - (categoryValue.getWidth() + computerValue.getWidth()) / 2, viewport.getWorldHeight() / 2 - computerValue.getHeight() / 2);
////        computerValue.setPosition(viewport.getWorldWidth() / 2 - computerValue.getWidth() / 2 + categoryValue.getWidth(), viewport.getWorldHeight() / 2 - computerValue.getHeight() / 2 + 50);
//        computerValue.setPosition(categoryValue.getX() + categoryValue.getWidth(), viewport.getWorldHeight() / 2 - computerValue.getHeight() / 2 + 50);
////        humanValue.setPosition(viewport.getWorldWidth() / 2 - computerValue.getWidth() / 2+categoryValue.getWidth(), viewport.getWorldHeight() / 2 - humanValue.getHeight() / 2 - 50);
//        humanValue.setPosition(categoryValue.getX() + categoryValue.getWidth(), viewport.getWorldHeight() / 2 - humanValue.getHeight() / 2 - 50);
//        compNameValue.setPosition(viewport.getWorldWidth() / 2 - compNameValue.getWidth() / 2, viewport.getWorldHeight() - compNameValue.getHeight());
//        humanNameValue.setPosition(viewport.getWorldWidth() / 2 - humanNameValue.getWidth() / 2, 0);
//
//        // for green and red rectangle between the pics
//        compPicHeight = compPic.getHeight();
//        humanPicHeight = humanPic.getHeight();
//        computerValue.setScale(5);
//        humanValue.setScale(5);
        Image bg = new Image(new Texture(Gdx.files.internal("menu_bg.png")));
        bg.setSize(viewport.getWorldWidth(), viewport.getWorldHeight());

        switch (winner) {
            case 1:
                Image winImage = new Image(new Texture(Gdx.files.internal("win_screen.png")));
                winImage.setSize(viewport.getWorldWidth() * 0.5f, viewport.getWorldHeight() * 0.5f);
                winImage.setPosition(viewport.getWorldWidth() / 2 - winImage.getWidth() / 2, viewport.getWorldHeight() / 2 - winImage.getHeight() / 2 + 40);


                Label humanWin = new Label("YOU WIN", game.getSkin());
                humanWin.setFontScale(3);
                humanWin.setPosition(viewport.getWorldWidth() / 2 - humanWin.getWidth() / 2, viewport.getWorldHeight() / 2 - humanWin.getHeight() / 2);
//                stage.addActor(humanWin);
                stage.addActor(bg);
                stage.addActor(winImage);
                gameWon.play(Settings.getVolumePlaceholder() / 100);
                break;
            case -1:
                Image loseImg = new Image(new Texture(Gdx.files.internal("lose_screen.png")));
                loseImg.setSize(viewport.getWorldWidth() * 0.5f, viewport.getWorldHeight() * 0.5f);
                loseImg.setPosition(viewport.getWorldWidth() / 2 - loseImg.getWidth() / 2, viewport.getWorldHeight() / 2 - loseImg.getHeight() / 2 + 40);


                Label compWin = new Label("YOU LOST", game.getSkin());
                compWin.setFontScale(3);
                compWin.setPosition(viewport.getWorldWidth() / 2 - compWin.getWidth() / 2, viewport.getWorldHeight() / 2 - compWin.getHeight() / 2);
//                stage.addActor(compWin);
                stage.addActor(bg);
                stage.addActor(loseImg);

                gameLost.play(Settings.getVolumePlaceholder() / 100);
                break;
        }

        if (fontX < 0) fontX = 0;

        stage.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
//                game.getGameHandler().nextState();

                game.getDatabase().deleteGameFiles();
                game.setScreen(Screens.getMainMenuScreen());
                gameWon.stop();
                gameLost.stop();
                //dispose(); // fixme
            }
        });

//        Actor actor = new Actor() {
//            @Override
//            public void draw(Batch batch, float parentAlpha) {
//                drawRectangle(batch, compareResult);
//            }
//        };

//        stage.addActor(actor);

//        stage.addActor(categoryValue);
//        stage.addActor(humanValue);
//        stage.addActor(computerValue);
//        stage.addActor(compPic);
//        stage.addActor(humanPic);
//        stage.addActor(compNameValue);
//        stage.addActor(humanNameValue);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1f, 1f, 1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        cam.update();

        //FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        //parameter.size = 10;
        //BitmapFont cardTitle2 = generator.generateFont(parameter);

        //cardTitle2.setUseIntegerPositions(false);

        /*game.batch.begin();
        Gdx.gl.glClear(GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(1f, 1, 1, 1f);

        //game.batch.draw(headerImg, 0, headerImg_y, neww, newh);
        game.font.draw(game.batch, glyphLayout, fontX, fontY);
        //categoryTable.draw(game.batch, 1f);
        game.batch.end();*/
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        cam.update();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        stage.dispose();
        gameWon.dispose();
        gameLost.dispose();
        //generator.dispose(); // don't forget to dispose to avoid memory leaks!
    }

    public Image scaleSprite(Sprite sprite) {
        float faktor = viewport.getWorldWidth() / sprite.getWidth();
        neww = sprite.getWidth() * faktor;
        newh = sprite.getHeight() * faktor;
        float sprite_y = viewport.getWorldHeight() - newh;

        Texture t = sprite.getTexture();
        Image img = new Image(t);
        img.setBounds(0, sprite_y, neww, newh);

        return img;
    }

    /**
     * @param batch
     * @param compareResult
     */
    private void drawRectangle(Batch batch, int compareResult) {
        Gdx.gl.glLineWidth(0);
        batch.end();


        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        ShapeRenderer shapeRenderer = new ShapeRenderer();
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
//        batch.enableBlending();
        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        switch (compareResult) {
            case 1:

                shapeRenderer.setColor(new Color(Color.GREEN.r, Color.GREEN.g, Color.GREEN.b, 0.7f));
                shapeRenderer.rect(0, compPicHeight, viewport.getWorldWidth(), viewport.getWorldHeight() - compPicHeight - humanPicHeight);
                shapeRenderer.end();
                batch.begin();
                break;
            case -1:
                shapeRenderer.setColor(new Color(200, 0, 0, 0.7f));
                shapeRenderer.rect(0, compPicHeight, viewport.getWorldWidth(), viewport.getWorldHeight() - compPicHeight - humanPicHeight);
                shapeRenderer.end();
                batch.begin();
                break;
            case 0:
                batch.begin();
                break;
        }
    }

    public int getCard_id() {
        return card_id;
    }

    class SelectedCategory {
        /**
         * ID of the category
         */
        // todo use enum of categories for this?
        private int id;

        /**
         * Position of the actor who draws the label of the category name
         */
        private int position;

        SelectedCategory(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }
}
