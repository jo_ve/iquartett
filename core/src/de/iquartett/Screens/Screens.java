package de.iquartett.Screens;

public class Screens {

    // todo make this more dynamic
    // maybe usage like: Screens.setScreen(Screen screen, ScreenType ScreenType.GameScreen)
    // and if possible generate enum ScreenType dynamic at runtime with all classes that exist in package Screens as values
    private static GameScreen gameScreen;
    private static MainMenuScreen mainMenuScreen;
    private static OptionsMenuScreen optionsMenuScreen;
    private static CardScreen cardScreen;
    private static DownloadScreen downloadScreen;

    public Screens() {
    }

    public static GameScreen getGameScreen() {
        return gameScreen;
    }

    public static void setGameScreen(GameScreen gameScreen) {
        Screens.gameScreen = gameScreen;
    }

    public static MainMenuScreen getMainMenuScreen() {
        return mainMenuScreen;
    }

    public static void setMainMenuScreen(MainMenuScreen mainMenuScreen) {
        Screens.mainMenuScreen = mainMenuScreen;
    }

    public static OptionsMenuScreen getOptionsMenuScreen() {
        return optionsMenuScreen;
    }

    public static void setOptionsMenuScreen(OptionsMenuScreen optionsMenuScreen) {
        Screens.optionsMenuScreen = optionsMenuScreen;
    }

    public static CardScreen getCardScreen() {
        return cardScreen;
    }

    public static void setCardScreen(CardScreen cardScreen) {
        Screens.cardScreen = cardScreen;
    }

    public static DownloadScreen getDownloadScreen() {
        return downloadScreen;
    }

    public static void setDownloadScreen(DownloadScreen downloadScreen) {
        Screens.downloadScreen = downloadScreen;
    }
}
