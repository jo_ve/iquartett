package de.iquartett.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.iquartett.GameHandler;
import de.iquartett.QuartettGame;

public class MainMenuScreen implements Screen {

    private final QuartettGame game;
    private OrthographicCamera cam;
    private Stage stage;
    private Viewport viewport;
    private GameHandler gameHandler;
    private Label error_label;

    public MainMenuScreen(final QuartettGame game) {
        this.game = game;

        cam = new OrthographicCamera();
        cam.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        //cam.position.set(cam.viewportWidth / 2, cam.viewportHeight / 2, 0);
        cam.update();

        // todo how to make not pixelated?
        // https://stackoverflow.com/questions/35753792/libgdx-batch-draws-outside-of-fitviewport
        viewport = new FitViewport(Gdx.graphics.getWidth() / 4f, Gdx.graphics.getHeight() / 4f, cam);
        //viewport.setScreenBounds(Gdx.graphics.getWidth() / 2, 0, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight());
        viewport.apply();

        Table mainTable = new Table();
        error_label = new Label("", game.getSkin());

        stage = new Stage(viewport, game.batch);

        // Just for faster debugging
        //game.setScreen(new RuleScreen(game));

        Image bg = new Image(new Texture(Gdx.files.internal("menu_bg.png")));
        bg.setSize(viewport.getWorldWidth(), viewport.getWorldHeight());

        Label iq = new Label("iQuartett", game.getSkin(), "title");

        // Create buttons
        TextButton playButton = new TextButton("Play", game.getSkin());
        TextButton rulesButton = new TextButton("Rules", game.getSkin());
        TextButton optionsButton = new TextButton("Settings", game.getSkin());
        TextButton exitButton = new TextButton("Exit", game.getSkin());

        // Add listeners to buttons
        playButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                try {
                    gameHandler = new GameHandler(game);
                    gameHandler.start();
                    // is dispose() really called?
                    dispose();
                } catch (GdxRuntimeException e) {
                    System.err.println("Error starting game: " + e);
                    error_label.setText("Please generate textures\nvia settings first!");
                }
            }
        });

        rulesButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new RuleScreen(game, Screens.getMainMenuScreen()));
                dispose();
            }
        });

        optionsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //AtlasGenerator atlasGenerator = new AtlasGenerator(game);
                //atlasGenerator.create_atlas();
                OptionsMenuScreen optionsMenuScreen = new OptionsMenuScreen(game);
                Screens.setOptionsMenuScreen(optionsMenuScreen);
                game.setScreen(optionsMenuScreen);
                dispose();
                //dispose();
            }
        });
        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });

        mainTable.setTransform(true);
        //mainTable.setScale(1.5f);
        mainTable.setSkin(game.getSkin());
        mainTable.add(iq).colspan(2).padBottom(iq.getPrefHeight() * 0.5f);
        mainTable.row();
        mainTable.add(playButton).padBottom(20);
        mainTable.row();
        mainTable.add(rulesButton).padBottom(20);
        mainTable.row();
        mainTable.add(optionsButton).padBottom(20);
        mainTable.row();
        mainTable.add(exitButton).padBottom(20);
        mainTable.row();
        mainTable.add(error_label);
        mainTable.setFillParent(true);

        stage.addActor(bg);
        stage.addActor(mainTable);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        error_label.setText("");
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(255, 255, 255, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        cam.update();
        game.batch.setProjectionMatrix(cam.combined);

        /*game.batch.begin();
        game.font.draw(game.batch, "Welcome to QuartettGame", 100, 150);
        game.font.draw(game.batch, "Tap anywhere to begin!", 100, 100);
        game.batch.end();*/

        stage.act();
        stage.draw();

        /*if (Gdx.input.isTouched()) {
            game.setScreen(new GameScreen(game));
            dispose();
        }*/
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        //cam.position.set(cam.viewportWidth / 2, cam.viewportHeight / 2, 0);
        //cam.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    }
}
