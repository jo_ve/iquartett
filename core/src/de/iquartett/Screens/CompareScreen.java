package de.iquartett.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.iquartett.Generator.AtlasGenerator;
import de.iquartett.Logic.GameData.PowerUps;
import de.iquartett.Logic.GameData.Settings;
import de.iquartett.Parser.CardDeck.Card;
import de.iquartett.QuartettGame;

public class CompareScreen implements Screen {
    private final QuartettGame game;
    private OrthographicCamera cam;
    float fontX;
    float fontY;
    private int card_id;

    float neww;
    float newh;
    private Stage stage;
    private Viewport viewport;
    //GlyphLayout glyphLayout;
    private Table categoryTable;
    //private FreeTypeFontGenerator generator;
    private BitmapFont font;
    private Card card;
    Card humanCard;
    Card computerCard;
    int categoryId;
    int compareResult;
    float compPicHeight;
    float humanPicHeight;

    private Sound roundWon;
    private Sound roundLost;

    public CompareScreen(final QuartettGame game, Card humanCard, Card computerCard, int categoryId, int compareResult) {
        this.game = game;
        this.humanCard = humanCard;
        this.computerCard = computerCard;
        this.categoryId = categoryId;
        this.compareResult = compareResult;

        cam = new OrthographicCamera();
        viewport = new FitViewport(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, cam);
        cam.setToOrtho(false, viewport.getWorldWidth(), viewport.getWorldHeight());
        //cam.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        game.batch.setProjectionMatrix(cam.combined);
        cam.update();

        font = game.font;

        // todo implement font generator
        // generator = new FreeTypeFontGenerator(Gdx.files.internal("OpenSans-Regular.ttf"));

        viewport.apply();

        stage = new Stage(viewport, game.batch);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);

        AtlasGenerator atlasGenerator = new AtlasGenerator(game);

        switch (compareResult) {
            case 1:
                roundWon = Settings.getRoundWon();
                roundWon.play(Settings.getVolumePlaceholder() / 100);
                break;
            case -1:
                roundLost = Settings.getRoundLost();
                roundLost.play(Settings.getVolumePlaceholder() / 100);
                break;
        }

        Sprite compSprite = atlasGenerator.create_headerImg(computerCard);
        Sprite humanSprite = atlasGenerator.create_headerImg(humanCard);

        Image compPic = scaleSprite(compSprite);
        Image humanPic = scaleSprite(humanSprite);
        compPic.setPosition(compPic.getX(), viewport.getWorldHeight() - compPic.getHeight());
        humanPic.setPosition(humanPic.getX(), 0);

        String currentCategory = game.getGameHandler().getBatchConfig().getProperties().get(categoryId).getText();
        String currentUnit = game.getGameHandler().getBatchConfig().getProperties().get(categoryId).getUnit();

        String compValueText = computerCard.getCategories().get(categoryId).getValue() + "";
        String humanValueText = humanCard.getCategories().get(categoryId).getValue() + "";
        String compNameText = computerCard.getName();
        String humanNameText = humanCard.getName();

        Label compCardName = new Label(compNameText, game.getSkin(), "title");
        Label humanCardName = new Label(humanNameText, game.getSkin(), "title");

        Label categoryName = new Label(currentCategory + ": ", game.getSkin(), "big");

        Label computerValue = new Label(compValueText + " " + currentUnit, game.getSkin(), "big");
        Label humanValue = new Label(humanValueText + " " + currentUnit, game.getSkin(), "big");

        compCardName.setFontScale(0.6f);
        humanCardName.setFontScale(0.6f);

        categoryName.setScale(1.8f);

        computerValue.setScale(0.3f);
        humanValue.setScale(0.3f);

        compCardName.setPosition(30, viewport.getWorldHeight() - compCardName.getHeight());
        humanCardName.setPosition(viewport.getWorldWidth() - humanCardName.getPrefWidth() - 30, 0);

        categoryName.setPosition(viewport.getWorldWidth() / 2 - (categoryName.getWidth() + computerValue.getWidth()) / 2, viewport.getWorldHeight() / 2 - computerValue.getHeight() / 2);

        computerValue.setPosition(categoryName.getX() + categoryName.getWidth(), viewport.getWorldHeight() / 2 - computerValue.getHeight() / 2 + 30);
        humanValue.setPosition(categoryName.getX() + categoryName.getWidth(), viewport.getWorldHeight() / 2 - humanValue.getHeight() / 2 - 30);

        Table mainTable = new Table();
        mainTable.setSkin(game.getSkin());
        mainTable.setSize(viewport.getWorldWidth(), viewport.getWorldHeight());

        mainTable.add();
        mainTable.add(computerValue);
        mainTable.row();
        mainTable.add(categoryName);
        mainTable.add();
        mainTable.row();
        mainTable.add();
        mainTable.add(humanValue);

        mainTable.setFillParent(true);
        mainTable.setOrigin(Align.center);

        // for green and red rectangle between the pics
        compPicHeight = compPic.getHeight();
        humanPicHeight = humanPic.getHeight();

        if (fontX < 0) fontX = 0;

        stage.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                //game.getGameHandler().nextState();
                game.setScreen(Screens.getGameScreen());
                //dispose(); // fixme
            }
        });

        Actor actor = new Actor() {
            @Override
            public void draw(Batch batch, float parentAlpha) {
                drawRectangle(batch, compareResult);
            }
        };

        stage.addActor(actor);
        stage.addActor(mainTable);

        stage.addActor(compPic);
        stage.addActor(humanPic);
        stage.addActor(compCardName);
        stage.addActor(humanCardName);
        showUsedPowerUp(compPic.getY());
    }

    public void showUsedPowerUp(float yImgKi) {
        float screenWidth = viewport.getWorldWidth();
//        float buttonScale = screenWidth * 0.7f / 5;
        float buttonScale = screenWidth / 10;

        // shield ki
        Texture shieldUsedTexture = new Texture(Gdx.files.internal("shield_ki.png"));
        Image shieldPic = new Image(shieldUsedTexture);
        shieldPic.setSize(buttonScale, buttonScale);
        shieldPic.setPosition(screenWidth - shieldPic.getWidth(), yImgKi - shieldPic.getHeight());

        // unstoppable ki
        Texture unstoppableUsedTexture = new Texture(Gdx.files.internal("unstoppable_ki.png"));
        Image unstoppablePic = new Image(unstoppableUsedTexture);
        unstoppablePic.setSize(buttonScale, buttonScale);
        unstoppablePic.setPosition(screenWidth - unstoppablePic.getWidth(), yImgKi - unstoppablePic.getHeight());

        // skill_drain ki
        Texture skill_drainUsedTexture = new Texture(Gdx.files.internal("skill_drain_ki.png"));
        Image skill_drainPic = new Image(skill_drainUsedTexture);
        skill_drainPic.setSize(buttonScale, buttonScale);
        skill_drainPic.setPosition(screenWidth - skill_drainPic.getWidth(), yImgKi - skill_drainPic.getHeight());

        if (game.getGameHandler().getPlayerHandler().getAi().getActivePowerUps().contains(PowerUps.SHIELD)) {
            stage.addActor(shieldPic);
        }
        if (game.getGameHandler().getPlayerHandler().getAi().getActivePowerUps().contains(PowerUps.UNSTOPPABLE)) {
            stage.addActor(unstoppablePic);
        }
        if (game.getGameHandler().getPlayerHandler().getAi().getActivePowerUps().contains(PowerUps.SKILL_DRAIN)) {
            stage.addActor(skill_drainPic);
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        cam.update();

        //FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        //parameter.size = 10;
        //BitmapFont cardTitle2 = generator.generateFont(parameter);

        //cardTitle2.setUseIntegerPositions(false);

        /*game.batch.begin();
        Gdx.gl.glClear(GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(1f, 1, 1, 1f);

        //game.batch.draw(headerImg, 0, headerImg_y, neww, newh);
        game.font.draw(game.batch, glyphLayout, fontX, fontY);
        //categoryTable.draw(game.batch, 1f);
        game.batch.end();*/
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        cam.update();
    }

    @Override
    public void pause() {
        try {
//            game.getDatabase().saveObject(0, "stacks.db");
//            game.getDatabase().saveObject(2, "human.db");
//            game.getDatabase().saveObject(3, "ai.db");
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
        System.out.println("write from comparescreen");

    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        stage.dispose();
        //generator.dispose(); // don't forget to dispose to avoid memory leaks!
    }

    private Image scaleSprite(Sprite sprite) {

        float sprite_y = viewport.getWorldHeight() - newh;

        Texture t = sprite.getTexture();
        Image img = new Image(t);

        img.setBounds(0, sprite_y, viewport.getWorldWidth(), viewport.getWorldHeight() * 0.4f);

        return img;
    }

    /**
     * @param batch
     * @param compareResult
     */
    private void drawRectangle(Batch batch, int compareResult) {
        Gdx.gl.glLineWidth(0);
        batch.end();

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        ShapeRenderer shapeRenderer = new ShapeRenderer();
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
//        batch.enableBlending();
        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        switch (compareResult) {
            case 1:

                shapeRenderer.setColor(new Color(0, 200, 0, 0.7f));
                shapeRenderer.rect(0, compPicHeight, viewport.getWorldWidth(), viewport.getWorldHeight() - compPicHeight - humanPicHeight);
                shapeRenderer.end();
                batch.begin();
                break;
            case -1:
                shapeRenderer.setColor(new Color(200, 0, 0, 0.7f));
                shapeRenderer.rect(0, compPicHeight, viewport.getWorldWidth(), viewport.getWorldHeight() - compPicHeight - humanPicHeight);
                shapeRenderer.end();
                batch.begin();
                break;
            case 0:
                batch.begin();
                break;
        }
    }
}
