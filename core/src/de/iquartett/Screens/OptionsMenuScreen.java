package de.iquartett.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.iquartett.Generator.AtlasGenerator;
import de.iquartett.Logic.GameData.Settings;
import de.iquartett.QuartettGame;

public class OptionsMenuScreen implements Screen {
    private final QuartettGame game;
    private OrthographicCamera cam;
    private Stage stage;
    private Viewport viewport;
    private boolean appLoaded = true;
    static int cardCounter = 0;

    OptionsMenuScreen(final QuartettGame game) {
        this.game = game;

        game.loadBatchConfig();

        cam = new OrthographicCamera();
        viewport = new FitViewport(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 2f, cam);

        cam.setToOrtho(false, viewport.getScreenWidth(), viewport.getScreenHeight());
        //cam.position.set(cam.viewportWidth / 2, cam.viewportHeight / 2, 0);
        game.batch.setProjectionMatrix(cam.combined);
        cam.update();

        // todo how to make not pixelated?
        // https://stackoverflow.com/questions/35753792/libgdx-batch-draws-outside-of-fitviewport
        //viewport.setScreenBounds(Gdx.graphics.getWidth() / 2, 0, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight());
        viewport.apply();

        stage = new Stage(viewport, game.batch);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);

        Image bg = new Image(new Texture(Gdx.files.internal("menu_bg.png")));
        bg.setSize(viewport.getWorldWidth(), viewport.getWorldHeight());

        Label header = new Label("Settings", game.getSkin(), "title");

        // Create content
        final FileHandle[] f = game.getDecksPath().list();
        for (int i = 0; i < f.length; i++) {
            if (f[i].name().equals(game.getSettings().getDeckName())) {
                game.getSettings().setDeckID(i);
            }
        }

        Label soundTxt = new Label("Sound", game.getSkin(), "title");
        final Slider slider = new Slider(0, 100, 1, false, game.getSkin());
        slider.setValue(game.getSettings().getVolume());
        Label animationsTxt = new Label("Animations", game.getSkin(), "title");
        final TextButton animationsTrigger = new TextButton("On", game.getSkin());
        if (game.getSettings().getAnimations()) {
            animationsTrigger.setText("On");
        } else {
            animationsTrigger.setText("Off");
        }

        FileHandle handle = Gdx.files.external(game.getCurrentDeckPath() + "/img/" + game.getBatchConfig().getCards().get(0).getImages().get(0).getFilename());
        if (!handle.exists()) {
            game.getSettings().setDeckID(0);
            game.getSettings().setDeckName("tuning");
            game.loadBatchConfig();
        }
        final Texture deckPreview2 = new Texture(Gdx.files.external(game.getCurrentDeckPath() + "/img/" + game.getBatchConfig().getCards().get(0).getImages().get(0).getFilename()));
        Image deckPreview = new Image(deckPreview2);
        TextButton leftDeckBtn = new TextButton("<", game.getSkin());
        final Label deckName = new Label(game.getSettings().getDeckName().substring(0, 1).toUpperCase() + game.getSettings().getDeckName().substring(1), game.getSkin(), "title");
        TextButton rightDeckBtn = new TextButton(">", game.getSkin());
        final TextButton generateBtn = new TextButton("Generate Deck", game.getSkin());
        final TextButton downloadBtn = new TextButton("Store", game.getSkin());
        final TextButton winConditions = new TextButton("", game.getSkin());
        if (game.getSettings().isWinThroughRounds()) {
            winConditions.setText("Rounds");
        } else {
            winConditions.setText("Time");
        }
        //final SelectBox<Integer> timeOrRounds = new SelectBox<Integer>(game.getSkin());
        //timeOrRounds.setItems(1, 5, 10, 15, 20, 30, 60);
        final Slider timeOrRoundsSlider = new Slider(5, 60, 5, false, game.getSkin());
        if (game.getSettings().isWinThroughRounds()) {
            timeOrRoundsSlider.setValue(game.getSettings().getAmountOfRounds());
            //timeOrRounds.setSelected(game.getSettings().getAmountOfRounds());
        } else {
            timeOrRoundsSlider.setValue(game.getSettings().getAmountOfTimeInMin());
            //timeOrRounds.setSelected(game.getSettings().getAmountOfTimeInMin());
        }
        final Label timeOrRoundsLabel = new Label(String.valueOf(Math.round(timeOrRoundsSlider.getValue())), game.getSkin(), "title");
        TextButton cancelBtn = new TextButton("Cancel", game.getSkin());
        final TextButton applyBtn = new TextButton("Apply", game.getSkin());

        // Add listeners to buttons
        animationsTrigger.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.getSettings().setAnimations(!game.getSettings().getAnimations());
                if (game.getSettings().getAnimations()) {
                    animationsTrigger.setText("On");
                } else if (!game.getSettings().getAnimations()) {
                    animationsTrigger.setText("Off");
                }
            }
        });

        timeOrRoundsSlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                timeOrRoundsLabel.setText(String.valueOf(Math.round(timeOrRoundsSlider.getValue())));
            }
        });

        deckPreview.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.getSettings().setVolume(slider.getValue());
                if (game.getSettings().isWinThroughRounds()) {
                    game.getSettings().setAmountOfRounds(Math.round(timeOrRoundsSlider.getValue()));
                } else {
                    game.getSettings().setAmountOfTimeInMin(Math.round(timeOrRoundsSlider.getValue()));
                }
                dispose();
                game.getSettings().setDeckName(f[game.getSettings().getDeckID()].name());
                game.loadBatchConfig();
                game.setScreen(new DeckPreviewScreen(game, game.getBatchConfig().getCards().get(cardCounter)));
            }
        });

        leftDeckBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (game.getSettings().getDeckID() > 0) {
                    game.getSettings().setDeckID(game.getSettings().getDeckID() - 1);
                } else {
                    game.getSettings().setDeckID(f.length - 1);
                }
                deckName.setText(f[game.getSettings().getDeckID()].name().substring(0, 1).toUpperCase() + f[game.getSettings().getDeckID()].name().substring(1));
                game.getSettings().setDeckName(f[game.getSettings().getDeckID()].name());
                game.loadBatchConfig();
                Texture placeholder = new Texture(Gdx.files.external(game.getCurrentDeckPath() + "/img/" + game.getBatchConfig().getCards().get(0).getImages().get(0).getFilename()));
                //Texture placeholder = new Texture(Gdx.files.external("iQuartett/raw/" + f[game.getSettings().getDeckID()].name() + "/img/" + game.getBatchConfig().getCards().get(0).getImages().get(0).getFilename()));
                deckPreview2.load(placeholder.getTextureData());
            }
        });

        rightDeckBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (game.getSettings().getDeckID() < f.length - 1) {
                    game.getSettings().setDeckID(game.getSettings().getDeckID() + 1);
                } else {
                    game.getSettings().setDeckID(0);
                }
                deckName.setText(f[game.getSettings().getDeckID()].name().substring(0, 1).toUpperCase() + f[game.getSettings().getDeckID()].name().substring(1));
                game.getSettings().setDeckName(f[game.getSettings().getDeckID()].name());
                System.out.println("setting deck name to: " + f[game.getSettings().getDeckID()].name());
                game.loadBatchConfig();
                //Texture placeholder = new Texture(Gdx.files.external("iQuartett/raw/" + f[game.getSettings().getDeckID()].name() + "/img/" + game.getBatchConfig().getCards().get(0).getImages().get(0).getFilename()));
                Texture placeholder = new Texture(Gdx.files.external(game.getCurrentDeckPath() + "/img/" + game.getBatchConfig().getCards().get(0).getImages().get(0).getFilename()));
                deckPreview2.load(placeholder.getTextureData());
            }
        });

        downloadBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.getSettings().setVolume(slider.getValue());
                if (game.getSettings().isWinThroughRounds()) {
                    game.getSettings().setAmountOfRounds(Math.round(timeOrRoundsSlider.getValue()));
                } else {
                    game.getSettings().setAmountOfTimeInMin(Math.round(timeOrRoundsSlider.getValue()));
                }
                game.getSettings().setDeckName(f[game.getSettings().getDeckID()].name());
                game.loadBatchConfig();
                game.setScreen(new DownloadScreen(game));
            }
        });

        generateBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                appLoaded = false;
                game.getSettings().setDeckName(f[game.getSettings().getDeckID()].name());
                generateBtn.setText("Please wait ...");
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        // post a Runnable to the rendering thread that processes the result
                        Gdx.app.postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                // process the result, e.g. add it to an Array<Result> field of the ApplicationListener.
                                // do something important here, asynchronously to the rendering thread
//                                wait.setPosition();

                                AtlasGenerator atlasGenerator = new AtlasGenerator(game);
                                atlasGenerator.create_atlas();
                                appLoaded = true;
                                generateBtn.setText("Generate Deck");

                            }
                        });
                    }
                }).start();
                /*
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                    }
                }).start();*/
            }
        });

        winConditions.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.getSettings().setWinThroughRounds(!game.getSettings().isWinThroughRounds());
                if (game.getSettings().isWinThroughRounds()) {
                    winConditions.setText("Rounds");
                } else if (!game.getSettings().isWinThroughRounds()) {
                    winConditions.setText("Time");
                }
            }
        });

        cancelBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (appLoaded) {
                    game.getSettings().setVolume(game.getSettings().getPlaceholderVolume());
                    game.getSettings().setAnimations(game.getSettings().getPlaceholderAnimations());
                    game.getSettings().setDeckName(game.getSettings().getPlaceholderDeckName());
                    game.getSettings().setWinThroughRounds(game.getSettings().isPlaceholderWinThroughRounds());
                    game.getSettings().setAmountOfTimeInMin(game.getSettings().getPlaceholderAmountOfTimeInMin());
                    game.getSettings().setAmountOfRounds(game.getSettings().getPlaceholderAmountOfRounds());
                    game.setScreen(Screens.getMainMenuScreen());
                    dispose();
                }
            }
        });

        applyBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (appLoaded) {
                    game.getSettings().setDeckName(f[game.getSettings().getDeckID()].name());
                    game.getSettings().setVolume(slider.getValue());
                    if (game.getSettings().isWinThroughRounds()) {
                        game.getSettings().setAmountOfRounds(Math.round(timeOrRoundsSlider.getValue()));
                    } else {
                        game.getSettings().setAmountOfTimeInMin(Math.round(timeOrRoundsSlider.getValue()));
                    }
                    game.getSettings().setPlaceholderVolume(game.getSettings().getVolume());
                    game.getSettings().setPlaceholderAnimations(game.getSettings().getAnimations());
                    game.getSettings().setPlaceholderDeckName(game.getSettings().getDeckName());
                    game.getSettings().setPlaceholderWinThroughRounds(game.getSettings().isWinThroughRounds());
                    game.getSettings().setPlaceholderAmountOfTimeInMin(game.getSettings().getAmountOfTimeInMin());
                    game.getSettings().setPlaceholderAmountOfRounds(game.getSettings().getAmountOfRounds());
                    Settings.setVolumePlaceholder(slider.getValue());
                    game.getDatabase().saveObject(1, "settings.db");
                    game.setScreen(Screens.getMainMenuScreen());
                    dispose();
                }
            }
        });

        Table mainTable = new Table();
        // mainTable.pad(Gdx.graphics.getHeight() / 25);

        Table imageTable = new Table();
        Table sliderContainer = new Table();
        mainTable.setSkin(game.getSkin());

        //imageTable.pad(100);
        float space_bot = 30;

        mainTable.setTransform(true);
        //mainTable.setScale(1.3f);
        mainTable.setSize(viewport.getScreenWidth(), viewport.getScreenHeight());

        //float faktor = (viewport.getScreenWidth() / 2.8f) / deckPreview.getWidth();
        //System.out.println(deckPreview.getWidth() + " " + deckPreview.getHeight());
        float neww = 591f * viewport.getScreenWidth() / 2000f;//deckPreview.getWidth() * faktor;
        float newh = 384f * viewport.getScreenWidth() / 2000f; //deckPreview.getHeight() * faktor;
        System.out.println(neww);
        //mainTable.setWidth(neww);
        //img.setSize(10, 10);
        //img.setScaling(Scaling.none);
        imageTable.add(deckPreview).colspan(2).width(neww).height(newh).spaceBottom(space_bot / 2);
        imageTable.row();
        imageTable.add(deckName).colspan(2).spaceBottom(space_bot / 2);
        imageTable.row();
        imageTable.add(generateBtn).colspan(2).spaceBottom(space_bot / 2);
        imageTable.row();
        imageTable.add(leftDeckBtn).spaceBottom(space_bot);
        imageTable.add(rightDeckBtn).spaceBottom(space_bot);


        sliderContainer.add(timeOrRoundsLabel).spaceBottom(0);
        sliderContainer.row();
        sliderContainer.add(timeOrRoundsSlider).spaceBottom(space_bot);

        soundTxt.setFontScale(0.7f);
        animationsTxt.setFontScale(0.7f);
        deckName.setFontScale(0.7f);

        mainTable.add(soundTxt).expandX().spaceBottom(space_bot);
        mainTable.add(slider).expandX().spaceBottom(space_bot);
        mainTable.row().expandX().spaceBottom(space_bot);
        //mainTable.add(animationsTxt).expandX().spaceBottom(space_bot);
        //mainTable.add(animationsTrigger).expandX().spaceBottom(space_bot);
        //mainTable.row().expandX().spaceBottom(space_bot);
        mainTable.add(winConditions).spaceBottom(space_bot);
        mainTable.add(sliderContainer).spaceBottom(space_bot);
        mainTable.row().expandX().spaceBottom(space_bot);

        mainTable.add(imageTable).colspan(2).expandX().spaceBottom(space_bot);

        mainTable.row().expandX().spaceBottom(space_bot);
        mainTable.add(downloadBtn).colspan(2).spaceBottom(space_bot);

        mainTable.row().expandX().spaceBottom(space_bot);
        mainTable.add(cancelBtn).expandX().spaceBottom(space_bot);
        mainTable.add(applyBtn).expandX().spaceBottom(space_bot);

        mainTable.setFillParent(true);
        mainTable.setOrigin(Align.center);

        stage.addActor(bg);

        header.setPosition(viewport.getWorldWidth() / 2 - header.getPrefWidth() / 2, viewport.getWorldHeight() - header.getPrefHeight() - 30);
        //stage.addActor(header);

        stage.addActor(mainTable);
    }

    @Override
    public void render(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)) {
            game.setScreen(Screens.getMainMenuScreen());
        }

        Gdx.gl.glClearColor(255, 255, 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        cam.update();
        //game.batch.setProjectionMatrix(cam.combined);

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        //  viewport.update(width, height);
        //cam.position.set(cam.viewportWidth / 2, cam.viewportHeight / 2, 0);
        //cam.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        // cam.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
