package de.iquartett.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.iquartett.Downloader.DeckDownloader;
import de.iquartett.Downloader.ImageDownloader;
import de.iquartett.Helper.ListItem;
import de.iquartett.Helper.SimpleDirectionGestureDetector;
import de.iquartett.Parser.DeckStore.Deck;
import de.iquartett.QuartettGame;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DownloadScreen implements Screen {

    private QuartettGame game;
    private OrthographicCamera cam;
    private Stage stage;
    private Table container;
    private Viewport viewport;
    private float imageScale;
    private Image default_img;
    private Drawable download_btn_img;
    float space = 50;
    private boolean isDownloading = false;

    DownloadScreen(QuartettGame game) {
        this.game = game;
        cam = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.update();

        // fixme scrollbar is scratched
        viewport = new FitViewport(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 2f, cam);
        viewport.apply();

        Screens.setDownloadScreen(this);

        stage = new Stage(viewport, game.batch);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(new InputMultiplexer(stage, (new SimpleDirectionGestureDetector(new SimpleDirectionGestureDetector.DirectionListener() {
            @Override
            public void onUp() {
            }

            @Override
            public void onDown() {
            }

            @Override
            public void onLeft() {
                if (!isDownloading) {
                    dispose();
                    game.setScreen(Screens.getOptionsMenuScreen());
                }
            }

            @Override
            public void onRight() {
                if (!isDownloading) {
                    dispose();
                    game.setScreen(Screens.getOptionsMenuScreen());
                }
            }
        }))));

        imageScale = viewport.getWorldWidth() / 3;

        default_img = new Image(new Texture(Gdx.files.internal("placeholder.png")));
        download_btn_img = new TextureRegionDrawable(new Texture(Gdx.files.internal("download.png")));

        final float faktor = Screens.getDownloadScreen().imageScale / default_img.getPrefWidth();
        //default_img.setSize(default_img.getPrefWidth() * faktor, default_img.getPrefHeight() * faktor);

        container = new Table();
        container.setFillParent(true);

        final Table items = new Table();

        Image bg = new Image(new Texture(Gdx.files.internal("menu_bg.png")));
        bg.setSize(viewport.getWorldWidth(), viewport.getWorldHeight());

        final ScrollPane scroll = new ScrollPane(items, game.getSkin());
        scroll.setScrollingDisabled(true, false);

        final Drawable downloadPic = new TextureRegionDrawable(new Texture(Gdx.files.internal("download.png")));
        final Drawable download_alreadyPic = new TextureRegionDrawable(new Texture(Gdx.files.internal("downloaded_already.png")));
        final Drawable download_deletePic = new TextureRegionDrawable(new Texture(Gdx.files.internal("delete.png")));

        Call<java.util.List<Deck>> callDeckList = game.getStore().getDecks();

        final FileHandle[] f = game.getDecksPath().list();

        callDeckList.enqueue(new Callback<java.util.List<Deck>>() {
            @Override
            public void onResponse(Call<java.util.List<Deck>> call, Response<java.util.List<Deck>> response) {
                java.util.List<Deck> decks = response.body();
                assert decks != null;
                for (int i = 0; i < decks.size(); i++) {

                    final Deck deck = decks.get(i);
                    final ListItem itemRow = new ListItem(deck.getName(), default_img, download_btn_img, new ProgressBar(0, 1, 0.1f, false, game.getSkin()), deck.getDescription(), deck.getId(), game.getSkin());
                    itemRow.getDownloadBtn().getStyle().imageUp = downloadPic;
                    itemRow.setPreview_image(default_img);

                    // test if deck already downloaded
                    final boolean[] already_downloaded = new boolean[1];
                    for (FileHandle aF : f) {
                        if (!deck.getName().toLowerCase().equals(aF.name())) {
                            itemRow.getDownloadBtn().getStyle().imageUp = downloadPic;
                        } else {
                            if (deck.getName().toLowerCase().equals("tuning")) {
                                itemRow.getDownloadBtn().getStyle().imageUp = download_alreadyPic;
                            } else {
                                itemRow.getDownloadBtn().getStyle().imageUp = download_deletePic;
                            }
                            already_downloaded[0] = true;
                            break;
                        }
                    }
                    final Actor img = new Image(itemRow.getPreview_image().getDrawable()); // generate new preview image
                    img.setName("image" + i);
                    items.add(img).size(itemRow.getPreview_image().getPrefWidth() * faktor, itemRow.getPreview_image().getPrefHeight() * faktor);
                    items.add(itemRow.getTableNameAndDescr()).expandX();
                    items.add(itemRow.getDownloadBtn()).size(itemRow.getDeckName().getHeight() * 2).padLeft(30);
                    items.row().spaceBottom(space / 2).spaceTop(space / 2);

                    // Toggle download and set progress bar
                    itemRow.getDownloadBtn().addListener(new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            if (!already_downloaded[0]) {
                                itemRow.getDownloadBtn().setTouchable(Touchable.disabled);
                                final DeckDownloader deckDownloader = new DeckDownloader(game, deck);
                                itemRow.getProgressBar().setPosition(viewport.getWorldWidth() / 2 - itemRow.getProgressBar().getWidth(), 20);

                                Cell<Actor> cell = items.getCell(img);
                                cell.setActor(itemRow.getProgressBar());

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        while (true) {
                                            //itemRow.getProgressBar().act();
                                            itemRow.getProgressBar().setValue(deckDownloader.getProgress());
                                            if (deckDownloader.isFinished()) {

                                                if (deck.getName().toLowerCase().equals("tuning")) {
                                                    // DISPLAY HAKEN ICON FOR TUNING
                                                    itemRow.getDownloadBtn().getStyle().imageUp = download_alreadyPic;
                                                } else {
                                                    // DISPLAY DELETE ICON FOR OTHER DECKS
                                                    itemRow.getDownloadBtn().getStyle().imageUp = download_deletePic;
                                                }
                                                Cell<Actor> cell = items.getCell((Actor) itemRow.getProgressBar());
                                                cell.setActor(img);
                                                already_downloaded[0] = true;
                                                itemRow.getDownloadBtn().setTouchable(Touchable.enabled);
                                                break;
                                            }
                                        }
                                    }
                                }).start();
                                deckDownloader.download();
                            } else {
                                if (!deck.getName().toLowerCase().equals("tuning")) {
                                    Gdx.files.external("iQuartett" + "/" + deck.getName().toLowerCase()).deleteDirectory();
                                    Gdx.files.external(game.getDecksPath() + "/" + deck.getName().toLowerCase()).deleteDirectory();
                                    itemRow.getDownloadBtn().getStyle().imageUp = downloadPic;
                                    already_downloaded[0] = false;
                                }
                            }
                        }
                    });
                }
                ImageDownloader downloader = new ImageDownloader();
                downloader.download_deck_images(decks, items);
            }

            @Override
            public void onFailure(Call<java.util.List<Deck>> call, Throwable throwable) {
                System.out.println("onFailure " + throwable);
                Screens.getDownloadScreen().setDownloading(false);
                Label errorLabel = new Label("Please check your\ninternet connection!", game.getSkin(), "white-big");
                errorLabel.setPosition(viewport.getWorldWidth() / 2 - errorLabel.getWidth() / 2, viewport.getWorldHeight() / 2 - errorLabel.getHeight() / 2);
                stage.addActor(errorLabel);
            }
        });
        container.add(scroll).size(viewport.getWorldWidth() - 100, viewport.getWorldHeight() - 100).expand().fill();

        stage.addActor(bg);

        Label header = new Label("Store", game.getSkin(), "title");
        header.setPosition(viewport.getWorldWidth() / 2 - header.getPrefWidth() / 2, viewport.getWorldHeight() - header.getPrefHeight() * 2);
        stage.addActor(header);

        stage.addActor(container);
    }

    public void killDownload(Deck deck) {
        // todo there could be a bug if multiple decks are downloading but timeout occurs not for all decks, so decks are left in an unfinished state
        Gdx.files.external(game.getDecksPath() + "/" + deck.getName().toLowerCase()).deleteDirectory();
        setDownloading(false);
    }

    public boolean isDownloading() {
        return isDownloading;
    }

    public void setDownloading(boolean downloading) {
        isDownloading = downloading;
    }

    @Override
    public void render(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)) {
            game.setScreen(Screens.getOptionsMenuScreen());
        }

        Gdx.gl.glClearColor(255, 255, 255, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}

