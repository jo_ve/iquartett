package de.iquartett.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalShadowLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.DepthShaderProvider;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.iquartett.Card.CardDeck;
import de.iquartett.Card.CardSpriteDeck;
import de.iquartett.Logic.PlayerData.PlayerEnum;
import de.iquartett.Parser.CardDeck.Card;
import de.iquartett.QuartettGame;

import static de.iquartett.Logic.GameData.GameState.COMPARE_SCREEN;

public class GameScreen extends InputAdapter implements InputProcessor, Screen {
    public final static float CARD_WIDTH = 1f;
    public final static float CARD_HEIGHT = CARD_WIDTH * 277f / 200f;
    public final static float CARD_DEPTH = 0.02f;

    private final static float MINIMUM_VIEWPORT_SIZE = 7f;
    private Stage stage;
    private Label game_left_label;
    protected BitmapFont font;
    private Label score_label;
    private StringBuilder game_left_stringBuilder;
    private ShapeRenderer shapeRenderer;
    private PerspectiveCamera cam;
    private CameraInputController camController;
    private ModelBatch modelBatch;
    private Model tableTopModel;
    private ModelInstance tableTop;
    private Environment environment;
    private DirectionalShadowLight shadowLight;
    private ModelBatch shadowBatch;
    private StringBuilder score_stringBuilder;
    private ModelInstance background;
    private float spawnTimer = -1f;
    private Card humanCard;
    private Card computerCard;
    private int compareResult;
    private ArrayList<Card> played_cards;

    private QuartettGame game;

    private boolean skillDrainIsClicked = false;

    private CardScreen cardScreen;
    private CompareScreen compareScreen;
    private boolean selecting;

    private boolean start_game;
    private final int shuffled_ct = 3;
    private int shuffled_curr = 0;
    private boolean shuffled;
    private boolean block_animation;
    private PlayerEnum lastTurn;

    public GameScreen(final QuartettGame game) {
        this.game = game;
        stage = new Stage();
        font = game.font;
        score_label = new Label(" ", new Label.LabelStyle(font, Color.WHITE));
        game_left_label = new Label(" ", new Label.LabelStyle(font, Color.WHITE));
        score_label.setFontScale(2f);
        score_label.setPosition(10, 20);
        stage.addActor(score_label);
        game_left_label.setFontScale(2f);

        stage.addActor(game_left_label);
        game_left_stringBuilder = new StringBuilder();
        score_stringBuilder = new StringBuilder();
        modelBatch = new ModelBatch();
        game.getGameHandler().getCardHandler().spawnDeck();

        cam = new PerspectiveCamera();
        set_cam_default();
        camController = new CameraInputController(cam);
        camController.zoom(3);
        cam.update();

        //Gdx.input.setInputProcessor(camController);
        //Gdx.input.setInputProcessor(this);

        Texture closeTexture = new Texture(Gdx.files.internal("close.png"));
        Drawable closePic = new TextureRegionDrawable(new TextureRegion(closeTexture));
        ImageButton closeButton = new ImageButton(closePic);

        /*ImageButton helpButton = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal("help.png"))));

        helpButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new RuleScreen(game, Screens.getGameScreen()));
            }
        });*/

        closeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.getDatabase().deleteGameFiles();
                game.setScreen(Screens.getMainMenuScreen());
            }
        });
        closeButton.setSize(Gdx.graphics.getWidth() * 0.7f / 7, Gdx.graphics.getWidth() * 0.7f / 7);
        //helpButton.setSize(Gdx.graphics.getWidth() * 0.7f / 7, Gdx.graphics.getWidth() * 0.7f / 7);

        closeButton.setPosition(Gdx.graphics.getWidth() - closeButton.getWidth(), Gdx.graphics.getHeight() - closeButton.getHeight());
        //helpButton.setPosition(0, Gdx.graphics.getHeight() - helpButton.getHeight());

        //stage.addActor(helpButton);
        stage.addActor(closeButton);

        /*ModelLoader loader = new ObjLoader();
        Model m = new Model(new ModelData());
        Model model = loader.loadModel(Gdx.files.internal("spacesphere.obj"));
        background = new ModelInstance(model);*/

        String sphere_name = "g3d/shapes/sphere.g3dj";
        String material_name = "g3d/materials/moon01.g3dj";
        String material_name_box = "g3d/materials/chesterfield.g3dj";
        AssetManager assetManager = new AssetManager();
        assetManager.load(sphere_name, Model.class);
        assetManager.load(material_name, Model.class);
        assetManager.load(material_name_box, Model.class);
        assetManager.finishLoading();
        Model sphere = assetManager.get(sphere_name, Model.class);

        Model material_model = assetManager.get(material_name, Model.class);
        Material material = material_model.materials.get(0);
        sphere.materials.get(0).set(material);

        sphere.materials.get(0).set(new IntAttribute(IntAttribute.CullFace, 0));

        background = new ModelInstance(sphere);
        background.transform.scale(10, 9, 9);

        // TextureAttribute material = TextureAttribute.createDiffuse(new Texture("g3d/materials/wood_diffuse_01.png"));

        //TextureRegion tr = new TextureRegion(new Texture(Gdx.files.internal("badlogic.jpg")));

        /*ModelBuilder builder = new ModelBuilder();
        builder.begin();
        builder.node().id = "top";
        MeshPartBuilder mpb = builder.part("top", GL20.GL_TRIANGLES, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal,
                //new Material(TextureAttribute.createDiffuse(tr.getTexture())));
                new Material(ColorAttribute.createDiffuse(Color.LIGHT_GRAY)));
        //mpb.setUVRange(tr);
        BoxShapeBuilder.build(mpb, 0f, 0f, -0.5f, 25, 25, 1f);
        tableTopModel = builder.end();*/

        ModelBuilder builder = new ModelBuilder();
        builder.begin();
        builder.node().id = "top";
        builder.part("top", GL20.GL_TRIANGLES, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal,
                new Material(ColorAttribute.createDiffuse(new Color(Color.LIGHT_GRAY))))
                .box(0f, 0f, -0.5f, 20f, 20f, 1f);
        tableTopModel = builder.end();
        tableTop = new ModelInstance(tableTopModel);

             /*ModelBuilder modelBuilder = new ModelBuilder();
        modelBuilder.begin();
        MeshPartBuilder mpb = modelBuilder.part("box", GL20.GL_TRIANGLES, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal, new Material(material));
        mpb.setUVRange(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        mpb.box(0, 0, -0.5f, 10f, 10f, 1f);
        Model bg = modelBuilder.end();
        background2 = new ModelInstance(bg);*/
        //tableTopModel.materials.get(0).set(assetManager.get(material_name_box, Model.class).materials.get(0));
        tableTop = new ModelInstance(tableTopModel);
        //tableTop.materials.get(0).set(assetManager.get(material_name_box, Model.class).materials.get(0));

        shadowBatch = new ModelBatch(new DepthShaderProvider());
        shadowLight = new DirectionalShadowLight(1024, 1024, 10f, 10f, 1f, 20f);
        shadowLight.set(0.8f, 0.8f, 0.8f, -.4f, -.4f, -.4f);

        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1.f));
        environment.add(shadowLight);
        environment.shadowMap = shadowLight;

        shapeRenderer = new ShapeRenderer();
        if (game.getDatabase().loadObject(0, "stacks.db") != null) {
            game.getGameHandler().getCardHandler().repositionCards((CardSpriteDeck) game.getDatabase().loadObject(0, "stacks.db"));
        }
    }

    private void set_cam_default() {
        cam.position.set(0, 0, 10);
        cam.lookAt(0, 0, 0);
    }

    @Override
    public void resize(int width, int height) {
        float halfHeight = MINIMUM_VIEWPORT_SIZE * 0.5f;
        if (height > width)
            halfHeight *= (float) height / (float) width;
        float halfFovRadians = MathUtils.degreesToRadians * cam.fieldOfView * 0.5f;
        // TODO make cards look bigger!
        float distance = halfHeight / (float) Math.tan(halfFovRadians);

        cam.viewportWidth = width;
        cam.viewportHeight = height;
        //cam.position.set(0, 0, distance);
        cam.lookAt(0, 0, 0);
        cam.update();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(new InputMultiplexer(this, stage, camController));

        block_animation = false;

        // After CardScreen if category was selected
        if (cardScreen != null && cardScreen.getSelectedCategory() != null && cardScreen.getSelectedCategory().getId() != -1 && cardScreen.getSelectedCategory().isConfirmed()) {
            humanCard = CardDeck.getHumanDeck().peek();
            computerCard = CardDeck.getComputerDeck().peek();

            lastTurn = game.getGameHandler().getCurrentTurn();
            compareResult = game.getGameHandler().getGameLogic().compareCategories(cardScreen.getSelectedCategory().getId());
            game.getGameHandler().getGameLogic().testForPowerUps();

//            if (!skillDrainIsClicked) {
//                compareResult = game.getGameHandler().getGameLogic().compareCategories(cardScreen.getSelectedCategory().getId());
//                game.getGameHandler().getGameLogic().testForPowerUps();
//            } else {
//                game.getGameHandler().getGameLogic().testForPowerUps();
//                compareResult = game.getGameHandler().getGameLogic().compareCategories(cardScreen.getSelectedCategory().getId());
//            }
//            skillDrainIsClicked = false;

            played_cards = new ArrayList<Card>(game.getGameHandler().getCardHandler().handleRoundResult());
            // After CompareScreen
        } else if (compareScreen != null && cardScreen == null) {
            game.getGameHandler().getCardHandler().cardsToStack(compareResult, played_cards);
            game.getGameHandler().testForGameOver();
        }
        for (int i = 0; i < game.getGameHandler().getCardHandler().getCardSpriteDeck().getCardSprites().length; i++) {
            //System.out.println(game.getGameHandler().getCardHandler().getCardSpriteDeck().getCard(i).position.x + ", " + game.getGameHandler().getCardHandler().getCardSpriteDeck().getCard(i).position.y + ", " + game.getGameHandler().getCardHandler().getCardSpriteDeck().getCard(i).position.z);
            game.getGameHandler().getCardHandler().getCardSpriteDeck().getCard(i).update();
        }
    }

    @Override
    public void render(float delta) {
        //final float delta = Math.min(1 / 30f, Gdx.graphics.getDeltaTime());
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        //Gdx.app.log("Cam Pos", String.valueOf(cam.position.x + " " + cam.position.y + " " + cam.position.z));
        //Gdx.app.log("FoV", String.valueOf(cam.fieldOfView));

        // Set boundaries for camera
        //if (true || game.getGameHandler().getGameState() != GameState.SELECT_SCREEN) {
        cam.position.z = MathUtils.clamp(cam.position.z, 2, 12);
        cam.position.y = MathUtils.clamp(cam.position.y, -10, 0);
        cam.position.x = MathUtils.clamp(cam.position.x, -10, 10);
        cam.lookAt(0, 0, 0);
        // todo camera ist übelst schief
        //cam.direction.set(2, 2, 2); // what does this do?
        cam.up.set(0, 1, 3); // not perfect
//            cam.normalizeUp();
        cam.update();
        //}

        if (game.getDatabase().loadObject(0, "stacks.db") == null) {
            if (!shuffled && start_game) {
                shuffled = game.getGameHandler().getCardHandler().shuffle(shuffled_curr == shuffled_ct - 1);
                if (shuffled) {
                    shuffled_curr++;
                    shuffled = false;
                    game.getGameHandler().getCardHandler().shuffleReset();
                }
                if (shuffled_curr >= shuffled_ct) {
                    shuffled = true;
                    if (!game.getGameHandler().isDealt()) {
                        spawnTimer = 1f;
                    }
                }
            }
        }

        if (!game.getGameHandler().isDealt() && shuffled) {
            if (spawnTimer >= 0 && (spawnTimer -= delta) <= 0f) { // todo how does spawnTimer work?
                spawnTimer = 0.25f;
                game.getGameHandler().getCardHandler().deal();
            }
        }

        game.getGameHandler().getCardHandler().getActions().update(delta);

        shadowLight.begin(Vector3.Zero, Vector3.Zero);
        shadowBatch.begin(shadowLight.getCamera());
        shadowBatch.render(game.getGameHandler().getCardHandler().getCards());
        shadowBatch.end();
        shadowLight.end();

        /* Rendering */
        /*game.batch.begin();
        Gdx.gl.glClear(GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(0, 10, 0, 1f);
        game.batch.draw(sprite, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        game.batch.end();*/

        modelBatch.begin(cam);
        modelBatch.render(background);
        modelBatch.render(tableTop, environment);
        modelBatch.render(game.getGameHandler().getCardHandler().getCards(), environment);
        modelBatch.end();

        score_label.setText(game_left_stringBuilder);
        game_left_label.setText(score_stringBuilder);
        stage.act();
        stage.draw();

        game_left_stringBuilder.setLength(0);
        score_stringBuilder.setLength(0);

        /*game_left_stringBuilder.append(" FPS: ").append(Gdx.graphics.getFramesPerSecond());*/
        if (game.getSettings().isWinThroughRounds()) {
            game_left_stringBuilder.append(" Round: ").append(game.getGameHandler().getCurrentRound()).append(" / ").append(game.getSettings().getAmountOfRounds());
        } else {
            SimpleDateFormat df = new SimpleDateFormat("mm:ss", Locale.getDefault());
            String time = df.format(new Date(Math.abs(game.getGameHandler().getSecondsToGo()) * 1000));
            game_left_stringBuilder.append(" Time left: ");
            if (game.getGameHandler().getSecondsToGo() < 0) {
                game_left_stringBuilder.append("-");
            }
            game_left_stringBuilder.append(time);
        }
        score_stringBuilder.append("  Score: ").append(CardDeck.getHumanDeck().size()).append(" : ").append(CardDeck.getComputerDeck().size());
        game_left_label.setPosition(Gdx.graphics.getWidth() / 2f - game_left_label.getPrefWidth() / 2, Gdx.graphics.getHeight() - game_left_label.getPrefHeight());
    }

    private void moveCameraToUpperCard() {
        cam.position.set(0f, -2, 2);
        cam.direction.set(0, -2, 2);
        cam.up.set(0, 2, 2);
        cam.lookAt(new Vector3(0f, -2f, CardDeck.getHumanDeck().size()));
        cam.update();
    }

    @Override
    public void pause() {
        try {
            game.getDatabase().saveObject(0, "stacks.db");
            game.getDatabase().saveObject(2, "human.db");
            game.getDatabase().saveObject(3, "ai.db");
            game.getDatabase().saveGameStateAndTurn(game.getGameHandler().getGameState(), game.getGameHandler().getCurrentTurn());
            game.getDatabase().saveCardDecks();
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        modelBatch.dispose();
        game.getGameHandler().getCardHandler().getCards().dispose();
        tableTopModel.dispose();
        shadowBatch.dispose();
        shadowLight.dispose();
        shapeRenderer.dispose();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        selecting = checkHit(screenX, screenY);
        if (game.getGameHandler().getGameState() == COMPARE_SCREEN) {
            return true;
        }
        return selecting;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (game.getGameHandler().getGameState() == COMPARE_SCREEN && !block_animation) {
            block_animation = true;
            int selected = cardScreen.getSelectedCategory().getId();
            cardScreen.dispose();
            cardScreen = null;
            compareScreen = new CompareScreen(game, humanCard, computerCard, selected, compareResult);
            game.setScreen(compareScreen);
            return true;

        }
        if (selecting && !block_animation) {
            block_animation = true;
            if (checkHit(screenX, screenY)) {
                start_game = true;
                // start dealing of cards
                /*if (shuffled && !game.getGameHandler().isDealt()) {
                    spawnTimer = 1f;
                }*/
                switch (game.getGameHandler().getGameState()) {
                    case HIDDEN_CARD:
                        if (game.getGameHandler().isDealt()) {
                            game.getGameHandler().getCardHandler().turnUpperCard();
                        }
                        break;
                    case TURNED_CARD: // On second click on user card load card
//                        if (game.getGameHandler().getCurrentTurn() == PlayerEnum.HUMAN) {
                        if (cardScreen == null) {
                            //moveCameraToUpperCard();
                            cardScreen = new CardScreen(game, CardDeck.getHumanDeck().peek());
                        }
                        game.setScreen(cardScreen);
//                        }
                        break;
                }
                block_animation = false;
                return true;
            }
            block_animation = false;
        } else {
            /*if (game.getGameHandler().getGameState() == GameState.SELECTED_CARD && !game.getGameHandler().getCardHandler().checkUserCardStackHit(screenX, screenY, cam)) {
                set_cam_default();
                game.getGameHandler().getGameState().previousState();
                return true;
            }*/
        }
        return super.touchUp(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        // Use this for 2D cam above cards
        /*float dX = (float)(dragX-screenX)/(float)Gdx.graphics.getWidth();
        float dY = (float)(screenY-dragY)/(float)Gdx.graphics.getHeight();
        dragX = screenX;
        dragY = screenY;
        cam.position.add(dX * 10f, dY * 10f, 0f);
        cam.update();
        return true;*/

        //cam.position.lerp() //todo what does this do?
        //cam.update();
        return false;
    }

    public int getCompareResult() {
        return compareResult;
    }

    public void setCompareResult(int compareResult) {
        this.compareResult = compareResult;
    }

    public CardScreen getCardScreen() {
        return cardScreen;
    }

    public boolean isSkillDrainIsClicked() {
        return skillDrainIsClicked;
    }

    public void setSkillDrainIsClicked(boolean skillDrainIsClicked) {
        this.skillDrainIsClicked = skillDrainIsClicked;
    }

    /**
     * If game has not begun check if user touched on card stack, else check if touch was on user's card stack
     */
    private boolean checkHit(int screenX, int screenY) {
        if (!game.getGameHandler().isDealt()) {
            return game.getGameHandler().getCardHandler().checkCardStackHit(screenX, screenY, cam);
        } else {
            return game.getGameHandler().getCardHandler().checkUserCardStackHit(screenX, screenY, cam);
        }
    }

    public PlayerEnum getLastTurn() {
        return this.lastTurn;
    }

}
