package de.iquartett.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.SnapshotArray;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.iquartett.Card.CardDeck;
import de.iquartett.Generator.AtlasGenerator;
import de.iquartett.Helper.SimpleDirectionGestureDetector;
import de.iquartett.Logic.GameData.PowerUps;
import de.iquartett.Logic.PlayerData.PlayerEnum;
import de.iquartett.Parser.CardDeck.Card;
import de.iquartett.QuartettGame;

public class CardScreen implements Screen {

    private final QuartettGame game;
    private OrthographicCamera cam;
    private final SelectedCategory selectedCategory = new SelectedCategory(-1);

    private Stage stage;
    private Viewport viewport;
    private Table categoryTable;

    private Texture t;
    private int imgCounter = 0;
    private Card thisCard;

    CardScreen(final QuartettGame game, Card card) {
        this.game = game;
        Screens.setCardScreen(this);

        cam = new OrthographicCamera();

        viewport = new FitViewport(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 2f, cam);
        cam.setToOrtho(false, viewport.getWorldWidth(), viewport.getWorldHeight());
        viewport.apply();
        game.batch.setProjectionMatrix(cam.combined);
        cam.update();

        //private FreeTypeFontGenerator generator;

        // todo implement font generator
        // generator = new FreeTypeFontGenerator(Gdx.files.internal("OpenSans-Regular.ttf"));

        thisCard = card;

        int ansButtons = 4;
        float screenWidth = viewport.getWorldWidth();
//        float buttonScale = screenWidth * 0.7f / ansButtons;
        float buttonScale = screenWidth / (ansButtons * 1.5f + 0.5f);
//        float gapButtons = screenWidth * 0.3f / ansButtons + 1;
        float gapButtons = buttonScale / 2;
        System.out.println(buttonScale + " : " + gapButtons);

        stage = new Stage(viewport, game.batch);

        AtlasGenerator atlasGenerator = new AtlasGenerator(game);

        Sprite headerImg = atlasGenerator.create_headerImg(card);
        //font = atlasGenerator.create_cardTitle(c);

        /* Scale image to whole width */
//        float neww;
//        float newh;
//        float fakew = viewport.getWorldWidth();
//        do {
//            float faktor = fakew / headerImg.getWidth();
//            neww = headerImg.getWidth() * faktor;
//            newh = headerImg.getHeight() * faktor;
//            fakew = fakew * 0.99f;
//            System.out.println("h " + newh + " w" + neww);
//        } while (newh > viewport.getWorldHeight() * 0.4);


        t = headerImg.getTexture();
        Image img = new Image(t);

//        float faktor = viewport.getWorldHeight() * 0.4f / img.getHeight();
//        float imgH = faktor * img.getImageHeight();
//        float imgW = faktor * img.getImageWidth();
        float imgH = viewport.getWorldHeight() * 0.4f;
        float imgW = viewport.getWorldWidth();
        float headerImg_y = viewport.getWorldHeight() - imgH;


        img.setBounds(viewport.getWorldWidth() / 2 - imgW / 2, headerImg_y, imgW, imgH);

        categoryTable = atlasGenerator.create_categories(card,
                (int) viewport.getWorldWidth(),
                (int) viewport.getWorldHeight(),
                imgH,
                2,
                viewport.getWorldWidth() / 25,
                30);

        //font.getData().setScale(2);

        //glyphLayout = new GlyphLayout();
        String item = card.getName();
        if (QuartettGame.DEBUG) {
            item = String.valueOf(card.getId() + " " + item);
        }
        //glyphLayout.setText(font, item);

        //categoryTable.setSize(Gdx.graphics.getWidth(), fontY);

        final float cardHeaderScale = 0.8f;

        Label cardH = new Label(item, game.getSkin(), "big");
        cardH.setFontScale(cardHeaderScale);
        //cardH.setScale();
        float fontX = 0 + (viewport.getWorldWidth() - cardH.getPrefWidth()) / 2;
        if (fontX < 0) fontX = 0;

        float fontY = headerImg_y - cardH.getHeight() * 1.5f;
        cardH.setPosition(fontX, fontY);

        categoryTable.setSize(viewport.getWorldWidth(), cardH.getY() - cardH.getHeight() * 1f);
        categoryTable.align(Align.top);

        //   Label goBack = new Label("X", new Label.LabelStyle(font, Color.BLUE));
        // Label confirmSelection = new Label("CONFIRM", new Label.LabelStyle(font, Color.BLUE));
//        Label useShield = new Label("SHIELD", new Label.LabelStyle(font, Color.RED));
//        final Label useShield = new Label("CLUELESS", new Label.LabelStyle(font, Color.RED));
//        useShield.setPosition(0, 0);
        final Label enemyCardName = new Label(CardDeck.getComputerDeck().peek().getName(), game.getSkin(), "big");
        enemyCardName.setFontScale(cardHeaderScale - 0.1f);
        enemyCardName.setPosition((viewport.getWorldWidth() - enemyCardName.getPrefWidth()) / 2, cardH.getY() - enemyCardName.getHeight() - 10);
//        enemyCardName.setColor(Color.YELLOW);

        //categoryTable.setTouchable(Touchable.enabled);
        //atlasGenerator.getTable().setTouchable(Touchable.enabled);
        //stage.addActor(atlasGenerator.getCat_text());
        //stage.addActor(atlasGenerator.getCat_val());
        //stage.addActor(atlasGenerator.getTable());

        /*goBack.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.getDatabase().loadObject(0, "stacks.db");
                selectedCategory.setConfirmed(false);
                game.setScreen(Screens.getGameScreen());
            }
        });
        confirmSelection.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (selectedCategory.getId() != -1) {
                    selectedCategory.setConfirmed(true);
                    game.getGameHandler().nextState();
                    game.setScreen(Screens.getGameScreen());
                }
            }
        });
*/
        Image bg = new Image(new Texture(Gdx.files.internal("menu_bg.png")));
        bg.setSize(viewport.getWorldWidth(), viewport.getWorldHeight());

        Texture shieldTexture = new Texture(Gdx.files.internal("shield.png"));
        final Texture shieldTextureActive = new Texture(Gdx.files.internal("shield_active.png"));
        final Texture shieldTextureUsed = new Texture(Gdx.files.internal("shield_used.png"));
        final ImageButton shieldButton;
//        Drawable shieldPicActive = new TextureRegionDrawable(new TextureRegion(shieldTextureActive));
        if (game.getGameHandler().getPlayerHandler().getHuman().getRemainingPowerUps().contains(PowerUps.SHIELD)) {
            Drawable shieldPic = new TextureRegionDrawable(new TextureRegion(shieldTexture));
            shieldButton = new ImageButton(shieldPic);
        } else {
            Drawable shieldPic = new TextureRegionDrawable(new TextureRegion(shieldTextureUsed));
            shieldButton = new ImageButton(shieldPic);
        }

        shieldButton.setPosition(gapButtons, 20);
        shieldButton.setSize(buttonScale, buttonScale);

        Texture peekABooTexture = new Texture(Gdx.files.internal("peek_a_boo.png"));
        final Texture peekABooActive = new Texture(Gdx.files.internal("peek_a_boo_active.png"));
        final Texture peekABooUsed = new Texture(Gdx.files.internal("peek_a_boo_used.png"));
//        Drawable peekABooPic = new TextureRegionDrawable(new TextureRegion(peekABooTexture));
        final ImageButton peekABooButton;
        if (game.getGameHandler().getPlayerHandler().getHuman().getRemainingPowerUps().contains(PowerUps.PEEK_A_BOO)) {
            Drawable peekABooPic = new TextureRegionDrawable(new TextureRegion(peekABooTexture));
            peekABooButton = new ImageButton(peekABooPic);
        } else {
            Drawable peekABooPic = new TextureRegionDrawable(new TextureRegion(peekABooUsed));
            peekABooButton = new ImageButton(peekABooPic);
        }
        peekABooButton.setPosition(2 * gapButtons + buttonScale, 20);
        peekABooButton.setSize(buttonScale, buttonScale);


        Texture unstoppableTexture = new Texture(Gdx.files.internal("unstoppable.png"));
        final Texture unstoppableTextureActive = new Texture(Gdx.files.internal("unstoppable_active.png"));
        final Texture unstoppableTextureUsed = new Texture(Gdx.files.internal("unstoppable_used.png"));
//        Drawable unstoppablePic = new TextureRegionDrawable(new TextureRegion(unstoppableTexture));
        final ImageButton unstoppableButton;
        if (game.getGameHandler().getPlayerHandler().getHuman().getRemainingPowerUps().contains(PowerUps.UNSTOPPABLE)) {
            Drawable unstoppablePic = new TextureRegionDrawable(new TextureRegion(unstoppableTexture));
            unstoppableButton = new ImageButton(unstoppablePic);
        } else {
            Drawable unstoppablePic = new TextureRegionDrawable(new TextureRegion(unstoppableTextureUsed));
            unstoppableButton = new ImageButton(unstoppablePic);
        }
        unstoppableButton.setPosition(gapButtons * 3 + buttonScale * 2, 20);
        unstoppableButton.setSize(buttonScale, buttonScale);

        Texture cluelessTexture = new Texture(Gdx.files.internal("clueless.png"));
        final Texture cluelessTextureActive = new Texture(Gdx.files.internal("clueless_active.png"));
        final Texture cluelessTextureUsed = new Texture(Gdx.files.internal("clueless_used.png"));
        final ImageButton cluelessButton;
//        Drawable shieldPicActive = new TextureRegionDrawable(new TextureRegion(shieldTextureActive));
        if (game.getGameHandler().getPlayerHandler().getHuman().getRemainingPowerUps().contains(PowerUps.CLUELESS)) {
            Drawable cluelessPic = new TextureRegionDrawable(new TextureRegion(cluelessTexture));
            cluelessButton = new ImageButton(cluelessPic);
        } else {
            Drawable cluelessPic = new TextureRegionDrawable(new TextureRegion(cluelessTextureUsed));
            cluelessButton = new ImageButton(cluelessPic);
        }

        Texture skill_drainTexture = new Texture(Gdx.files.internal("skill_drain.png"));
        final Texture skill_drainTextureActive = new Texture(Gdx.files.internal("skill_drain_active.png"));
        final Texture skill_drainTextureUsed = new Texture(Gdx.files.internal("skill_drain_used.png"));
//        Drawable unstoppablePic = new TextureRegionDrawable(new TextureRegion(unstoppableTexture));
        final ImageButton skill_drainButton;
        if (game.getGameHandler().getPlayerHandler().getHuman().getRemainingPowerUps().contains(PowerUps.UNSTOPPABLE)) {
            Drawable skill_drainPic = new TextureRegionDrawable(new TextureRegion(skill_drainTexture));
            skill_drainButton = new ImageButton(skill_drainPic);
        } else {
            Drawable skill_drainPic = new TextureRegionDrawable(new TextureRegion(skill_drainTextureUsed));
            skill_drainButton = new ImageButton(skill_drainPic);
        }
        skill_drainButton.setPosition(gapButtons * 5 + buttonScale * 4, 20);
        skill_drainButton.setSize(buttonScale, buttonScale);

        cluelessButton.setPosition(3 * buttonScale + 4 * gapButtons, 20);
        cluelessButton.setSize(buttonScale, buttonScale);

        skill_drainButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (game.getGameHandler().getPlayerHandler().getHuman().usePowerUp(PowerUps.SKILL_DRAIN)) {
                    kickKiPowerUp(PowerUps.SKILL_DRAIN);
                    skill_drainButton.getStyle().imageUp = new TextureRegionDrawable(skill_drainTextureActive);
                }
            }
        });

        cluelessButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (game.getGameHandler().getPlayerHandler().getHuman().usePowerUp(PowerUps.CLUELESS)) {
                    kickKiPowerUp(PowerUps.CLUELESS);
                    cluelessButton.getStyle().imageUp = new TextureRegionDrawable(cluelessTextureActive);
                    select_category(game.getGameHandler().getPlayerHandler().getAi().calculateCategory(CardDeck.getHumanDeck().peek()));
                }
            }
        });

        unstoppableButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (game.getGameHandler().getPlayerHandler().getHuman().usePowerUp(PowerUps.UNSTOPPABLE)) {
                    kickKiPowerUp(PowerUps.UNSTOPPABLE);
                    unstoppableButton.getStyle().imageUp = new TextureRegionDrawable(unstoppableTextureActive);
                }
            }
        });

        shieldButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (game.getGameHandler().getPlayerHandler().getHuman().usePowerUp(PowerUps.SHIELD)) {
                    kickKiPowerUp(PowerUps.SHIELD);
                    shieldButton.getStyle().imageUp = new TextureRegionDrawable(shieldTextureActive);
                }
            }
        });

//        final Label enemyCardName = new Label(CardDeck.getComputerDeck().peek().getName(), new Label.LabelStyle(font, Color.BLUE));
//        enemyCardName.setPosition(viewport.getWorldWidth() / 2 - enemyCardName.getWidth() / 2, viewport.getWorldHeight() - 100);

        // todo listener needs to work for whole table row and not only for labels!

        if (game.getGameHandler().getCurrentTurn().equals(PlayerEnum.HUMAN)) {
            // calculates poweUp
            game.getGameHandler().getPlayerHandler().getAi().calculateCategory(CardDeck.getComputerDeck().peek());
            // no more cards available
            if (CardDeck.getHumanDeck().size() == 0) {
                select_category(game.getGameHandler().getPlayerHandler().getAi().calculateCategory(null));
                // human selects category
            } else {
                for (int i = 0; i < atlasGenerator.getLabels_text().size(); i++) {
                    final int finalI = i;
                    atlasGenerator.getLabels_text().get(i).addListener(new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            select_category(finalI);
                        }
                    });
                }

                for (int i = 0; i < atlasGenerator.getLabels_val().size(); i++) {
                    final int finalI = i;
                    atlasGenerator.getLabels_val().get(i).addListener(new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            select_category(finalI);
                        }
                    });
                }
            }
        } else {
            select_category(game.getGameHandler().getPlayerHandler().getAi().calculateCategory(CardDeck.getComputerDeck().peek()));
//            selectedCategory.setConfirmed(true);
        }


        ImageButton helpButton = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal("help.png"))));

        helpButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new RuleScreen(game, Screens.getCardScreen()));
            }
        });

        helpButton.setSize(viewport.getWorldWidth() * 0.7f / 7, viewport.getWorldWidth() * 0.7f / 7);

        helpButton.setPosition(0, viewport.getWorldHeight() - helpButton.getHeight());

        // categoryTable.debug();

        //categoryTable.debug();
        //stage.addActor(bg);
        stage.addActor(img);
        stage.addActor(cardH);
        stage.addActor(categoryTable);
        stage.addActor(shieldButton);
        stage.addActor(peekABooButton);
        stage.addActor(unstoppableButton);
        stage.addActor(cluelessButton);
//        stage.addActor(skill_drainButton);
        stage.addActor(helpButton);

//        useShield.addListener(new ClickListener() {
//            @Override
//            public void clicked(InputEvent event, float x, float y) {
////                game.getGameHandler().getPlayerHandler().getHuman().usePowerUp(PowerUps.SHIELD);
////                game.getGameHandler().getPlayerHandler().getHuman().usePowerUp(PowerUps.UNSTOPPABLE);
//
////                stage.act();
////                stage.draw();
//                System.out.println("DRAW");
//            }
//        });

        peekABooButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (game.getGameHandler().getPlayerHandler().getHuman().usePowerUp(PowerUps.PEEK_A_BOO)) {
                    peekABooButton.getStyle().imageUp = new TextureRegionDrawable(peekABooActive);
                    stage.addActor(enemyCardName);
                }
            }
        });

    }

    @Override
    public void show() {
        //Gdx.input.setInputProcessor(stage);
        changeInputProzessor();
    }

    private void changeInputProzessor() {
        Gdx.input.setInputProcessor(new InputMultiplexer(stage, (new SimpleDirectionGestureDetector(new SimpleDirectionGestureDetector.DirectionListener() {

            @Override
            public void onUp() {
                if (selectedCategory.getId() != -1) {
                    selectedCategory.setConfirmed(true);
                    game.getGameHandler().nextState();
                    game.setScreen(Screens.getGameScreen());
                }
            }

            @Override
            public void onDown() {
                selectedCategory.setConfirmed(false);
                //select_category(-1);
                //show();
                game.setScreen(Screens.getGameScreen());
            }

            @Override
            public void onLeft() {
                imgCounter++;
                if (imgCounter >= game.getBatchConfig().getCards().get(thisCard.getId()).getImages().size()) {
                    imgCounter = 0;
                }
                Texture placeholder = new Texture(Gdx.files.external("iQuartett/raw/" + game.getSettings().getDeckName() + "/img/" + game.getBatchConfig().getCards().get(thisCard.getId()).getImages().get(imgCounter).getFilename()));
                t.load(placeholder.getTextureData());
            }

            @Override
            public void onRight() {
                imgCounter--;
                if (imgCounter < 0) {
                    imgCounter = game.getBatchConfig().getCards().get(thisCard.getId()).getImages().size() - 1;
                }
                Texture placeholder = new Texture(Gdx.files.external("iQuartett/raw/" + game.getSettings().getDeckName() + "/img/" + game.getBatchConfig().getCards().get(thisCard.getId()).getImages().get(imgCounter).getFilename()));
                t.load(placeholder.getTextureData());
            }
        }))));
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        cam.update();

        //FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        //parameter.size = 10;
        //BitmapFont cardTitle2 = generator.generateFont(parameter);

        //cardTitle2.setUseIntegerPositions(false);

        /*game.batch.begin();
        Gdx.gl.glClear(GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(1f, 1, 1, 1f);

        //game.batch.draw(headerImg, 0, headerImg_y, neww, newh);
        game.font.draw(game.batch, glyphLayout, fontX, fontY);
        //categoryTable.draw(game.batch, 1f);
        game.batch.end();*/
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        cam.update();
    }

    @Override
    public void pause() {
        try {
            game.getDatabase().saveObject(0, "stacks.db");
            game.getDatabase().saveObject(2, "human.db");
            game.getDatabase().saveObject(3, "ai.db");
            game.getDatabase().saveCardDecks();
            game.getDatabase().saveGameStateAndTurn(game.getGameHandler().getGameState(), game.getGameHandler().getCurrentTurn());
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }

        System.out.println("write from cardscreen");

    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        stage.dispose();
        //generator.dispose(); // FIXME don't forget to dispose to avoid memory leaks!
    }

    public SelectedCategory getSelectedCategory() {
        return selectedCategory;
    }

    private void select_category(int pos) {
        if (selectedCategory.getId() == -1 || selectedCategory.getId() != pos) {
            // draw new actor
            selectedCategory.setId(pos);
            final SnapshotArray<Actor> category_actors = categoryTable.getChildren();
            for (int k = 0; k < category_actors.size; k++) {
                Actor category_actor = category_actors.get(k);
                if (category_actor.getName() != null && category_actor.getName().equals("text_actor" + pos)) {
                    selectedCategory.setPosition(k);
                    break;
                }
            }
            if (game.getGameHandler().getCurrentTurn() == PlayerEnum.HUMAN) {
                Actor actor = new Actor() {
                    @Override
                    public void draw(Batch batch, float parentAlpha) {
                        mark_category(batch, category_actors, selectedCategory.getPosition());
                    }
                };
                actor.setName("rect" + pos);
                stage.addActor(actor);
            }
        }
    }

    private void mark_category(Batch batch, SnapshotArray<Actor> category_actors, int k) {
        Actor category_actor_new = category_actors.get(k);
        Actor width_actor_new = category_actors.get(k + 2);

        Gdx.gl.glLineWidth(viewport.getWorldHeight() / 300);
        batch.end();
        ShapeRenderer shapeRenderer = new ShapeRenderer();
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());

        // Draw new rectangle
        float div = 255;
        Color border = new Color(9 / div, 64 / div, 145 / div, 1);
        shapeRenderer.setColor(border);
        shapeRenderer.rect(
                category_actor_new.getX() - 10,
                category_actor_new.getY() - 5,
                width_actor_new.getX() + 10,
                category_actor_new.getHeight() + 10);
        shapeRenderer.end();
        batch.begin();
    }

    private void kickKiPowerUp(PowerUps powerUp) {
        if (game.getGameHandler().getPlayerHandler().getAi().getActivePowerUps().contains(powerUp)) {
            game.getGameHandler().getPlayerHandler().getAi().getActivePowerUps().remove(powerUp);
            game.getGameHandler().getPlayerHandler().getAi().getRemainingPowerUps().add(powerUp);
        }
    }

    public class SelectedCategory {
        /**
         * ID of the category
         */
        // todo use enum of categories for this?
        private int id;

        /**
         * Position of the actor who draws the label of the category name
         */
        private int position;

        private boolean confirmed;

        SelectedCategory(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public boolean isConfirmed() {
            return confirmed;
        }

        public void setConfirmed(boolean confirmed) {
            this.confirmed = confirmed;
        }
    }
}
