package de.iquartett.Database;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import de.iquartett.Card.CardDeck;
import de.iquartett.Card.CardSpriteDeck;
import de.iquartett.Logic.GameData.GameState;
import de.iquartett.Logic.GameData.Settings;
import de.iquartett.Logic.PlayerData.AI;
import de.iquartett.Logic.PlayerData.Human;
import de.iquartett.Logic.PlayerData.PlayerEnum;
import de.iquartett.QuartettGame;

public class Database {
    //    private Preferences gameStateAndTurn;
    //    private Preferences cardsStacks;
//    private Preferences powerUps;
    private QuartettGame game;

    public Database(QuartettGame game) {
        // todo constructor
        this.game = game;
//        settings = Gdx.app.getPreferences("Settings");
//        cardsStacks = Gdx.app.getPreferences("CardsStacks");
//        powerUps = Gdx.app.getPreferences("PowerUps");
    }

    /**
     * @param object   0: CardSpriteDeck, 1: Settings, 2: Human, 3:AI, 4: PowerUps
     * @param fileName
     */
    public void saveObject(int object, String fileName) {
        ObjectOutputStream o;
        FileHandle fileHandle = Gdx.files.external("iQuartett/" + fileName);
        if (fileHandle.exists()) {
            try {
                fileHandle.file().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            o = new ObjectOutputStream(fileHandle.write(false));

            switch (object) {
                case 0:
                    if (game.getGameHandler().getCardHandler().getCardSpriteDeck() != null) {
                        o.writeObject(game.getGameHandler().getCardHandler().getCardSpriteDeck());
                        o.close();
                        //System.out.println("STACKS WRITTEN");
                        break;
                    }
                case 1:
                    if (game.getSettings() != null) {
                        o.writeObject(game.getSettings());
                        o.close();
                        //System.out.println("SETTINGS WRITTEN");
                        break;
                    }
                case 2:
                    if (game.getGameHandler().getPlayerHandler().getHuman() != null) {
                        o.writeObject(game.getGameHandler().getPlayerHandler().getHuman());
                        o.close();
                        //System.out.println("HUMAN WRITTEN");
                        break;
                    }
                case 3:
                    if (game.getGameHandler().getPlayerHandler().getAi() != null) {
                        o.writeObject(game.getGameHandler().getPlayerHandler().getAi());
                        o.close();
                        //System.out.println("AI WRITTEN");
                        break;
                    }
                case 4:

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param object   0: CardSpriteDeck, 1: Settings, 2: Human, 3:AI, 4: PowerUps
     * @param filename
     * @return
     */
    public Object loadObject(int object, String filename) {
        FileHandle fileHandle = Gdx.files.external("iQuartett/" + filename);
        CardSpriteDeck cardSpriteDeck;
        Settings settings;
        Human human;
        AI ai;
        InputStream f;
        ObjectInputStream o;
        try {
            if (fileHandle.exists()) {
                f = fileHandle.read();
                o = new ObjectInputStream(f);
                switch (object) {
                    case 0:
                        cardSpriteDeck = (CardSpriteDeck) o.readObject();
                        o.close();
                        f.close();
                        //System.out.println("STACKS READ");
                        return cardSpriteDeck;
                    case 1:
                        settings = (Settings) o.readObject();
                        o.close();
                        f.close();
                        //System.out.println("SETTINGS READ");
                        return settings;
                    case 2:
                        human = (Human) o.readObject();
                        o.close();
                        f.close();
                        //System.out.println("HUMAN READ");
                        return human;
                    case 3:
                        ai = (AI) o.readObject();
                        o.close();
                        f.close();
                        //System.out.println("AI READ");
                        return ai;
                }
                o.close();
                f.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
//        System.out.println("OBJECT READ FALSE");
        return null;
    }

    public void saveGameStateAndTurn(GameState gameState, PlayerEnum player) {
        ObjectOutputStream o;
        FileHandle fileHandle = Gdx.files.external("iQuartett/gameStateAndTurn.db");

        if (fileHandle.exists()) {
            try {
                fileHandle.file().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            o = new ObjectOutputStream(fileHandle.write(false));
            o.writeObject(new GameStateAndTurn(gameState, player));
            o.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public GameStateAndTurn loadGameStateAndTurn() {
        FileHandle fileHandle = Gdx.files.external("iQuartett/gameStateAndTurn.db");
        InputStream f;
        ObjectInputStream o;
        GameStateAndTurn gameStateAndTurn;
        if (fileHandle.exists()) {
            f = fileHandle.read();
            try {
                o = new ObjectInputStream(f);

                gameStateAndTurn = (GameStateAndTurn) o.readObject();
                o.close();
                f.close();
                return gameStateAndTurn;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void saveCardDecks() {
        //System.out.println(CardDeck.getHumanDeck().peek().getName() + "==================");

        ObjectOutputStream o;
        FileHandle fileHandleHuman = Gdx.files.external("iQuartett/humanDeck.db");
        FileHandle fileHandleComputer = Gdx.files.external("iQuartett/computerDeck.db");
        FileHandle fileHandleNeutral = Gdx.files.external("iQuartett/neutralDeck.db");

        if (fileHandleHuman.exists()) {
            try {
                fileHandleHuman.file().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (fileHandleComputer.exists()) {
            try {
                fileHandleComputer.file().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (fileHandleNeutral.exists()) {
            try {
                fileHandleNeutral.file().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            o = new ObjectOutputStream(fileHandleHuman.write(false));
            o.writeObject(CardDeck.getHumanDeck());
            // Write objects to file
            //System.out.println("HUMANDECK WRITTEN");

            o = new ObjectOutputStream(fileHandleComputer.write(false));
            o.writeObject(CardDeck.getComputerDeck());
            // Write objects to file
            //System.out.println("COMPDECK WRITTEN");

            o = new ObjectOutputStream(fileHandleNeutral.write(false));
            o.writeObject(CardDeck.getNeutralDeck());
            //System.out.println("NEUTRALDECK WRITTEN");

            o.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public CardDeck loadCardDeckHuman() {
        CardDeck cardDeck = null;
        FileHandle fileHandle = Gdx.files.external("iQuartett/humanDeck.db");
        InputStream f;
        ObjectInputStream o;

        try {
            if (fileHandle.exists()) {
                f = fileHandle.read();
                o = new ObjectInputStream(f);

                // read objects
                cardDeck = (CardDeck) o.readObject();

                o.close();
                f.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //System.out.println("HUMANDECK READ");
//        System.out.println("HumanDeck: " + cardDeck.size());
        return cardDeck;
    }

    public CardDeck loadCardDeckComputer() {
        CardDeck cardDeck = null;
        FileHandle fileHandle = Gdx.files.external("iQuartett/computerDeck.db");
        InputStream f;
        ObjectInputStream o;

        try {
            if (fileHandle.exists()) {
                f = fileHandle.read();
                o = new ObjectInputStream(f);

                // read objects
                cardDeck = (CardDeck) o.readObject();

                o.close();
                f.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //System.out.println("COMPUTERDECK READ");
        return cardDeck;
    }

    public CardDeck loadCardDeckNeutral() {
        CardDeck cardDeck = null;
        FileHandle fileHandle = Gdx.files.external("iQuartett/neutralDeck.db");
        InputStream f;
        ObjectInputStream o;

        try {
            if (fileHandle.exists()) {
                f = fileHandle.read();
                o = new ObjectInputStream(f);

                // read objects
                cardDeck = (CardDeck) o.readObject();

                o.close();
                f.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //System.out.println("NEUTRALDECK READ");
        return cardDeck;
    }

    public void deleteGameFiles() {
        FileHandle fileHandle_humanDeck = Gdx.files.external("iQuartett/humanDeck.db");
        FileHandle fileHandle_computerDeck = Gdx.files.external("iQuartett/computerDeck.db");
        FileHandle fileHandle_neutralDeck = Gdx.files.external("iQuartett/neutralDeck.db");
        FileHandle fileHandle_ai = Gdx.files.external("iQuartett/ai.db");
        FileHandle fileHandle_human = Gdx.files.external("iQuartett/human.db");
        FileHandle fileHandle_stacks = Gdx.files.external("iQuartett/stacks.db");
        FileHandle fileHandle_gameStateAndTurn = Gdx.files.external("iQuartett/gameStateAndTurn.db");

        fileHandle_humanDeck.delete();
        fileHandle_computerDeck.delete();
        fileHandle_neutralDeck.delete();
        fileHandle_ai.delete();
        fileHandle_human.delete();
        fileHandle_stacks.delete();
        fileHandle_gameStateAndTurn.delete();

    }
}
