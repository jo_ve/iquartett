package de.iquartett.Database;

import de.iquartett.Logic.GameData.GameState;
import de.iquartett.Logic.PlayerData.PlayerEnum;

public class GameStateAndTurn implements java.io.Serializable {
    private GameState gameState;
    private PlayerEnum player;

    public GameStateAndTurn(GameState gameState, PlayerEnum player) {
        this.gameState = gameState;
        this.player = player;
    }

    public GameState getGameState() {
        return gameState;
    }

    public PlayerEnum getPlayer() {
        return player;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public void setPlayer(PlayerEnum player) {
        this.player = player;
    }
}
