package de.iquartett.Card;

import com.badlogic.gdx.math.Vector3;

class CardAction {
    public final Vector3 toPosition = new Vector3();
    private final Vector3 fromPosition = new Vector3();

    public Vector3 toAngles = new Vector3();
    public float speed;
    private Vector3 fromAngles = new Vector3();
    private CardActions parent;
    private CardSprite cardSprite;
    private float alpha;
    private String wait = "";

    CardAction(CardActions parent) {
        this.parent = parent;
    }

    public String getWait() {
        return wait;
    }

    public void setWait(String wait) {
        this.wait = wait;
    }

    public void reset(CardSprite cardSprite) {
        this.cardSprite = cardSprite;
        fromPosition.set(cardSprite.position);
        //fromAngles.set(cardSprite.angles);

        //fromAngles.y = cardSprite.getAngles().y;
        fromAngles.set(
                new Vector3(
                        cardSprite.getAngles().x,
                        cardSprite.getAngles().y,
                        cardSprite.getAngles().z)
        );
        alpha = 0f;
    }

    public void update(float delta) {
        alpha += delta * speed;
        if (alpha >= 1f) {
            alpha = 1f;
            parent.actionComplete(this);
        }
        cardSprite.position.set(fromPosition).lerp(toPosition, alpha);
        //cardSprite.angles.set(((toAngles.sub(fromAngles)).scl(alpha)).add(fromAngles));
        //cardSprite.angles.set(new Vector3(0, fromAngles.y + alpha * (toAngles.y - fromAngles.y), 0));

        //cardSprite.getAngles().y = fromAngles.y + alpha * (toAngles.y - fromAngles.y);
        cardSprite.setAngles(
                new Vector3(
                        fromAngles.x + alpha * (toAngles.x - fromAngles.x),
                        fromAngles.y + alpha * (toAngles.y - fromAngles.y),
                        fromAngles.z + alpha * (toAngles.z - fromAngles.z)
                )
        );
        cardSprite.update();
    }
}
