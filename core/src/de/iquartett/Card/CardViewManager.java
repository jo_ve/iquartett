package de.iquartett.Card;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.FloatAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

import de.iquartett.Logic.GameData.Settings;
import de.iquartett.Parser.CardDeck.Card;
import de.iquartett.QuartettGame;
import de.iquartett.Screens.GameScreen;

public class CardViewManager {

    private float spawnZ = GameScreen.CARD_DEPTH;
    protected int ct_cards;
    protected CardSpriteDeck deck;
    protected CardBatch cards;
    protected CardActions actions;
    private QuartettGame game;

    private Sound cardFlip = Settings.getCardFlip();

    public CardViewManager(int ct_cards, TextureAtlas cardAtlas, QuartettGame game) {
        this.ct_cards = ct_cards;
        this.game = game;
        CardSpriteDeck csd = (CardSpriteDeck) game.getDatabase().loadObject(0, "stacks.db");
        if (csd == null || csd.getCardSprites().length == 0) {
            deck = new CardSpriteDeck(ct_cards, cardAtlas, 3);
        } else {
            deck = (CardSpriteDeck) game.getDatabase().loadObject(0, "stacks.db");
//            for (int i = 0; i < deck.getCardSprites().length; i++) {
//                if (deck.getCardSprites()[i].position.y > 0) {
//                    CardDeck.getComputerDeck().add(game.getBatchConfig().getCards().get(i));
//                } else if (deck.getCardSprites()[i].position.y < 0) {
//                    CardDeck.getHumanDeck().add(game.getBatchConfig().getCards().get(i));
//                } else {
//                    CardDeck.getNeutralDeck().add(game.getBatchConfig().getCards().get(i));
//                }
//            }
        }
        Texture backtex = cardAtlas.findRegion("1").getTexture();
        //backtex = cardAtlas.findRegion("back").getTexture();
        //backtex = cardHandler.getCardAtlas().getTextures().first();
        Material material = new Material(
                TextureAttribute.createDiffuse(backtex),
                new BlendingAttribute(false, 1f),
                FloatAttribute.createAlphaTest(0.2f));
        cards = new CardBatch(material);
        actions = new CardActions();
    }

    public CardBatch getCards() {
        return cards;
    }

    public CardActions getActions() {
        return actions;
    }

    protected void spawnDeck(ArrayList<Card> spawned_deck) {
        for (int j = spawned_deck.size() - 1; j >= 0; j--) {
            Card card = spawned_deck.get(j);
            int cardId = card.getId();
            //System.out.println("Spawning card " + cardId + " at z " + spawnZ);
            //System.out.println("Spawning card with index: " + cardId);
            CardSprite cardSprite = deck.getCard(cardId);
            //cardSprite.setAngles(Vector3.Y, 180f); // todo how to set with Vector3?


            cardSprite.setAngles(new Vector3(0, 180f, 0));
            cardSprite.position.set(0, 0, spawnZ);

            /*actions.animate(cardSprite, 0, 0, spawnZ,
                    new Vector3(0, 180f, 0), 5f);*/

            spawnZ += GameScreen.CARD_DEPTH;
            cards.add(cardSprite);
            cardSprite.update();
        }
    }

    protected void prepare_shuffle(ArrayList<Card> spawned_deck) {
        for (int i = 0; i < spawned_deck.size(); i++) {
            Card c = spawned_deck.get(i);
            CardSprite sprite = deck.getCard(c.getId());

            if (i < spawned_deck.size() / 2) {
                actions.animate(sprite, 3, 0, spawned_deck.size() * GameScreen.CARD_DEPTH * 1.5f - i * GameScreen.CARD_DEPTH, new Vector3(0, 180, 0), 2f, "prepare");
            } else {
                actions.animate(
                        sprite,
                        -3,
                        0,
                        (spawned_deck.size() / 2) * GameScreen.CARD_DEPTH + spawned_deck.size() * GameScreen.CARD_DEPTH * 1.5f - i * GameScreen.CARD_DEPTH,
                        new Vector3(0, 180, 0),
                        2f,
                        "prepare"
                );
            }
        }
    }

    protected void shuffle(int i, Card card) {
        actions.animate(deck.getCard(card.getId()), 0, 0, GameScreen.CARD_DEPTH + i * GameScreen.CARD_DEPTH, new Vector3(0, 180, 0), 15f, "shuffle" + i);
        cardFlip.play(Settings.getVolumePlaceholder()/100);
    }

    protected void deal(Card card, boolean toggle) {
        //Gdx.app.log("Getting cardSprite", String.valueOf(card.getId()));
        CardSprite cardSprite = deck.getCard(card.getId());
        if (cards.contains(cardSprite)) {
            //cardSprite.setAngleY(180f);
            // Split cards for user/AI
            if (toggle) {
                //System.out.println("Dealing human card " + card.getId() + " at z " + GameScreen.CARD_DEPTH * CardDeck.getHumanDeck().size());
                actions.animate(cardSprite, 0, -2, GameScreen.CARD_DEPTH * CardDeck.getHumanDeck().size(),
                        new Vector3(0, 180, 0), 4f);
                //cardSprite.position.set(0, -2, 0.GameScreen.CARD_DEPTH * CardDeck.getHumanDeck().size()); // use this for faster debugging
            } else {
                //System.out.println("Dealing computer card " + card.getId() + " at z " + GameScreen.CARD_DEPTH * CardDeck.getComputerDeck().size());
                actions.animate(cardSprite, 0, 2, GameScreen.CARD_DEPTH * CardDeck.getComputerDeck().size(),
                        new Vector3(0, 180, 180), 4f);
                //cardSprite.position.set(0, 2, 0.GameScreen.CARD_DEPTH * CardDeck.getComputerDeck().size()); // use this for faster debugging
            }
            cardSprite.update();
        }
    }

    // Check if user hits undealt CardStack
    public boolean checkCardStackHit(int screenX, int screenY, Camera cam) {
        if (deck.getCard(ct_cards - 1) != null) {
            return Intersector.intersectRayBounds(cam.getPickRay(screenX, screenY), deck.getCard(ct_cards - 1).calcBB(), new Vector3());
        } else {
            return false;
        }
    }

    // Check if user hits his own CardStack
    public boolean checkUserCardStackHit(int screenX, int screenY, Camera cam) {
        if (CardDeck.getHumanDeck().peek() != null) {
            CardSprite card = deck.getCard(CardDeck.getHumanDeck().peek().getId());
            return Intersector.intersectRayBounds(cam.getPickRay(screenX, screenY), card.calcBB(), new Vector3());
        } else {
            // fixme this is getting printed if user clicks on last card
            System.err.println("USER CARD DECK HAS NO CARD");
            return false;
        }
    }

    public void turnUpperCard() {
        CardSprite card = deck.getCard(CardDeck.getHumanDeck().peek().getId());
        //System.out.println("Turn upper card: " + CardDeck.getHumanDeck().peek().getId() + " to zpos " + GameScreen.CARD_DEPTH * CardDeck.getHumanDeck().size() + " current zpos is " + card.position.z);
        actions.animate(card, 0, -2f, GameScreen.CARD_DEPTH * CardDeck.getHumanDeck().size(),
                new Vector3(0, 0, 0), 2f);
        card.update();
    }

    protected void handleRoundResult(ArrayList<Card> played_cards) {
        // Position cards towards each other
        for (int i = 0; i < played_cards.size(); i++) {
            int z_rot = 0;
            if (i == 1) {
                z_rot = 180;
            }
            CardSprite cardsprite = deck.getCard(played_cards.get(i).getId());
            float newy = (float) (Math.pow(-1, i + 1) - Math.pow(-1, i + 1) * 0.25); // fixme zusammenfassen
            actions.animate(cardsprite, 1.5f * GameScreen.CARD_WIDTH, newy, GameScreen.CARD_DEPTH * i,
                    new Vector3(0, 0, z_rot), 2f);
            cardsprite.update();
        }
    }

    public void repositionCards(CardSpriteDeck cardSpriteDeck) {
        for (int i = 0; i < cardSpriteDeck.getCardSprites().length; i++) {
            game.getGameHandler().getCardHandler().getCardSpriteDeck().getCard(i).position.set(cardSpriteDeck.getCard(i).position);
//            cardSpriteDeck.getCard(i).position.set(cardSpriteDeck.getCard(i).position);
            //System.out.println("CARDPOSITION " + cardSpriteDeck.getCard(i).position);
            cardSpriteDeck.getCard(i).transform.set(cardSpriteDeck.getCardSprites()[i].transform);
            cardSpriteDeck.getCard(i).indices = cardSpriteDeck.getCardSprites()[i].indices;
            cardSpriteDeck.getCard(i).vertices = cardSpriteDeck.getCardSprites()[i].vertices;
            cardSpriteDeck.getCardSprites()[i].update();
        }

    }

    protected void cardsToStack(int winnerResult, ArrayList<Card> gui_cards, int ct_neutral) {
        ArrayList<Card> winning_deck = null;
        //int ct_neutral = CardDeck.getNeutralDeck().size();
        float y = 0;
        float z_rot = 0;

        switch (winnerResult) {
            case 1:
                y = -2;
                winning_deck = new ArrayList<Card>(CardDeck.getHumanDeck());
                //System.out.println("Human won deck size: " + winning_deck.size());
                break;
            case 0:
                y = 0;
                winning_deck = new ArrayList<Card>(CardDeck.getNeutralDeck());
                //System.out.println("Neutral won deck size: " + winning_deck.size());
                break;
            case -1:
                y = 2;
                z_rot = 180;
                winning_deck = new ArrayList<Card>(CardDeck.getComputerDeck());
                //System.out.println("Computer won deck size: " + winning_deck.size());
                break;
        }

        assert winning_deck != null;

        //float pos_up = played_cards.size() - ct_neutral;

        //System.out.println("played: " + played_cards.size());
        //System.out.println("neutral: " + ct_neutral);
        /*if (ct_neutral > 2) {
            ct_neutral -= 2;
        }*/

        // Move cards of winning stack up
        for (int i = 0; i < winning_deck.size(); i++) {
            CardSprite cardSprite = deck.getCard(winning_deck.get(i).getId());

            float pos;
            if (winnerResult == 0) {
                //pos = (GameScreen.CARD_DEPTH * ct_neutral) + cardSprite.position.z_rot;
                pos = cardSprite.position.z + GameScreen.CARD_DEPTH * 2;
            } else {
                //pos = (GameScreen.CARD_DEPTH * gui_cards.size()) + cardSprite.position.z_rot;
                pos = cardSprite.position.z + GameScreen.CARD_DEPTH * 2 + ct_neutral * GameScreen.CARD_DEPTH;
            }

            //System.out.println("Move card " + winning_deck.get(i).getId() + " up to zpos: " + pos + " old was " + cardSprite.position.z_rot + "( " + cardSprite.position.z_rot + " " + GameScreen.CARD_DEPTH * 2 + " " + ct_neutral * GameScreen.CARD_DEPTH + ")");

            actions.animate(cardSprite, 0, y, pos, new Vector3(0, 180, z_rot), 2f);
            cardSprite.update();
        }

        // Add new cards to bottom of stack
        int runner;
        // Add cards to neutral
        if (winnerResult == 0) {
            Card c = gui_cards.get(gui_cards.size() - 1);
            CardSprite cardSprite = deck.getCard(c.getId());
            float pos = GameScreen.CARD_DEPTH;
            //System.out.println("Add card " + c.getId() + " to zpos: " + pos);
            actions.animate(cardSprite, 0, y, pos, new Vector3(0, 180, z_rot), 1f);
            cardSprite.update();

            Card c2 = gui_cards.get(gui_cards.size() - 2);
            cardSprite = deck.getCard(c2.getId());
            pos = GameScreen.CARD_DEPTH * 2;
            //System.out.println("Add card " + c2.getId() + " to zpos: " + pos);
            actions.animate(cardSprite, 0, y, pos, new Vector3(0, 180, z_rot), 1f);
            cardSprite.update();
            // Add cards to player deck
        } else {
            runner = gui_cards.size();

            for (float i = 0; i < runner; i++) {
                int posfetcher = (int) i;
                CardSprite cardSprite = deck.getCard(gui_cards.get(posfetcher).getId());
                float pos = GameScreen.CARD_DEPTH * (runner - i);
                //System.out.println("Add card " + gui_cards.get((int) i).getId() + " to zpos: " + pos);

                actions.animate(cardSprite, 0, y, pos, new Vector3(0, 180, z_rot), 1f);
                cardSprite.update();
            }
        }
        //System.out.println("Runner is " + runner);
    }
}
