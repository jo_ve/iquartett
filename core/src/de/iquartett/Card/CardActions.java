package de.iquartett.Card;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;

import java.util.ArrayList;
import java.util.Collections;

import de.iquartett.Logic.GameData.Settings;

public class CardActions {
    private Pool<CardAction> actionPool = new Pool<CardAction>() {
        protected CardAction newObject() {
            return new CardAction(CardActions.this);
        }
    };
    private Array<CardAction> actions = new Array<CardAction>();
    private final static ArrayList<String> waiter = new ArrayList<String>();

    private Sound cardFlip = Settings.getCardFlip();

    public boolean isFinished(String key) {
        return !waiter.contains(key);
    }

    void actionComplete(CardAction action) {
        actions.removeValue(action, true);
        actionPool.free(action);
        waiter.removeAll(Collections.singleton(action.getWait()));
    }

    public void update(float delta) {
        for (CardAction action : actions) {
            action.update(delta);
        }
    }

    public void animate(CardSprite cardSprite, float x, float y, float z, Vector3 angles, float speed) {
        animate(cardSprite, x, y, z, angles, speed, "");
        cardFlip.play(Settings.getVolumePlaceholder() / 100);
    }

    public void animate(CardSprite cardSprite, float x, float y, float z, Vector3 angles, float speed, String wait) {
        CardAction action = actionPool.obtain();
        action.reset(cardSprite);
        action.toPosition.set(x, y, z);
        action.toAngles.set(angles.x, angles.y, angles.z);
        action.speed = speed;
        action.setWait(wait);
        waiter.add(wait);
        actions.add(action);
    }
}