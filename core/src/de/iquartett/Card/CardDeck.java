package de.iquartett.Card;

import java.util.LinkedHashSet;

import de.iquartett.Parser.CardDeck.Card;

public class CardDeck extends LinkedHashSet<Card> implements java.io.Serializable {

    private static CardDeck humanDeck = new CardDeck();
    private static CardDeck computerDeck = new CardDeck();
    private static CardDeck neutralDeck = new CardDeck();
    private static final long serialVersionUID = 30L;

    private CardDeck() {
    }

    public static CardDeck getHumanDeck() {
        return humanDeck;
    }

    public static CardDeck getComputerDeck() {
        return computerDeck;
    }

    public static void setHumanDeck(LinkedHashSet<Card> linkedHashSet) {
        humanDeck.clear();
        humanDeck.addAll(linkedHashSet);
    }

    public static void setComputerDeck(LinkedHashSet<Card> linkedHashSet) {
        computerDeck.clear();
        computerDeck.addAll(linkedHashSet);
    }

    public static void setHumanDeckFromDatabase(CardDeck humanDeckDatabase) {
        humanDeck = null;
        humanDeck = new CardDeck();
        humanDeck.addAll(humanDeckDatabase);
    }

    public static void setComputerDeckFromDatabase(CardDeck computerDeckDeckDatabase) {
        computerDeck.clear();
        computerDeck = computerDeckDeckDatabase;
    }

    public static void setNeutralDeckFromDatabase(CardDeck neutralDeckDatabase) {
        neutralDeck.clear();
        neutralDeck = neutralDeckDatabase;
    }

    // for cards after comparing with draw result
    public static CardDeck getNeutralDeck() {
        return neutralDeck;
    }

    @Override
    public boolean add(Card card) {
        if (humanDeck.contains(card) || computerDeck.contains(card) || neutralDeck.contains(card)) {
            throw new IllegalStateException();
        } else {
            return super.add(card);
        }
    }

    public Card next() {
        if (iterator().hasNext()) {
            Card card = iterator().next();
            humanDeck.remove(card);
            computerDeck.remove(card);
            neutralDeck.remove(card);
            //iterator().remove();
            return card;
        }
        return null;
    }

    public Card peek() {
        if (iterator().hasNext()) {
            return iterator().next();
        }
        return null;
    }

    public static void resetCardDeck() {
        humanDeck.clear();
        computerDeck.clear();
        neutralDeck.clear();
    }

}
