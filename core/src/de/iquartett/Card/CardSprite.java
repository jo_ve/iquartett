package de.iquartett.Card;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.utils.MeshBuilder;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;

import de.iquartett.Screens.GameScreen;

public class CardSprite implements java.io.Serializable {
    private static final long serialVersionUID = 26L;
    final Matrix4 transform = new Matrix4();
    public final Vector3 position = new Vector3();
    float[] vertices;
    short[] indices;
    private Vector3 angles = new Vector3();

    CardSprite(Sprite back, Sprite front) {
        assert (front.getTexture() == back.getTexture());
        front.setPosition(-front.getWidth() * 0.5f, -front.getHeight() * 0.5f);
        back.setPosition(-back.getWidth() * 0.5f, -back.getHeight() * 0.5f);

        vertices = convert(front.getVertices(), back.getVertices());
        indices = new short[]{0, 1, 2, 2, 3, 0, 4, 5, 6, 6, 7, 4};
    }

    public Vector3 getAngles() {
        return angles;
    }

    public void setAngles(Vector3 angles) {
        this.angles = angles;
    }

    public void setAngleX(float x) {
        this.angles.x = x;
    }

    public void setAngleY(float y) {
        this.angles.y = y;
    }

    public void setAngleZ(float z) {
        this.angles.z = z;
    }

    private static float[] convert(float[] front, float[] back) {
        return new float[]{
                front[Batch.X2], front[Batch.Y2], GameScreen.CARD_DEPTH, 0, 0, 1, front[Batch.U2], front[Batch.V2],
                front[Batch.X1], front[Batch.Y1], GameScreen.CARD_DEPTH, 0, 0, 1, front[Batch.U1], front[Batch.V1],
                front[Batch.X4], front[Batch.Y4], GameScreen.CARD_DEPTH, 0, 0, 1, front[Batch.U4], front[Batch.V4],
                front[Batch.X3], front[Batch.Y3], GameScreen.CARD_DEPTH, 0, 0, 1, front[Batch.U3], front[Batch.V3],

                back[Batch.X1], back[Batch.Y1], -GameScreen.CARD_DEPTH, 0, 0, -1, back[Batch.U1], back[Batch.V1],
                back[Batch.X2], back[Batch.Y2], -GameScreen.CARD_DEPTH, 0, 0, -1, back[Batch.U2], back[Batch.V2],
                back[Batch.X3], back[Batch.Y3], -GameScreen.CARD_DEPTH, 0, 0, -1, back[Batch.U3], back[Batch.V3],
                back[Batch.X4], back[Batch.Y4], -GameScreen.CARD_DEPTH, 0, 0, -1, back[Batch.U4], back[Batch.V4]
        };
    }

    /*public void update() {
        float z = position.z + 0.5f * Math.abs(MathUtils.sinDeg(angle));
        transform.setToRotation(Vector3.Y, angle);
        transform.trn(position.x, position.y, z);
    }*/

    public void update() {
        /*Vector3 abstand_damit_karte_nicht_im_tisch_klebt = new Vector3(
                position.z + 0.5f * Math.abs(MathUtils.sinDeg(angles.x)), // ??
                position.z + 0.5f * Math.abs(MathUtils.sinDeg(angles.y)), // ??
                position.z
        );*/

        //System.out.println("ROTATION X " + angles.x + " Y " + angles.y + " Z " + angles.z);

        float z = position.z + 0.5f * Math.abs(MathUtils.sinDeg(angles.y));
        //transform.setToRotation(Vector3.X, angles.x);
        //transform.rotate(Vector3.X, angles.x);
        //transform.rotate(Vector3.Y, angles.y);
        transform.setFromEulerAngles(angles.y, angles.x, angles.z);
        transform.trn(position.x, position.y, z);

    }

    BoundingBox calcBB() {
        // The following is just used to calculate the bounding box of one card
        // TODO make this easier and do not load meshbuilder etc. to just AtlasGenerator boundingbox
        // -> Calculate BoundingBox out of this.transform, this.vertices and this.indices
        final int maxNumberOfCards = 52;
        final int maxNumberOfVertices = maxNumberOfCards * 8;
        final int maxNumberOfIndices = maxNumberOfCards * 12;
        Renderable renderable;
        renderable = new Renderable();
        MeshBuilder meshBuilder = new MeshBuilder();
        Mesh mesh = new Mesh(false, maxNumberOfVertices, maxNumberOfIndices,
                VertexAttribute.Position(), VertexAttribute.Normal(), VertexAttribute.TexCoords(0));
        meshBuilder.begin(mesh.getVertexAttributes());
        meshBuilder.part("cards", GL20.GL_TRIANGLES, renderable.meshPart);
        meshBuilder.setVertexTransform(this.transform);
        meshBuilder.addMesh(this.vertices, this.indices);
        meshBuilder.end(mesh);
        return mesh.calculateBoundingBox();
    }

}
