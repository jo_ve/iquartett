package de.iquartett.Card;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import static de.iquartett.Screens.GameScreen.CARD_HEIGHT;
import static de.iquartett.Screens.GameScreen.CARD_WIDTH;

public class CardSpriteDeck implements java.io.Serializable {
    private final CardSprite[] cardSprites;
    private static final long serialVersionUID = 25L;

    public CardSprite[] getCardSprites() {
        return cardSprites;
    }

    public CardSpriteDeck(int ctCards, TextureAtlas cardAtlas, int backIndex) {
        cardSprites = new CardSprite[ctCards];
        for (int i = 0; i < ctCards; i++) {
            Sprite front = cardAtlas.createSprite(String.valueOf(i), -1);
            front.setSize(CARD_WIDTH, CARD_HEIGHT);
            Sprite back = cardAtlas.createSprite("back", -1);
            back.setSize(CARD_WIDTH, CARD_HEIGHT);
            cardSprites[i] = new CardSprite(back, front);
        }
    }

    public CardSprite getCard(int i) {
        if (i < 0 || i >= cardSprites.length) {
            return null;
        } else {
            return cardSprites[i];
        }
    }


}
