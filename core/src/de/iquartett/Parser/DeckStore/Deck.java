package de.iquartett.Parser.DeckStore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Deck {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("misc")
    @Expose
    private String misc;
    @SerializedName("misc_version")
    @Expose
    private String miscVersion;
    @SerializedName("image")
    @Expose
    private String image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMisc() {
        return misc;
    }

    public void setMisc(String misc) {
        this.misc = misc;
    }

    public String getMiscVersion() {
        return miscVersion;
    }

    public void setMiscVersion(String miscVersion) {
        this.miscVersion = miscVersion;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
