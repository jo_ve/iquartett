package de.iquartett.Parser.DeckStore;

import java.util.List;

public class FullCardDeck {

    private Deck deck;
    private List<Card> cardList;

    public FullCardDeck(Deck deck, List<Card> cardList) {
        this.deck = deck;
        this.cardList = cardList;
    }

    public Deck getDeck() {
        return deck;
    }

    public List<Card> getCardList() {
        return cardList;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public void setCardList(List<Card> cardList) {
        this.cardList = cardList;
    }
}
