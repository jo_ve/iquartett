package de.iquartett.Parser.DeckStore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Card {
    private List<Attribute> attributes;
    private List<Image> images;

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("deck")
    @Expose
    private Integer deck;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("order")
    @Expose
    private Integer order;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDeck() {
        return deck;
    }

    public void setDeck(Integer deck) {
        this.deck = deck;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}