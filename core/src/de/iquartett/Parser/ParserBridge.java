package de.iquartett.Parser;

import java.util.ArrayList;
import java.util.List;

import de.iquartett.Generator.DeckGenerator;
import de.iquartett.Parser.CardDeck.BatchConfig;
import de.iquartett.Parser.CardDeck.Card;
import de.iquartett.Parser.CardDeck.Category;
import de.iquartett.Parser.CardDeck.Description;
import de.iquartett.Parser.CardDeck.Image;
import de.iquartett.Parser.CardDeck.Properties;
import de.iquartett.Parser.DeckStore.Attribute;
import de.iquartett.Parser.DeckStore.FullCardDeck;
import de.iquartett.QuartettGame;

//import de.iquartett.Parser.DeckStore.Card;

public class ParserBridge {
    // convert FullCardDeck into BatchConfig

    // deckstore attribute name, value -> category
    // attribute name,  unit -> properties
    private FullCardDeck fullCardDeck;
    private QuartettGame game;

    public ParserBridge(QuartettGame game, FullCardDeck fullCardDeck) {
        this.game = game;
        this.fullCardDeck = fullCardDeck;
    }


    public void parseFullCardDeck() {
        ArrayList<Card> cards = new ArrayList<Card>();

        ArrayList<Properties> properties = new ArrayList<Properties>();

        for (Attribute attribute : fullCardDeck.getCardList().get(0).getAttributes()) {
            String text = replaceUmlauts(attribute.getName());

            int higherwins;
            if (attribute.getWhatWins().equals("higher_wins")) {
                higherwins = 1;
            } else higherwins = -1;

            int id = attribute.getId();

            String unit = attribute.getUnit();

            // never used
            int precision = 0;

            Properties crrntProperties = new Properties(text, higherwins, id, unit, precision);
            properties.add(crrntProperties);
        }

        Description description = new Description(fullCardDeck.getDeck().getDescription());

        int id = 0;
        for (de.iquartett.Parser.DeckStore.Card card : fullCardDeck.getCardList()) {
            ArrayList<Image> cardDeckImages = new ArrayList<Image>();
            for (de.iquartett.Parser.DeckStore.Image deckStoreImage : card.getImages()) {
                Image cardDeckImage = new Image(deckStoreImage.getId(), deckStoreImage.getImage());
                cardDeckImages.add(cardDeckImage);
            }
            ArrayList<Category> values = new ArrayList<Category>();
            List<Attribute> attributes = card.getAttributes();

            for (int i = 0, attributesSize = attributes.size(); i < attributesSize; i++) {
                Attribute attribute = attributes.get(i);
                float value = Float.parseFloat(attribute.getValue());
                //int propertyId = attribute.getId();
                Category category = new Category(value, i);
                values.add(category);
            }

            String name = replaceUmlauts(card.getName());

            de.iquartett.Parser.CardDeck.Card cardDeckCard = null;

            cardDeckCard = new Card(cardDeckImages, description, values, id, name);
            id++;

            if (cardDeckCard != null) {
                cards.add(cardDeckCard);
            } else {
                System.out.println("CARD NULL");
            }

        }

        System.out.println("Parsing successful");

        // Pass result to DeckGenerator
        DeckGenerator deckGenerator = new DeckGenerator(game, new BatchConfig(cards, properties, description, fullCardDeck.getDeck().getName()));
        deckGenerator.saveDeck();
    }

    private String replaceUmlauts(String text) {
        return text.replaceAll("ü", "ue").replaceAll("ö", "oe").replaceAll("ä", "ae").replaceAll("ß", "ss").replaceAll("Ü", "Ue").replaceAll("Ö", "Oe").replaceAll("Ä", "Ae");
    }
}
