package de.iquartett.Parser.CardDeck;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Card implements java.io.Serializable {
    private static final long serialVersionUID = 24L;
    @SerializedName("images")
    @Expose
    private final ArrayList<Image> images;

    @SerializedName("description")
    @Expose
    private final Description description;

    @SerializedName("values")
    @Expose
    private final ArrayList<Category> categories;

    @SerializedName("id")
    @Expose
    private final int id;

    @SerializedName("name")
    @Expose
    private final String name;

    public Card(ArrayList<Image> images, Description description, ArrayList<Category> values, int id, String name) {
        this.images = images;
        this.description = description;
        this.categories = values;
        this.id = id;
        this.name = name;
    }

    public List<Image> getImages() {
        return Collections.unmodifiableList(images);
    }

    public Description getDescription() {
        return description;
    }

    public List<Category> getCategories() {
        return Collections.unmodifiableList(categories);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
