package de.iquartett.Parser.CardDeck;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Description implements java.io.Serializable {
    private static final long serialVersionUID = 21L;
    @SerializedName("description")
    @Expose
    private final String description; // data type not safe

    public Description(String description) {
        this.description = description;
    }

    public String getDescriptio() {
        return description;
    }
}
