package de.iquartett.Parser.CardDeck;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category implements java.io.Serializable {
    private static final long serialVersionUID = 20L;

    @SerializedName("value")
    @Expose
    private final float value;

    @SerializedName("propertyId")
    @Expose
    private final int propertyId;

    public Category(float value, int propertyid) {
        this.value = value;
        this.propertyId = propertyid;
    }

    public float getValue() {
        return value;
    }

    public int getPropertyId() {
        return propertyId;
    }
}
