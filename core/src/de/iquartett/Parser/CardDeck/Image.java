package de.iquartett.Parser.CardDeck;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image implements java.io.Serializable {
    private static final long serialVersionUID = 22L;
    @SerializedName("id")
    @Expose
    private final int id;

    @SerializedName("filename")
    @Expose
    private final String filename;

    public Image(int id, String filename) {
        this.id = id;
        this.filename = filename;
    }

    public int getId() {
        return id;
    }

    public String getFilename() {
        return filename;
    }
}
