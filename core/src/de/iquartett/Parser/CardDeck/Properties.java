package de.iquartett.Parser.CardDeck;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Properties {
    @SerializedName("text")
    @Expose
    private final String text;

    @SerializedName("compare")
    @Expose
    private final int higherWins;

    @SerializedName("id")
    @Expose
    private final int id;

    @SerializedName("unit")
    @Expose
    private final String unit;

    @SerializedName("precision")
    @Expose
    private final int precision;

    public Properties(String text, int higherWins, int id, String unit, int precision) {
        this.text = text;
        this.higherWins = higherWins;
        this.id = id;
        this.unit = unit;
        this.precision = precision;
    }

    public String getText() {
        return text;
    }

    public int getId() {
        return id;
    }

    public String getUnit() {
        return unit;
    }

    public int getPrecision() {
        return precision;
    }

    public boolean higherWins() {
        return higherWins == 1;
    }
}
