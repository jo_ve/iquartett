package de.iquartett.Parser.CardDeck;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BatchConfig {
    @SerializedName("cards")
    @Expose
    private final ArrayList<Card> cards;

    @SerializedName("properties")
    @Expose
    private final ArrayList <Properties> properties;

    @SerializedName("description")
    @Expose
    private final Description description;

    @SerializedName("name")
    @Expose
    private final String name;

    public BatchConfig(ArrayList<Card> cards, ArrayList<Properties> properties, Description description, String name) {
        this.cards = cards;
        this.properties = properties;
        this.description = description;
        this.name = name;
    }

    public List<Card> getCards() {
        return Collections.unmodifiableList(cards);
    }

    public List<Properties> getProperties() {
        return Collections.unmodifiableList(properties);
    }

    public Description getDescription() {
        return description;
    }

    public String getName() {
        return name.toLowerCase();
    }
}
