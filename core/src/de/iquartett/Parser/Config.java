package de.iquartett.Parser;

import com.badlogic.gdx.files.FileHandle;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;

public class Config {
    /**
     * The Gson object to handle the JSON parsing
     */
    private static Gson gson;

    static {
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    /**
     * Deserialize the given config file of the given type.
     *
     * @param path      The path to the configuration file
     * @param confclass Class file of the configuration, which is used to parse the JSON accordingly
     * @param <T>       Configuration object
     * @return LevelConfig or MatchConfig object with the parsed data
     */
    public static <T> T decodeConfData(InputStream path, Class<T> confclass) {
        InputStreamReader confReader;
        // Config file should have UTF-8 encoding
        //confReader = new InputStreamReader(new FileInputStream(path), Charset.forName("UTF-8"));
        confReader = new InputStreamReader(path);

        return gson.fromJson(confReader, confclass);
    }

    public static void encodeConfData(Object object, FileHandle handle) {
        try (Writer writer = new FileWriter(handle.file())) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            gson.toJson(object, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
