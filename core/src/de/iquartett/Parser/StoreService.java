package de.iquartett.Parser;

import java.util.List;

import de.iquartett.Parser.DeckStore.Attribute;
import de.iquartett.Parser.DeckStore.Card;
import de.iquartett.Parser.DeckStore.Deck;
import de.iquartett.Parser.DeckStore.Image;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface StoreService {

    @GET("/decks")
    Call<List<Deck>> getDecks();

    @GET("/decks/{DECK_ID}")
    Call<Deck> getDeck(@Path("DECK_ID") int deck_id); // with Call<List<Deck>> getDeck(@Path("DECK_ID") List<int> id) we can also give list of ids to getDeck() to receive multiple decks

    @GET("/decks/{DECK_ID}/cards")
    Call<List<Card>> getCards(@Path("DECK_ID") int deck_id);

    @GET("/decks/{DECK_ID}/cards/{CARD_ID}")
    Call<Card> getCard(@Path("DECK_ID") int deck_id, @Path("CARD_ID") int card_id);

    @GET("/decks/{DECK_ID}/cards/{CARD_ID}/attributes")
    Call<List<Attribute>> getAttributes(@Path("DECK_ID") int deck_id, @Path("CARD_ID") int card_id);

    @GET("/decks/{DECK_ID}/cards/{CARD_ID}/images")
    Call<List<Image>> getImages(@Path("DECK_ID") int deck_id, @Path("CARD_ID") int card_id);
}
