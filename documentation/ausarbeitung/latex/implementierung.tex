\chapter{Implementierung}
\label{cha:implementierung}

Dieses Kapitel soll einen Überblick über die Implementierung der vorgestellten mobilen Applikation geben. Zunächst wird ein Überblick über die entworfene Softwarearchitektur gegeben, sowie die umgesetzte Modularisierung in untergeordnete Java-Packages und die Zusammenhänge der einzelnen Komponenten.

% Abschnitt: Genutzte Technik
\section{Genutzte Technik}
\label{sec:implementierung:genutzte_technik}
%TODO Fachwörter kursiv

Die Anwendung wurde die Plattform Android für ein minimales API Level von 14 erstellt, was der Version 4.0 \textit{Ice Cream Sandwich} entspricht. Die empfohlene API Version liegt allerdings bei 26, was Android Version 8.0 \textit{Oreo} entspricht. Zur Implementierung wurde die Entwicklungsumgebung \textit{Android Studio} und das Build-Management-Tool \textit{Gradle} genutzt. Ebenfalls wurde in den Build-Prozess mit Gradle eine Pipeline integriert, um keinen fehlerhaften Code im laufenden Build-Prozess zuzulassen.\\

Des weiteren kam die Spielebibliothek \textit{libGDX} in Version 1.9.9 zum Einsatz, auf der ein Großteil der Anwendung basiert. Hierzu gehört neben der 3D-Ansicht während des Spiels auch die einzelnen Menüseiten und die Soundeffekte. Auch das Rendering der Karten im Spiel wurde mit der Engine umgesetzt.\\

Weitere verwendete Bibliotheken sind zur JSON-Verarbeitung \textit{Gson} und \textit{Retrofit}, um die API des Servers, der die Kartendecks zum Download bereit stellt, in ein Java Interface umzuwandeln.

% Abschnitt: Softwarearchitektur
\section{Softwarearchitektur}
\label{sec:implementierung:softwarearchitektur}

Um die Struktur der Anwendung so unkompliziert wie möglich zu gestalten, wurde ein modularer Aufbau und die Aufteilung auf einzelne Komponenten gewählt. Wichtig hierbei ist die saubere Trennung von Benutzerschnittstelle, Anwendungslogik und Spiellogik. Dies ermöglicht neben einer übersichtlichen Implementierung auch eine gute Erweiterbarkeit und Wartung der einzelnen Module. Ein Großteil des Codes für die animierten Karten entstammt einem Tutorial für die libGDX-Engine unter der Apache-Lizenz \footnote{https://xoppa.github.io/blog/a-simple-card-game/}. Da hier allerdings keinerlei Struktur oder Spiellogik implementiert war, wurde der vorhandene Code nur in Teilen als Grundlage für die Implementierung der grafischen Elemente genommen und in die neue Struktur eingepflegt.\\
\begin{wrapfigure}{l}{0.4\linewidth}
\vspace{-25pt}
\begin{center}
  \includegraphics[width=0.8\linewidth]{folder-structure} %pdf, jpg, png...
  \caption{folder-structure}
  \label{fig:folder-structure}
\end{center}
\end{wrapfigure}
Die Ordnerstruktur mit den Modulen wird in Abbildung \ref{fig:folder-structure} gezeigt. Auf der obersten Ebene befinden sich alle für die Anwendung zentralen Klassen, die auf die Module in den Unterordnern zugreifen. Alle Seiten der Anwendung wie Hauptmenü, Einstellungen und Deck-Store sind im Java-Package \texttt{Screens} enthalten und stellen somit die Nutzeroberfläche dar. Das Einlesen der Kartendecks und die Konvertierung der aus dem Deck-Store heruntergeladenen Kartendecks übernimmt die Klasse \texttt{Parser}. Da das Format der lokal gespeicherten Kartendecks nicht mit dem Format der Kartendecks aus dem Store übereinstimmt, übernimmt diese Klasse auch die notwendige Konvertierung der Datensätze auf das intern verwendete Format. Das Package \texttt{Logic} beinhaltet die Spiellogik des Spiels, wie das Vergleichen von Kategorien oder Spieländerungen bei Einsatz eines PowerUps. Im \texttt{Helper} Package werden verschiedene Hilfsklassen gesammelt, die mehrfach verwendeten Code beinhalten und in mehreren Modulen zum Einsatz kommen. Die \texttt{Generator} Klassen erfüllen zweierlei Aufgabe: Ein Generator übernimmt die Erstellung eines vollständigen Deckordners aus einem heruntergeladenen Kartendeck und ein weiterer konvertiert ein lokal vorhandenes Kartendeck zu Kartenbildern, die dann in der 3D-Ansicht im Spiel durch libGDX angezeigt werden. Das Herunterladen von neuen Kartendecks aus dem Store, sowie den dazugehörigen Bildern, übernimmt das \texttt{Downloader} Package. Für die Darstellung der einzelnen Spielkarten während des Spiels ist das \texttt{Card} Package verantwortlich.

Ein grober Überblick über die Basisfunktionalität der Grundklassen sowie deren Zusammenhänge, wird in Abbildung \ref{fig:class-diagram} verdeutlicht. Die Klasse \texttt{QuartettGame} fungiert als Basisklasse und dient auch als Einstiegspunkt für die libGDX-Engine. \texttt{QuartettGame} beinhaltet sowohl darstellungsrelevante Objekte der Engine, wie Schriftart und allgemeines Aussehen, als auch die allgemeinen Einstellungen (\texttt{Settings}) der Applikation und Informationen über eine laufende Runde (\texttt{GameHandler}). Bei der Navigation innerhalb der Anwendung wird intern zwischen den Objekten der \texttt{Screens} Klasse gewechselt. Wenn ein Nutzer sich in den Einstellungen (dem \texttt{optionsMenuScreen}) befindet, hat dieser die Möglichkeit Anwendungseinstellungen der \texttt{Settings} Klasse zu ändern. Da diese Klasse in \texttt{QuartettGame} enthalten ist, sind die Änderungen dadurch auch für alle anderen Klassen sichtbar, die \texttt{QuartettGame} beinhalten. Bei der Auswahl eines Decks in den Einstellungen, wird dies in der \texttt{BatchConfig} gespeichert. 

Sollte der Nutzer im Store ein Deck herunterladen, wird dies vom \texttt{DeckDownloader} erledigt. Das Speichern des Decks findet dann im \texttt{DeckGenerator} statt.

Beim Starten eines neuen Spiels wird ein neuer \texttt{GameHandler} initialisiert. Dieser enthält einen \texttt{PlayerHandler}, der für Computer und menschlichen Spieler jeweils einen \texttt{Player} enthält, der Informationen über diese speichert. Hierzu gehört beispielsweise eine Liste über noch verfügbare \texttt{PowerUps} und eine Lise mit den in der aktuellen Runde eingesetzten \texttt{PowerUps}. Zusätzlich enthält der \texttt{GameHandler} ein Objekt vom Typ \texttt{GameState}, das die aktuelle Rundenphase speichert.\\

\begin{figure}[htp]
\begin{center}
  \includegraphics[width=1.05\linewidth]{class-diagram} %pdf, jpg, png...
  \caption{Vereinfachtes Klassendiagramm}
  \label{fig:class-diagram}
\end{center}
\end{figure}

% Abschnitt: Erstellung eines Kartendecks
\section{Erstellung eines Kartendecks}
\label{sec:implementierung:deckerstellung}

Eine zusätzliche Anforderung an die Anwendung war das Herunterladen bereitgestellter Kartendecks eines Webservice. Intern erfordert dieser Prozess mehrere Schritte, bis aus dem zum Download ausgewählten Deck ein spielbares Kartendeck wird. Nach Auswahl eines neuen Kartendecks im Store durch den Nutzer werden die dazugehörigen Kartendeck Daten in Form mehrere JSON-Dateien heruntergeladen. Zusätzlich wird parallel der Download der dazugehörigen Kartenbilder gestartet. Anschließend werden die heruntergeladenen JSON-Dateien zu einer einzige JSON-Datei konvertiert und die Datei abgespeichert. Dieser Ablauf wird im ersten Teil der Abbildung \ref{fig:generate-deck} verdeutlicht. \\

\begin{figure}[htp]
\begin{center}
  \includegraphics[width=1\linewidth]{generate-deck} %pdf, jpg, png...
  \caption{Ablauf vom Herunterladen eines neuen Decks bis zum spielbaren Deck}
  \label{fig:generate-deck}
\end{center}
\end{figure}

\begin{wrapfigure}{r}{0.4\linewidth}
\vspace{-30pt}
\begin{center}
  \includegraphics[width=0.8\linewidth]{sprite} %pdf, jpg, png...
  \caption{Sprites}
  \label{fig:sprite}
\end{center}
\end{wrapfigure}

Bevor mit den heruntergeladenen Deckinformationen allerdings ein Spiel gestartet werden kann, ist es notwendig die Karten für die 3D-Ansicht zu generieren. Diese sind im Spiel 3D-Objekte (sogenannte \textit{Sprites}) mit eigener Größe, Position und Rotation. Die Erstellung dieser Objekte ist notwendig, damit die Karten frei umher bewegt werden können. Abbildung \ref{fig:sprite} zeigt mehrere Kartensprites auf zwei Kartenstapeln.\\
\begin{wrapfigure}{r}{0.6\linewidth}
\vspace{-30pt}
\begin{center}
  \includegraphics[width=1\linewidth]{tuning-deck} %pdf, jpg, png...
  \caption{Erstelltes Kartendeck}
  \label{fig:tuning-deck}
\end{center}
\end{wrapfigure}
Der Ablauf zum Erstellen der \textit{Sprites} wird im zweiten Teil der Abbildung \ref{fig:generate-deck} verdeutlicht. Mit Hilfe der libGDX-Engine werden zunächst die Kartenvorderseiten erstellt. Um die Karteninformationen wie Kartenbild, Kartentitel, sowie die Kategorien und deren Werte auf den Kartenobjekten darzustellen, ist ein \texttt{FrameBuffer} Objekt (FBO) notwendig. Der Framebuffer ermöglicht es, Objekte nicht nur innerhalb des Spiels darzustellen (auch \textit{rendering} genannt), sondern sie auch in für den Nutzer nicht sichtbaren Prozessen zu verwenden.

Durch Nutzung dieser Funktion können die Kartenobjekte in den Framebuffer geschrieben werden. Statt für jedes Kartenobjekt eine einzelne Render-Operation durchzuführen, wird hier ein \texttt{SpriteBatch} Objekt genutzt, das die zu rendernden Informationen bündelt und sie abschließend gesammelt an die Grafikeinheit des Gerätes zur Verarbeitung schickt. Dieses Verfahren ist als \textit{sprite batching} in der Spieleentwicklung bekannt.

Abschließend wird der Inhalt des \texttt{FrameBuffer} als \texttt{Pixmap} exportiert und mit allen anderen Karten von einem \texttt{PixmapPacker} kombiniert und als Kartendeck in einer PNG-Datei abgespeichert. Eine vollständig generierte Datei zu einem Deck ist in Abbildung \ref{fig:tuning-deck} zu sehen.

Ebenfalls zu dieser erstellten Bilddatei gehörend ist die Kartenrückseite, die zusammen mit den Vorderseiten abgespeichert wurde. Im Spiel können beide Kartenseiten dann als \textit{Sprite} zusamengesetzt und als fertiges 3D-Objekt angezeigt werden. Ein Code-Ausschnitt zum Generieren des gerade erläuterten Prozesses zeigt Listing \ref{lst:card-generator}.\\

\begin{lstlisting}[caption={Gekürzter Ausschnitt aus der Klasse \texttt{AtlasGenerator} zur Erstellung der Karten}, label={lst:card-generator}]
PixmapPacker packer = new PixmapPacker(packer_width, packer_height, Pixmap.Format.RGB565, padding, true);
packer.setTransparentColor(Color.WHITE);
FrameBuffer m_fbo = new FrameBuffer(Pixmap.Format.RGB565, (int) w, (int) h, false);

/* Generate all cards*/
int cardsCounter = game.getBatchConfig().getCards().size();
for (int cardID = 0; cardID < cardsCounter; cardID++) {
    m_fbo.begin();
    Gdx.gl.glClearColor(1f, 1f, 1f, 1);
    Gdx.gl.glClear(GL_COLOR_BUFFER_BIT);

    /* Get card data*/
    Card c = game.getBatchConfig().getCards().get(cardID);
    /* Insert card data to sprite object */
    create_card(c);
    
    /* Rendering */
    spriteBatch.begin();
    Gdx.gl.glClear(GL_COLOR_BUFFER_BIT);
    Gdx.gl.glClearColor(1f, 1, 1, 1f);
    spriteBatch.draw(sprite, 0, h - newh, neww, newh);
    font.draw(spriteBatch, layout, fontX, fontY);
    table.draw(spriteBatch, 1f);
    spriteBatch.end();
    
    /* Pack pixmap */
    Pixmap pixmap = ScreenUtils.getFrameBufferPixmap(0, 0, (int) w, (int) h);
    pixmap = flipPixmap(pixmap);
    packer.pack(String.valueOf(cardID), pixmap);
    m_fbo.end();
}
\end{lstlisting}